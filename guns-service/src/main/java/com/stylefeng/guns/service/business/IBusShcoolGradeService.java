package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;

/**
 * <p>
 * 图片 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-04
 */
public interface IBusShcoolGradeService extends IService<BusShcoolGrade> {

}
