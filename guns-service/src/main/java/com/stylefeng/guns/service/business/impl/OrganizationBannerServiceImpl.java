package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationBanner;
import com.stylefeng.guns.persistence.modular.business.mapper.OrganizationBannerMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.service.business.IOrganizationBannerService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 机构端-滚动横幅 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class OrganizationBannerServiceImpl extends ServiceImpl<OrganizationBannerMapper, OrganizationBanner> implements IOrganizationBannerService {

    @Override
    public Page<OrganizationBanner> selectByPage(Page<OrganizationBanner> page, Wrapper<OrganizationBanner> wrapper) {
        List<OrganizationBanner> organizationBanners = baseMapper.selectByPage(page, wrapper);
        page.setRecords(organizationBanners);
        return page;
    }
}
