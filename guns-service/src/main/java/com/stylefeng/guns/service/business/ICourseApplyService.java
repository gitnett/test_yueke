package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.CourseApply;
import com.baomidou.mybatisplus.service.IService;

import java.util.Map;

/**
 * <p>
 * 申请额外开课表 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-15
 */
public interface ICourseApplyService extends IService<CourseApply> {

    Page<CourseApply> selectByPage(Page<CourseApply> page, Wrapper<CourseApply> wrapper);
    Page<Map<String,Object>> selectByPage2(Page<Map<String,Object>> page, Wrapper<CourseApply> wrapper);
}
