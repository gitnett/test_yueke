package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.ImageMapper;
import com.stylefeng.guns.persistence.modular.business.model.Image;
import com.stylefeng.guns.service.business.IImageService;

/**
 * <p>
 * 图片 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements IImageService {

}
