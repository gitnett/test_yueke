package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationBanner;

/**
 * <p>
 * 机构端-滚动横幅 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface IOrganizationBannerService extends IService<OrganizationBanner> {


    Page<OrganizationBanner> selectByPage(Page<OrganizationBanner> page, Wrapper<OrganizationBanner> wrapper);
}
