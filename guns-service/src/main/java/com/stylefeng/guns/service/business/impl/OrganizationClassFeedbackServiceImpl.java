package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.OrganizationClassFeedbackMapper;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationClassFeedback;
import com.stylefeng.guns.service.business.IOrganizationClassFeedbackService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 机构-课后反馈 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class OrganizationClassFeedbackServiceImpl extends ServiceImpl<OrganizationClassFeedbackMapper, OrganizationClassFeedback> implements IOrganizationClassFeedbackService {

    @Override
    public Page<OrganizationClassFeedback> selectByPage(Page<OrganizationClassFeedback> page, Wrapper<OrganizationClassFeedback> wrapper) {
        List<OrganizationClassFeedback> organizationClassFeedbacks = baseMapper.selectByPage(page, wrapper);
        page.setRecords(organizationClassFeedbacks);
        return page;
    }
}
