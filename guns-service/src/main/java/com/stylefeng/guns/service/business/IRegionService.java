package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface IRegionService extends IService<Region> {

}
