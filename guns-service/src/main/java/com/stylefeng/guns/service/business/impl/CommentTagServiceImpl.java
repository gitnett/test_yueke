package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.CommentTagMapper;
import com.stylefeng.guns.persistence.modular.business.model.CommentTag;
import com.stylefeng.guns.service.business.ICommentTagService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
@Service
public class CommentTagServiceImpl extends ServiceImpl<CommentTagMapper, CommentTag> implements ICommentTagService {

}
