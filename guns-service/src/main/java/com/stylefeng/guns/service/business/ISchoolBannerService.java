package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.SchoolBanner;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学校端-滚动横幅 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface ISchoolBannerService extends IService<SchoolBanner> {
    Page<SchoolBanner> selectByPage(Page<SchoolBanner> page,  Wrapper<SchoolBanner> wrapper);
}
