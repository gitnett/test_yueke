package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourse;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 机构-课程表 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface IOrganizationCourseService extends IService<OrganizationCourse> {

    Page<OrganizationCourse> selectByPage(Page<OrganizationCourse> page, Wrapper<OrganizationCourse> wrapper);
}
