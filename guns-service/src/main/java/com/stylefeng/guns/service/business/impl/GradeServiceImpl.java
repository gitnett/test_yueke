package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.GradeMapper;
import com.stylefeng.guns.persistence.modular.business.model.Grade;
import com.stylefeng.guns.service.business.IGradeService;

/**
 * <p>
 * 年级列表 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade> implements IGradeService {

}
