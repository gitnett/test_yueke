package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.AccountMapper;
import com.stylefeng.guns.persistence.modular.business.model.Account;
import com.stylefeng.guns.service.business.IAccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
