package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.BusAcctHis;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 支付记录 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface IBusAcctHisService extends IService<BusAcctHis> {

    Page<BusAcctHis> selectByPage(Page<BusAcctHis> page,Wrapper<BusAcctHis> wrapper);
}
