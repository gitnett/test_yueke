package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.SchoolBannerMapper;
import com.stylefeng.guns.persistence.modular.business.model.SchoolBanner;
import com.stylefeng.guns.service.business.ISchoolBannerService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 学校端-滚动横幅 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class SchoolBannerServiceImpl extends ServiceImpl<SchoolBannerMapper, SchoolBanner> implements ISchoolBannerService {

    @Override
    public Page<SchoolBanner> selectByPage(Page<SchoolBanner> page, Wrapper<SchoolBanner> wrapper) {
        List<SchoolBanner> schoolBanners = baseMapper.selectByPage(page, wrapper);
        page.setRecords(schoolBanners);
        return page;
    }
}
