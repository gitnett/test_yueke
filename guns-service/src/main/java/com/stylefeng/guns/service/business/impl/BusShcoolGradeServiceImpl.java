package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.BusShcoolGradeMapper;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;

/**
 * <p>
 * 图片 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-04
 */
@Service
public class BusShcoolGradeServiceImpl extends ServiceImpl<BusShcoolGradeMapper, BusShcoolGrade> implements IBusShcoolGradeService {

}
