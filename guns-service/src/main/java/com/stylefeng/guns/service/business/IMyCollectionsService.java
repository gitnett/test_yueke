package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.MyCollections;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 我的收藏 服务类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface IMyCollectionsService extends IService<MyCollections> {

}
