package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.OrganizationCourseCommentMapper;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourseComment;
import com.stylefeng.guns.service.business.IOrganizationCourseCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 机构-课程评价 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class OrganizationCourseCommentServiceImpl extends ServiceImpl<OrganizationCourseCommentMapper, OrganizationCourseComment> implements IOrganizationCourseCommentService {

}
