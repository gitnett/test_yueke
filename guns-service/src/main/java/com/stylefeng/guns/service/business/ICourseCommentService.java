package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.CourseComment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 学校-课程评价 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface ICourseCommentService extends IService<CourseComment> {

}
