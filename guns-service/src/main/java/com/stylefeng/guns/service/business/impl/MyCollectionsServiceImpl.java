package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.MyCollectionsMapper;
import com.stylefeng.guns.persistence.modular.business.model.MyCollections;
import com.stylefeng.guns.service.business.IMyCollectionsService;

/**
 * <p>
 * 我的收藏 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class MyCollectionsServiceImpl extends ServiceImpl<MyCollectionsMapper, MyCollections> implements IMyCollectionsService {

}
