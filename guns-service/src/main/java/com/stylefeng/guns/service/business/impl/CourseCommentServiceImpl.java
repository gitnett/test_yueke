package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.CourseCommentMapper;
import com.stylefeng.guns.persistence.modular.business.model.CourseComment;
import com.stylefeng.guns.service.business.ICourseCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学校-课程评价 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
@Service
public class CourseCommentServiceImpl extends ServiceImpl<CourseCommentMapper, CourseComment> implements ICourseCommentService {

}
