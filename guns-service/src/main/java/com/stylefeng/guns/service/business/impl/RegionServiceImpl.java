package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.RegionMapper;
import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.stylefeng.guns.service.business.IRegionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements IRegionService {

}
