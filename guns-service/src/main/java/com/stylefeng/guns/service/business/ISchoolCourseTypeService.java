package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.SchoolCourseType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 机构-课程类别 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface ISchoolCourseTypeService extends IService<SchoolCourseType> {

}
