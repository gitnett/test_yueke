package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.CourseMapper;
import com.stylefeng.guns.persistence.modular.business.model.Course;
import com.stylefeng.guns.service.business.ICourseService;

/**
 * <p>
 * 课程列表 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

}
