package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.ClassFeedback;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 课后反馈 服务类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface IClassFeedbackService extends IService<ClassFeedback> {

    Page<ClassFeedback> selectByPage(Page<ClassFeedback> page, Wrapper<ClassFeedback> wrapper);
}
