package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.CourseApplyMapper;
import com.stylefeng.guns.persistence.modular.business.model.CourseApply;
import com.stylefeng.guns.service.business.ICourseApplyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 申请额外开课表 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-15
 */
@Service
public class CourseApplyServiceImpl extends ServiceImpl<CourseApplyMapper, CourseApply> implements ICourseApplyService {

    @Override
    public Page<CourseApply> selectByPage(Page<CourseApply> page, Wrapper<CourseApply> wrapper) {
        List<CourseApply> courseApplies = baseMapper.selectByPage(page, wrapper);
        page.setRecords(courseApplies);
        return page;
    }


    @Override
    public Page<Map<String,Object>> selectByPage2(Page<Map<String,Object>> page, Wrapper<CourseApply> wrapper) {
        List<Map<String, Object>> maps = baseMapper.selectByPage2(page, wrapper);
        page.setRecords(maps);
        return page;
    }
}
