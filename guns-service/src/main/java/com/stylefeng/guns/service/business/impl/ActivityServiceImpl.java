package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.Activity;
import com.stylefeng.guns.persistence.modular.business.mapper.ActivityMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.service.business.IActivityService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 活动表 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements IActivityService {

    @Override
    public Page<Activity> selectByPage(Page<Activity> page, Wrapper<Activity> wrapper) {
        List<Activity> activities = baseMapper.selectByPage(page, wrapper);
        page.setRecords(activities);
        return page;
    }
}
