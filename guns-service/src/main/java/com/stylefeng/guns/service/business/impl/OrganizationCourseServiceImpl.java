package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.OrganizationCourseMapper;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourse;
import com.stylefeng.guns.service.business.IOrganizationCourseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 机构-课程表 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class OrganizationCourseServiceImpl extends ServiceImpl<OrganizationCourseMapper, OrganizationCourse> implements IOrganizationCourseService {

    @Override
    public Page<OrganizationCourse> selectByPage(Page<OrganizationCourse> page, Wrapper<OrganizationCourse> wrapper) {
        List<OrganizationCourse> organizationCourses = baseMapper.selectByPage(page, wrapper);
        page.setRecords(organizationCourses);
        return page;
    }
}
