package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.School;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 学校 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface ISchoolService extends IService<School> {

}
