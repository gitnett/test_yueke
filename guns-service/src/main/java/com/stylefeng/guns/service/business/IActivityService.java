package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.Activity;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 活动表 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface IActivityService extends IService<Activity> {

    Page<Activity> selectByPage(Page<Activity> page, Wrapper<Activity> wrapper);
}
