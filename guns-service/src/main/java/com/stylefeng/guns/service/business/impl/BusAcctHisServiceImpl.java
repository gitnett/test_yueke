package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.BusAcctHis;
import com.stylefeng.guns.service.business.IBusAcctHisService;
import com.stylefeng.guns.persistence.modular.business.mapper.BusAcctHisMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 支付记录 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@Service
public class BusAcctHisServiceImpl extends ServiceImpl<BusAcctHisMapper, BusAcctHis> implements IBusAcctHisService {

    @Override
    public Page<BusAcctHis> selectByPage(Page<BusAcctHis> page, Wrapper<BusAcctHis> wrapper){
        List<BusAcctHis> busAcctHis = baseMapper.selectByPage(page,wrapper);
        page.setRecords(busAcctHis);
        return page;
    }
}
