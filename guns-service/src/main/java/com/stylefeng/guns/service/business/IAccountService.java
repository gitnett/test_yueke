package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.Account;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface IAccountService extends IService<Account> {

}
