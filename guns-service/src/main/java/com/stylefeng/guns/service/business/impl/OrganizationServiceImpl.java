package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.OrganizationMapper;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 机构列表 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@Service
public class OrganizationServiceImpl extends ServiceImpl<OrganizationMapper, Organization> implements IOrganizationService {

}
