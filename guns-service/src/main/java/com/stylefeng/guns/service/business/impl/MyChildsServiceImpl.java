package com.stylefeng.guns.service.business.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.MyChildsMapper;
import com.stylefeng.guns.persistence.modular.business.model.MyChilds;
import com.stylefeng.guns.service.business.IMyChildsService;

/**
 * <p>
 * 我的孩子 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class MyChildsServiceImpl extends ServiceImpl<MyChildsMapper, MyChilds> implements IMyChildsService {

}
