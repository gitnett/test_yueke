package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.SchoolMapper;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.service.business.ISchoolService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学校 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements ISchoolService {

}
