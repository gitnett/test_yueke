package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.ClassFeedbackMapper;
import com.stylefeng.guns.persistence.modular.business.model.ClassFeedback;
import com.stylefeng.guns.service.business.IClassFeedbackService;

import java.util.List;

/**
 * <p>
 * 课后反馈 服务实现类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@Service
public class ClassFeedbackServiceImpl extends ServiceImpl<ClassFeedbackMapper, ClassFeedback> implements IClassFeedbackService {

    @Override
    public Page<ClassFeedback> selectByPage(Page<ClassFeedback> page, Wrapper<ClassFeedback> wrapper) {
        List<ClassFeedback> classFeedbacks = baseMapper.selectByPage(page, wrapper);
        page.setRecords(classFeedbacks);
        return page;
    }
}
