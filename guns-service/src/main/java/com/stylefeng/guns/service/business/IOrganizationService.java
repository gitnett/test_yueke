package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 机构列表 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface IOrganizationService extends IService<Organization> {

}
