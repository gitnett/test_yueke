package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.Image;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 图片 服务类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface IImageService extends IService<Image> {

}
