package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.Course;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 课程列表 服务类
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface ICourseService extends IService<Course> {

}
