package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.CommentTag;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface ICommentTagService extends IService<CommentTag> {

}
