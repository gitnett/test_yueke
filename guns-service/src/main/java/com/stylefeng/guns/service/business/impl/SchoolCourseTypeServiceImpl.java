package com.stylefeng.guns.service.business.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.modular.business.mapper.SchoolCourseTypeMapper;
import com.stylefeng.guns.persistence.modular.business.model.SchoolCourseType;
import com.stylefeng.guns.service.business.ISchoolCourseTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 机构-课程类别 服务实现类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
@Service
public class SchoolCourseTypeServiceImpl extends ServiceImpl<SchoolCourseTypeMapper, SchoolCourseType> implements ISchoolCourseTypeService {

}
