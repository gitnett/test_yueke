package com.stylefeng.guns.service.business;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationClassFeedback;

/**
 * <p>
 * 机构-课后反馈 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface IOrganizationClassFeedbackService extends IService<OrganizationClassFeedback> {

    Page<OrganizationClassFeedback> selectByPage(Page<OrganizationClassFeedback> page, Wrapper<OrganizationClassFeedback> wrapper);
}
