package com.stylefeng.guns.service.business;

import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourseComment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 机构-课程评价 服务类
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface IOrganizationCourseCommentService extends IService<OrganizationCourseComment> {

}
