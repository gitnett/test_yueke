package com.stylefeng.guns.core.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * http请求工具类,可以发送GET,POST请求
 * 
 * @author LinYingQiang
 *
 */
public class HttpUtil {

	private static PoolingHttpClientConnectionManager connectionManager;

	static {
		connectionManager = new PoolingHttpClientConnectionManager();
		connectionManager.setMaxTotal(30);// 整个连接池最大连接数
	}

	private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	/**
	 * 执行http POST请求
	 * 
	 * @param url
	 *            请求服务器地址
	 * @param params
	 *            参数,key=value格式
	 * @param respClazz
	 *            期望返回的数据
	 * @return
	 */
	public static <T> T executePost(String url, Map<String, String> params, Class<T> respClazz) {
		T t = null;
		List<NameValuePair> formparams = new ArrayList<>();
		if (params != null && !params.isEmpty()) {
			params.forEach((k, v) -> {
				formparams.add(new BasicNameValuePair(k, v));
			});
		}
		try {
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(formparams, Charset.defaultCharset()));
			CloseableHttpClient req = HttpClients.custom().setConnectionManager(connectionManager).build();
			CloseableHttpResponse resp = req.execute(httpPost);
			HttpEntity entity = resp.getEntity();
			if (entity != null) {
				String result = EntityUtils.toString(entity);
				t = JSONObject.parseObject(result, respClazz);
				resp.close();
			}
			logger.info("HttpUtil发送POST请求处理完毕,地址---{},请求参数---{},返回参数---{}", url, params, t);
		} catch (ClientProtocolException e) {
			logger.error("HttpUtil发送POST请求出现异常---{}", e.getMessage());
		} catch (IOException e) {
			logger.error("HttpUtil发送POST请求出现异常---{}", e.getMessage());
		}
		return t;
	}

    /**
     * 执行http POST请求
     *
     * @param url
     *            请求服务器地址
     * @param json
     *            参数,JSON格式
     * @param respClazz
     *            期望返回的数据
     * @return
     */
    public static <T> T executePost(String url, String json, Class<T> respClazz) {
        T t = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity requestEntity = new StringEntity(json,Charset.defaultCharset());
            httpPost.setEntity(requestEntity);
            CloseableHttpClient req = HttpClients.custom().setConnectionManager(connectionManager).build();
            CloseableHttpResponse resp = req.execute(httpPost);
            HttpEntity entity = resp.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                t = JSONObject.parseObject(result, respClazz);
                resp.close();
            }
            logger.info("HttpUtil发送POST请求处理完毕,地址---{},请求参数---{},返回参数---{}", url, json, t);
        } catch (ClientProtocolException e) {
            logger.error("HttpUtil发送POST请求出现异常---{}", e.getMessage());
        } catch (IOException e) {
            logger.error("HttpUtil发送POST请求出现异常---{}", e.getMessage());
        }
        return t;
    }

	public static <T> T executeGet(String url, Map<String, String> params, Class<T> respClazz) {
		T t = null;
		List<NameValuePair> formparams = new ArrayList<>();
		if (params != null && !params.isEmpty()) {
			params.forEach((k, v) -> {
				formparams.add(new BasicNameValuePair(k, v));
			});
		}
		try {
			URIBuilder ub = new URIBuilder();
			ub.setPath(url);
            ub.setParameters(formparams);
			HttpGet httpGet = new HttpGet(ub.build());
			CloseableHttpClient req = HttpClients.custom().setConnectionManager(connectionManager).build();
			CloseableHttpResponse resp = req.execute(httpGet);
			HttpEntity entity = resp.getEntity();
			if (entity != null) {
				String result = EntityUtils.toString(entity);
				t = JSONObject.parseObject(result, respClazz);
				resp.close();
			}
			logger.info("HttpUtil发送GET请求处理完毕,地址---{},请求参数---{},返回参数---{}", url, params, t);
		} catch (ClientProtocolException e) {
			logger.error("HttpUtil发送GET请求出现异常---{}", e.getMessage());
		} catch (ParseException e) {
			logger.error("HttpUtil发送GET请求出现异常---{}", e.getMessage());
		} catch (URISyntaxException e) {
			logger.error("HttpUtil发送GET请求出现异常---{}", e.getMessage());
		} catch (IOException e) {
			logger.error("HttpUtil发送GET请求出现异常---{}", e.getMessage());
		}
		return t;
	}
}
