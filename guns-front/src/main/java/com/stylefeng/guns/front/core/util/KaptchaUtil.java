package com.stylefeng.guns.front.core.util;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.front.config.properties.GunsProperties;

/**
 * 验证码工具类
 */
public class KaptchaUtil {

    /**
     * 获取验证码开关
     */
    public static Boolean getKaptchaOnOff() {
        return SpringContextHolder.getBean(GunsProperties.class).getKaptchaOpen();
    }
}