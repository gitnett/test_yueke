package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 活动表
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@TableName("bus_activity")
public class Activity extends Model<Activity> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 活动图片
     */
    private String picture;
    /**
     * 活动名称
     */
    private String title;
    /**
     * 系统设定参与人数
     */
    @TableField("total_count")
    private Integer totalCount;
    /**
     * 参与人数
     */
    @TableField("join_count")
    private Integer joinCount;
    /**
     * 适合学生
     */
    @TableField("suitable_target")
    private String suitableTarget;
    /**
     * 活动时间
     */
    @TableField("activity_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date activityTime;
    /**
     * 活动地点
     */
    private String address;
    /**
     * 咨询电话
     */
    private String phone;
    /**
     * 活动内容
     */
    private String content;
    /**
     * 0-未发布,1-已发布
     */
    private Integer state;
    /**
     * 参与价格
     */
    private BigDecimal price;
    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 组织机构id
     */
    @TableField("organization_id")
    private Integer organizationId;

    @TableField(exist = false)
    private Organization organization;

    /**
     * 排序
     */
    @TableField("serial_no")
    private Integer serialNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(Integer joinCount) {
        this.joinCount = joinCount;
    }

    public String getSuitableTarget() {
        return suitableTarget;
    }

    public void setSuitableTarget(String suitableTarget) {
        this.suitableTarget = suitableTarget;
    }

    public Date getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(Date activityTime) {
        this.activityTime = activityTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Activity{" +
        "id=" + id +
        ", picture=" + picture +
        ", title=" + title +
        ", totalCount=" + totalCount +
        ", joinCount=" + joinCount +
        ", suitableTarget=" + suitableTarget +
        ", activityTime=" + activityTime +
        ", address=" + address +
        ", phone=" + phone +
        ", content=" + content +
        ", state=" + state +
        ", price=" + price +
        ", createtime=" + createtime +
        "}";
    }
}
