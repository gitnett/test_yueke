package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface RegionMapper extends BaseMapper<Region> {

}
