package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.ConfigInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-14
 */
public interface ConfigInfoMapper extends BaseMapper<ConfigInfo> {

}
