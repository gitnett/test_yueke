package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 学校
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@TableName("bus_school")
public class School extends Model<School> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 学校名称
     */
    private String name;
    /**
     * 学校地址
     */
    private String address;

    /**
     * 业务区域
     */
    @TableField("region_id")
    private Integer regionId;

    /**
     * 业务区域实体
     */
    @TableField(exist = false)
    private Region region;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 排序
     */
    @TableField("serial_no")
    private Integer serialNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    @Override
    public String toString() {
        return "School{" +
        "id=" + id +
        ", name=" + name +
        ", address=" + address +
        ", createtime=" + createtime +
        "}";
    }
}
