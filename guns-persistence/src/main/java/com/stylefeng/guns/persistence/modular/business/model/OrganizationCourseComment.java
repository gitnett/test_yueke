package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 机构-课程评价
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_organization_course_comment")
public class OrganizationCourseComment extends Model<OrganizationCourseComment> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @TableField("account_id")
    private Integer accountId;
    /**
     * 用户名称
     */
    @TableField("account_name")
    private String accountName;
    /**
     * 课程ID
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 学校ID
     */
    @TableField("organization_id")
    private Integer organizationId;
    /**
     * 学校名称
     */
    @TableField("organization_name")
    private String organizationName;
    private String content;
    /**
     * 星级
     */
    @TableField("star_count")
    private Integer starCount;
    /**
     * 评论时间
     */
    private Date createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStarCount() {
        return starCount;
    }

    public void setStarCount(Integer starCount) {
        this.starCount = starCount;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OrganizationCourseComment{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", accountName=" + accountName +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", organizationId=" + organizationId +
        ", organizationName=" + organizationName +
        ", content=" + content +
        ", starCount=" + starCount +
        ", createtime=" + createtime +
        "}";
    }
}
