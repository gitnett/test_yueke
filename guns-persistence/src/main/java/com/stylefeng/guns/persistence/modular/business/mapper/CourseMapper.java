package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.Course;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 课程列表 Mapper 接口
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface CourseMapper extends BaseMapper<Course> {

}
