package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.SchoolBanner;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学校端-滚动横幅 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface SchoolBannerMapper extends BaseMapper<SchoolBanner> {

    List<SchoolBanner> selectByPage(Page<SchoolBanner> page, @Param("ew") Wrapper<SchoolBanner> wrapper);
}
