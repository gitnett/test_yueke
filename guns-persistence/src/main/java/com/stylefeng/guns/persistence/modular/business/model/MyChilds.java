package com.stylefeng.guns.persistence.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 我的孩子
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@TableName("bus_my_childs")
public class MyChilds extends Model<MyChilds> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 孩子名称
     */
    @TableField("child_name")
    private String childName;
    /**
     * 孩子学校
     */
    @TableField("child_school")
    private String childSchool;
    /**
     * 孩子班级
     */
    @TableField("child_class")
    private String childClass;
    /**
     * 主表accout主键
     */
    @TableField("account_id")
    private Integer accountId;

    @TableField("phone_two")
    private String phoneTwo;

    @TableField("level_type")
    private String levelType;
    
    private Date createtime;
    
    private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getChildSchool() {
        return childSchool;
    }

    public void setChildSchool(String childSchool) {
        this.childSchool = childSchool;
    }

    public String getChildClass() {
        return childClass;
    }

    public void setChildClass(String childClass) {
        this.childClass = childClass;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    @Override
    public String toString() {
        return "MyChilds{" +
        "id=" + id +
        ", childName=" + childName +
        ", childSchool=" + childSchool +
        ", childClass=" + childClass +
        ", accountId=" + accountId +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
