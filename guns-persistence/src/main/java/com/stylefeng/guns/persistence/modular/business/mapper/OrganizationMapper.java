package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 机构列表 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

}
