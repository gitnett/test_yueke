package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-14
 */
@TableName("bus_config_info")
public class ConfigInfo extends Model<ConfigInfo> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 配置名称
     */
    private String name;
    /**
     * 配置值
     */
    private String content;
    private String tips;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ConfigInfo{" +
        "id=" + id +
        ", name=" + name +
        ", content=" + content +
        ", tips=" + tips +
        "}";
    }
}
