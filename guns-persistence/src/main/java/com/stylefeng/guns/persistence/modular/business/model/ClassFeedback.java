package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 课后反馈
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@TableName("bus_class_feedback")
public class ClassFeedback extends Model<ClassFeedback> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 反馈内容
     */
    @TableField("feedback_content")
    private String feedbackContent;
    /**
     * 主表accout主键
     */
    @TableField("account_id")
    private Integer accountId;

    /**
     * 学校id
     */
    @TableField("school_id")
    private Integer schoolId;

    @TableField(exist = false)
    private School school;
    
    @TableField("account_name")
    private String accountName;
    
    private Date createtime;
    
    private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getFeedbackContent() {
		return feedbackContent;
	}

	public void setFeedbackContent(String feedbackContent) {
		this.feedbackContent = feedbackContent;
	}

	public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
    public String toString() {
        return "ClassFeedback{" +
        "id=" + id +
        ", feedbackContent=" + feedbackContent +
        ", accountId=" + accountId +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
