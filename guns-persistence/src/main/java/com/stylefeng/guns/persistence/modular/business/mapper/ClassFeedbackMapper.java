package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.ClassFeedback;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 课后反馈 Mapper 接口
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface ClassFeedbackMapper extends BaseMapper<ClassFeedback> {

    List<ClassFeedback> selectByPage(Page<ClassFeedback> page, @Param("ew")Wrapper<ClassFeedback> wrapper);
}
