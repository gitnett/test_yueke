package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 学校-课程评价
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
@TableName("bus_course_comment")
public class CourseComment extends Model<CourseComment> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @TableField("account_id")
    private Integer accountId;
    /**
     * 用户名称
     */
    @TableField("account_name")
    private String accountName;
    /**
     * 课程ID
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 订单id
     */
    @TableField("his_id")
    private Integer hisId;
    /**
     * 标签
     */
    private String tag;
    private String content;
    /**
     * 星级
     */
    @TableField("star_count")
    private Integer starCount;
    /**
     * 评论时间
     */
    private Date createtime;

    @TableField(exist = false)
    private String schoolName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getHisId() {
        return hisId;
    }

    public void setHisId(Integer hisId) {
        this.hisId = hisId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStarCount() {
        return starCount;
    }

    public void setStarCount(Integer starCount) {
        this.starCount = starCount;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public String toString() {
        return "CourseComment{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", accountName=" + accountName +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", hisId=" + hisId +
        ", tag=" + tag +
        ", content=" + content +
        ", starCount=" + starCount +
        ", createtime=" + createtime +
        "}";
    }
}
