package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.CommentTag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface CommentTagMapper extends BaseMapper<CommentTag> {

}
