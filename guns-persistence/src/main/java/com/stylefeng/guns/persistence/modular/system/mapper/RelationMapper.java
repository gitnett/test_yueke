package com.stylefeng.guns.persistence.modular.system.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.persistence.modular.system.model.Relation;

/**
 * <p>
  * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-07-11
 */
public interface RelationMapper extends BaseMapper<Relation> {

}