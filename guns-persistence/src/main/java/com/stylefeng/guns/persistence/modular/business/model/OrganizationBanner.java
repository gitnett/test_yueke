package com.stylefeng.guns.persistence.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 机构端-滚动横幅
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_organization_banner")
public class OrganizationBanner extends Model<OrganizationBanner> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 横幅名称
     */
    private String title;
    /**
     * 图片路径
     */
    private String picture;
    /**
     * 排序序号
     */
    @TableField("serial_no")
    private Integer serialNo;
    /**
     * 链接
     */
    private String url;
    /**
     * 是否显示，0-否，1-是
     */
    private Integer state;
    /**
     * 机构ID
     */
    @TableField("organization_id")
    private Integer organizationId;

    /**
     * 机构
     */
    @TableField(exist = false)
    private Organization organization;

    /**
     * 创建时间
     */
    private Date createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "OrganizationBanner{" +
        "id=" + id +
        ", title=" + title +
        ", picture=" + picture +
        ", serialNo=" + serialNo +
        ", url=" + url +
        ", state=" + state +
        ", organizationId=" + organizationId +
        ", createtime=" + createtime +
        "}";
    }
}
