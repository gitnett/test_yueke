package com.stylefeng.guns.persistence.modular.business.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 支付记录
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_acct_his")
public class BusAcctHis extends Model<BusAcctHis> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 主表accout主键
     */
    @TableField("account_id")
    private Integer accountId;
    /**
     * 课程ID
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 用户姓名
     */
    @TableField("account_name")
    private String accountName;
    
    @TableField("account_phone")
    private String accountPhone;
    /**
     * 支付金额
     */
    private Double amount;
    /**
     * 支付方式
     */
    @TableField("pay_type")
    private String payType;
    /**
     * 来源渠道
     */
    @TableField("acct_channel")
    private String acctChannel;
    /**
     * 学校名称
     */
    @TableField("school_name")
    private String schoolName;
    /**
     * 年级班级
     */
    @TableField("grade_class_name")
    private String gradeClassName;
    /**
     * 支付状态
     */
    private String status;
    /**
     * 平台流水号
     */
    @TableField("flow_no")
    private String flowNo;
    /**
     * 第三方流水号
     */
    @TableField("bus_flow_no")
    private String busFlowNo;
    /**
     * 描述语
     */
    private String msg;
    
    /**
     * 孩子姓名
     */
    @TableField("child_name")
    private String childName;
    
    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;

    /**
     * 离校方式
     */
    @TableField(exist = false)
    private String levelType;

    /**
     * 紧急联系人
     */
    @TableField(exist = false)
    private String phoneTwo;

    /**
     * 孩子id
     */
    @TableField("child_id")
    private Integer childId;

    /**
     * 孩子
     */
    @TableField(exist = false)
    private MyChilds childs;
    
    
    private Date createtime;
    private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }


    

    public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getAcctChannel() {
        return acctChannel;
    }

    public void setAcctChannel(String acctChannel) {
        this.acctChannel = acctChannel;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getGradeClassName() {
        return gradeClassName;
    }

    public void setGradeClassName(String gradeClassName) {
        this.gradeClassName = gradeClassName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(String flowNo) {
        this.flowNo = flowNo;
    }

    public String getBusFlowNo() {
        return busFlowNo;
    }

    public void setBusFlowNo(String busFlowNo) {
        this.busFlowNo = busFlowNo;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    /**
	 * @return the childName
	 */
	public String getChildName() {
		return childName;
	}

	/**
	 * @param childName the childName to set
	 */
	public void setChildName(String childName) {
		this.childName = childName;
	}

	/**
	 * @return the idCard
	 */
	public String getIdCard() {
		return idCard;
	}

	/**
	 * @param idCard the idCard to set
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	

	/**
	 * @return the accountPhone
	 */
	public String getAccountPhone() {
		return accountPhone;
	}

	/**
	 * @param accountPhone the accountPhone to set
	 */
	public void setAccountPhone(String accountPhone) {
		this.accountPhone = accountPhone;
	}

    public Integer getChildId() {
        return childId;
    }

    public void setChildId(Integer childId) {
        this.childId = childId;
    }

    public MyChilds getChilds() {
        return childs;
    }

    public void setChilds(MyChilds childs) {
        this.childs = childs;
    }

    @Override
    public String toString() {
        return "BusAcctHis{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", accountName=" + accountName +
        ", amount=" + amount +
        ", payType=" + payType +
        ", acctChannel=" + acctChannel +
        ", schoolName=" + schoolName +
        ", gradeClassName=" + gradeClassName +
        ", status=" + status +
        ", flowNo=" + flowNo +
        ", busFlowNo=" + busFlowNo +
        ", msg=" + msg +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
