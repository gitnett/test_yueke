package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 图片
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@TableName("bus_image")
public class Image extends Model<Image> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 图片名称
     */
    @TableField("image_name")
    private String imageName;
    /**
     * 图片路径
     */
    @TableField("image_path")
    private String imagePath;
    /**
     * 图片类型
     */
    @TableField("image_type")
    private int imageType;
    /**
     * 图片大小
     */
    @TableField("image_size")
    private String imageSize;
    
    private Date createtime;
    private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    public int getImageType() {
		return imageType;
	}

	public void setImageType(int imageType) {
		this.imageType = imageType;
	}

	public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Image{" +
        "id=" + id +
        ", imageName=" + imageName +
        ", imagePath=" + imagePath +
        ", imageType=" + imageType +
        ", imageSize=" + imageSize +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
