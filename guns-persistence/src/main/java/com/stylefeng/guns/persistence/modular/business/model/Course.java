package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 课程列表
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@TableName("bus_course")
public class Course extends Model<Course> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 招生对象
     */
    @TableField("start_course_grade")
    private String startCourseGrade;
    
    /**
     * 招生对象
     */
    @TableField("end_course_grade")
    private String endCourseGrade;
    
    /**
    /**
     * 上课时间
     */
    @TableField("course_onclass_time")
    private String courseOnclassTime;
    
    /**
    /**
     * 上课时间:星期
     */
    @TableField("course_onclass_week")
    private String courseOnclassWeek;
    
    /**
     * 课程费用
     */
    @TableField("course_fee")
    private Double courseFee;
    /**
     * 剩余名额
     */
    @TableField("course_quota")
    private Integer courseQuota;
    /**
     * 总报名人数
     */
    @TableField("course_tol_num")
    private Integer courseTolNum;
    /**
     * 培训学期
     */
    @TableField("coures_train_term")
    private String couresTrainTerm;
    /**
     * 上课教师
     */
    @TableField("coures_teacher")
    private String couresTeacher;
    /**
     * 上课教室
     */
    @TableField("coures_classroom")
    private String couresClassroom;
    /**
     * 联系电话
     */
    @TableField("coures_iphone")
    private String couresIphone;
    /**
     * 开课时间
     */
    @TableField("coures_openclass_time")
    private String couresOpenclassTime;
    /**
     * 报名开始时间
     */
    @TableField("coures_signup_starttime")
    private String couresSignupStarttime;
    /**
     * 报名结束时间
     */
    @TableField("coures_signup_endtime")
    private String couresSignupEndtime;
    /**
     * 课程简介
     */
    @TableField("coures_introduction")
    private String couresIntroduction;
    /**
     * 课程课时
     */
    @TableField("coures_class_hour")
    private String couresClassHour;
    /**
     * 课程图片
     */
    @TableField("coures_img_path")
    private String couresImgPath;
    
    @TableField("school_name")  
    private String schoolName;
    
    @TableField("school_id")  
    private String schoolId;
    
    /**
     * 横幅
     */
    @TableField("coures_banner_path")
    private String bannerPath;
    
    /**
     * 状态：课程状态: 0:发布  1：下架';
     */
    @TableField("coures_status")
    private int courseStatus;
    
    private Date createtime;
    
    private Date updatetime;

    @TableField("type_id")
    private Integer typeId;

    @TableField(exist = false)
    private String typeName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }


    public String getStartCourseGrade() {
		return startCourseGrade;
	}

	public void setStartCourseGrade(String startCourseGrade) {
		this.startCourseGrade = startCourseGrade;
	}

	public String getEndCourseGrade() {
		return endCourseGrade;
	}

	public void setEndCourseGrade(String endCourseGrade) {
		this.endCourseGrade = endCourseGrade;
	}

	public String getCourseOnclassTime() {
        return courseOnclassTime;
    }

    public void setCourseOnclassTime(String courseOnclassTime) {
        this.courseOnclassTime = courseOnclassTime;
    }


    
    public Double getCourseFee() {
		return courseFee;
	}

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setCourseFee(Double courseFee) {
		this.courseFee = courseFee;
	}

	public Integer getCourseQuota() {
        return courseQuota;
    }

    public void setCourseQuota(Integer courseQuota) {
        this.courseQuota = courseQuota;
    }

    public Integer getCourseTolNum() {
        return courseTolNum;
    }

    public void setCourseTolNum(Integer courseTolNum) {
        this.courseTolNum = courseTolNum;
    }

    public String getCouresTrainTerm() {
        return couresTrainTerm;
    }

    public void setCouresTrainTerm(String couresTrainTerm) {
        this.couresTrainTerm = couresTrainTerm;
    }

    public String getCouresTeacher() {
        return couresTeacher;
    }

    public void setCouresTeacher(String couresTeacher) {
        this.couresTeacher = couresTeacher;
    }

    public String getCouresClassroom() {
        return couresClassroom;
    }

    public void setCouresClassroom(String couresClassroom) {
        this.couresClassroom = couresClassroom;
    }

    public String getCouresIphone() {
        return couresIphone;
    }

    public void setCouresIphone(String couresIphone) {
        this.couresIphone = couresIphone;
    }

   
    public String getCouresIntroduction() {
        return couresIntroduction;
    }

    public void setCouresIntroduction(String couresIntroduction) {
        this.couresIntroduction = couresIntroduction;
    }

    public String getCouresClassHour() {
        return couresClassHour;
    }

    public void setCouresClassHour(String couresClassHour) {
        this.couresClassHour = couresClassHour;
    }


    public String getCouresImgPath() {
		return couresImgPath;
	}

	public void setCouresImgPath(String couresImgPath) {
		this.couresImgPath = couresImgPath;
	}

	public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    
    

    public String getCouresOpenclassTime() {
		return couresOpenclassTime;
	}

	public void setCouresOpenclassTime(String couresOpenclassTime) {
		this.couresOpenclassTime = couresOpenclassTime;
	}

	public String getCouresSignupStarttime() {
		return couresSignupStarttime;
	}

	public void setCouresSignupStarttime(String couresSignupStarttime) {
		this.couresSignupStarttime = couresSignupStarttime;
	}

	public String getCouresSignupEndtime() {
		return couresSignupEndtime;
	}

	public void setCouresSignupEndtime(String couresSignupEndtime) {
		this.couresSignupEndtime = couresSignupEndtime;
	}
	
	

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}



	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getBannerPath() {
		return bannerPath;
	}

	public void setBannerPath(String bannerPath) {
		this.bannerPath = bannerPath;
	}

	public int getCourseStatus() {
		return courseStatus;
	}

	public void setCourseStatus(int courseStatus) {
		this.courseStatus = courseStatus;
	}

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

	/**
	 * @return the courseOnclassWeek
	 */
	public String getCourseOnclassWeek() {
		return courseOnclassWeek;
	}

	/**
	 * @param courseOnclassWeek the courseOnclassWeek to set
	 */
	public void setCourseOnclassWeek(String courseOnclassWeek) {
		this.courseOnclassWeek = courseOnclassWeek;
	}
    
    
}
