package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.MyChilds;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 我的孩子 Mapper 接口
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface MyChildsMapper extends BaseMapper<MyChilds> {

}
