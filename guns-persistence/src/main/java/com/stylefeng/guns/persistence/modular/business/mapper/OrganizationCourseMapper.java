package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourse;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 机构-课程表 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface OrganizationCourseMapper extends BaseMapper<OrganizationCourse> {

    List<OrganizationCourse> selectByPage(Page<OrganizationCourse> page, @Param("ew") Wrapper<OrganizationCourse> wrapper);
}
