package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 机构-课程表
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_organization_course")
public class OrganizationCourse extends Model<OrganizationCourse> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 课程图片
     */
    private String picture;
    /**
     * 课程名称
     */
    private String title;
    /**
     * 系统设定参与人数
     */
    @TableField("total_count")
    private Integer totalCount;
    /**
     * 参与人数
     */
    @TableField("join_count")
    private Integer joinCount;
    /**
     * 适合学生
     */
    @TableField("suitable_target")
    private String suitableTarget;
    /**
     * 课程时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @TableField("course_time")
    private Date courseTime;
    /**
     * 课程地点
     */
    private String address;
    /**
     * 咨询电话
     */
    private String phone;
    /**
     * 课程内容
     */
    private String content;
    /**
     * 0-未发布,1-已发布
     */
    private Integer state;
    /**
     * 课程价格
     */
    private BigDecimal price;
    /**
     * 机构ID
     */
    @TableField("organization_id")
    private Integer organizationId;

    @TableField(exist = false)
    private Organization organization;

    /**
     * 排序序号
     */
    @TableField("serial_no")
    private Integer serialNo;
    /**
     * 创建时间
     */
    private Date createtime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(Integer joinCount) {
        this.joinCount = joinCount;
    }

    public String getSuitableTarget() {
        return suitableTarget;
    }

    public void setSuitableTarget(String suitableTarget) {
        this.suitableTarget = suitableTarget;
    }

    public Date getCourseTime() {
        return courseTime;
    }

    public void setCourseTime(Date courseTime) {
        this.courseTime = courseTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "OrganizationCourse{" +
        "id=" + id +
        ", picture=" + picture +
        ", title=" + title +
        ", totalCount=" + totalCount +
        ", joinCount=" + joinCount +
        ", suitableTarget=" + suitableTarget +
        ", courseTime=" + courseTime +
        ", address=" + address +
        ", phone=" + phone +
        ", content=" + content +
        ", state=" + state +
        ", price=" + price +
        ", organizationId=" + organizationId +
        ", serialNo=" + serialNo +
        ", createtime=" + createtime +
        "}";
    }
}
