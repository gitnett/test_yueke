package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.School;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 学校 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface SchoolMapper extends BaseMapper<School> {

}
