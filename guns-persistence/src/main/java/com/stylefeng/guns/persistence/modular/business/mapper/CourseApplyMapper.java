package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.CourseApply;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 申请额外开课表 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-15
 */
public interface CourseApplyMapper extends BaseMapper<CourseApply> {


    List<CourseApply> selectByPage(Page<CourseApply> page, @Param("ew") Wrapper<CourseApply> wrapper);


    List<Map<String,Object>> selectByPage2(Page<Map<String,Object>> page, @Param("ew") Wrapper<CourseApply> wrapper);

}
