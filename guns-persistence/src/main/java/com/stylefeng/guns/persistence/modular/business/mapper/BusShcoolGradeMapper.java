package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 图片 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-04
 */
public interface BusShcoolGradeMapper extends BaseMapper<BusShcoolGrade> {

}
