package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.Account;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface AccountMapper extends BaseMapper<Account> {

}
