package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationBanner;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 机构端-滚动横幅 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface OrganizationBannerMapper extends BaseMapper<OrganizationBanner> {

    List<OrganizationBanner> selectByPage(Page<OrganizationBanner> page, @Param("ew") Wrapper<OrganizationBanner> wrapper);
}
