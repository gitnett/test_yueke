package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.CourseComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 学校-课程评价 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface CourseCommentMapper extends BaseMapper<CourseComment> {

}
