package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 机构-课后反馈
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_organization_class_feedback")
public class OrganizationClassFeedback extends Model<OrganizationClassFeedback> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 机构ID
     */
    @TableField("organization_id")
    private Integer organizationId;

    /**
     * 机构
     */
    @TableField(exist = false)
    private Organization organization;

    /**
     * 用户ID
     */
    @TableField("account_id")
    private Integer accountId;

    /**
     * 用户名称
     */
    @TableField("account_name")
    private String accountName;

    /**
     * 反馈内容
     */
    private String content;
    /**
     * 创建时间
     */
    private Date createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getAccountName() {
        return accountName;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OrganizationClassFeedback{" +
        "id=" + id +
        ", organizationId=" + organizationId +
        ", accountId=" + accountId +
        ", content=" + content +
        ", createtime=" + createtime +
        "}";
    }
}
