package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 申请额外开课表
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-15
 */
@TableName("bus_course_apply")
public class CourseApply extends Model<CourseApply> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @TableField("account_id")
    private Integer accountId;
    /**
     * 用户名称
     */
    @TableField("account_name")
    private String accountName;
    /**
     * 课程ID
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;

    @TableField(exist = false)
    private String schoolName;

    /**
     * 处理状态(0未处理，1已处理)
     */
    private Integer status;
    /**
     * 申请时间
     */
    private Date createtime;

    @TableField(exist = false)
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CourseApply{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", accountName=" + accountName +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", status=" + status +
        ", createtime=" + createtime +
        "}";
    }
}
