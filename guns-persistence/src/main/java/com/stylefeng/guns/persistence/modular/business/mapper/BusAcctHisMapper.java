package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.BusAcctHis;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 支付记录 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface BusAcctHisMapper extends BaseMapper<BusAcctHis> {

    List<BusAcctHis> selectByPage(Page<BusAcctHis> page, @Param("ew") Wrapper<BusAcctHis> wrapper);
}
