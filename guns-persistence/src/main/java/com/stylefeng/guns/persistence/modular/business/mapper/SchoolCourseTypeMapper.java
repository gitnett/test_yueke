package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.SchoolCourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 机构-课程类别 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-13
 */
public interface SchoolCourseTypeMapper extends BaseMapper<SchoolCourseType> {

}
