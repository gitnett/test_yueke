package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
@TableName("bus_account")
public class Account extends Model<Account> {

	private static final long serialVersionUID = 1L;

	/**
	 * 系统编号
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户手机号
	 */
	private String phone;
	
	/**
	 * 微信openId
	 */
	@TableField("open_id")
	private String openId;
	
	/**
	 * 用户昵称
	 */
	@TableField("nick_name")
	private String nickName;
	
	/**
	 * 头像路径
	 */
	@TableField("avatar_url")
	private String avatarUrl;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 来源：0-微信公众号，1-优教云
	 */
	private Integer sources;
	/**
	 * 创建时间
	 */
	private Date createtime;
	
	/**
	 * 修改时间
	 */
	private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSources() {
		return sources;
	}

	public void setSources(Integer sources) {
		this.sources = sources;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	
	
	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", phone=" + phone + ", openId=" + openId + ", nickName=" + nickName
				+ ", avatarUrl=" + avatarUrl + ", password=" + password + ", sources=" + sources + ", createtime="
				+ createtime + ", updatetime=" + updatetime + "]";
	}
	

	
}
