package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 学校端-滚动横幅
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
@TableName("bus_school_banner")
public class SchoolBanner extends Model<SchoolBanner> {

    private static final long serialVersionUID = 1L;

    /**
     * 系统编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 横幅名称
     */
    private String title;
    /**
     * 图片路径
     */
    private String picture;
    /**
     * 排序序号
     */
    @TableField("serial_no")
    private Integer serialNo;
    /**
     * 链接
     */
    private String url;
    /**
     * 是否显示，0-否，1-是
     */
    private Integer state;
    /**
     * 学校ID
     */
    @TableField("school_id")
    private Integer schoolId;

    /**
     * 学校
     */
    @TableField(exist = false)
    private School school;
    /**
     * 创建时间
     */
    private Date createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Integer serialNo) {
        this.serialNo = serialNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public String toString() {
        return "SchoolBanner{" +
        "id=" + id +
        ", title=" + title +
        ", picture=" + picture +
        ", serialNo=" + serialNo +
        ", url=" + url +
        ", state=" + state +
        ", schoolId=" + schoolId +
        ", createtime=" + createtime +
        "}";
    }
}
