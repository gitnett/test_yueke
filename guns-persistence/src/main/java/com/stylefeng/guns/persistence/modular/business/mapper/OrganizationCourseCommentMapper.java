package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourseComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 机构-课程评价 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-08-08
 */
public interface OrganizationCourseCommentMapper extends BaseMapper<OrganizationCourseComment> {

}
