package com.stylefeng.guns.persistence.modular.business.mapper;

import com.stylefeng.guns.persistence.modular.business.model.Image;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 图片 Mapper 接口
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
public interface ImageMapper extends BaseMapper<Image> {

}
