package com.stylefeng.guns.persistence.modular.business.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 我的收藏
 * </p>
 *
 * @author huangbiao123
 * @since 2018-07-31
 */
@TableName("bus_my_collections")
public class MyCollections extends Model<MyCollections> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程主键Id
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 主表accout主键
     */
    @TableField("account_id")
    private Integer accountId;
    
    private Date createtime;
    
    private Date updatetime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "MyCollections{" +
        "id=" + id +
        ", courseId=" + courseId +
        ", accountId=" + accountId +
        ", createtime=" + createtime +
        ", updatetime=" + updatetime +
        "}";
    }
}
