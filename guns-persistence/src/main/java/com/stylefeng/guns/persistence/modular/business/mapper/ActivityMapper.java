package com.stylefeng.guns.persistence.modular.business.mapper;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.modular.business.model.Activity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动表 Mapper 接口
 * </p>
 *
 * @author LinYingQiang123
 * @since 2018-07-30
 */
public interface ActivityMapper extends BaseMapper<Activity> {


    List<Activity> selectByPage(Page<Activity> page, @Param("ew") Wrapper<Activity> wrapper);
}
