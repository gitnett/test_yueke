/**
 * 初始化开课申请详情对话框
 */
var CourseApplyInfoDlg = {
    courseApplyInfoData : {},
    editor:null,
    validateFields: {
        courseName: {
            validators: {
                notEmpty: {
                    message: '课程名称不能为空'
                }
            }
        },
        courseOnclassTime: {
            validators: {
                notEmpty: {
                    message: '上课时间不能为空'
                },
                regexp: {
                    regexp: /^[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}$/,
                    message: '上课时间未按照格式填写'
                }
            }
        },
        courseFee: {
            validators: {
                notEmpty: {
                    message: '课程费用不能为空'
                },
                regexp: {
                    regexp: /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/,
                    message: '课程费用请输入数字,最多两位小数'
                }
            }
        },
        courseTolNum: {
            validators: {
                notEmpty: {
                    message: '总报名数不能为空'
                },
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: '总报名数输入数字'
                }
            }
        },
        couresIphone: {
            validators: {
                notEmpty: {
                    message: '联系电话不能为空'
                }
            }
        },
        couresClassHour: {
            validators: {
                notEmpty: {
                    message: '课程课时不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
CourseApplyInfoDlg.clearData = function() {
    this.courseApplyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseApplyInfoDlg.set = function(key, val) {
    this.courseApplyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseApplyInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CourseApplyInfoDlg.close = function() {
    parent.layer.close(window.parent.CourseApply.layerIndex);
}

/**
 * 验证数据是否为空
 */
CourseApplyInfoDlg.validate = function () {
    $('#submitForm').data("bootstrapValidator").resetForm();
    $('#submitForm').bootstrapValidator('validate');
    return $("#submitForm").data('bootstrapValidator').isValid();
}

/**
 * 收集数据
 */
CourseApplyInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accountId')
    .set('accountName')
    .set('courseId')
    .set('courseName')
    .set('status')
    .set('createtime');
}

/**
 * 提交添加
 */
CourseApplyInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",CourseApplyInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/courseApply/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.CourseApply.table.refresh();
                CourseApplyInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/courseApply/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.CourseApply.table.refresh();
    //    CourseApplyInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseApplyInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
CourseApplyInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/courseApply/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.CourseApply.table.refresh();
    //   CourseApplyInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseApplyInfoData);
    //ajax.start();

    if (!this.validate()) {
        return;
    }

    var formData = new FormData($("#submitForm")[0]);
    formData.append("couresIntroduction",CourseApplyInfoDlg.editor.txt.html())
    $.ajax({
        url:  Feng.ctxPath + "/courseApply/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("开课成功!");
                window.parent.CourseApply.table.refresh();
                CourseApplyInfoDlg.close();
            }else{
                Feng.error("开课失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {
    Feng.initValidator("submitForm", CourseApplyInfoDlg.validateFields);
});
