/**
 * 开课申请管理初始化
 */
var CourseApply = {
    id: "CourseApplyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CourseApply.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '课程编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '学校名称', field: 'schoolName', visible: true, align: 'center', valign: 'middle'},
            {title: '课程名称', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
            {title: '申请人数', field: 'applyCount', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
CourseApply.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CourseApply.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加开课申请
 */
CourseApply.openAddCourseApply = function () {
    var index = layer.open({
        type: 2,
        title: '添加开课申请',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/courseApply/courseApply_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看开课申请详情
 */
CourseApply.openCourseApplyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '开课申请详情',
            area: ['1000px', '720px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/courseApply/courseApply_update/' + CourseApply.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开查看开课申请列表
 */
CourseApply.openCourseApplyPlay = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '开课申请列表',
            area: ['600px', '320px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/courseApply/courseApply_play/' + CourseApply.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除开课申请
 */
CourseApply.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/courseApply/delete", function (data) {
            Feng.success("删除成功!");
            CourseApply.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询开课申请列表
 */
CourseApply.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['schoolId'] = $("#schoolId").val();
    CourseApply.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CourseApply.initColumn();
    var table = new BSTable(CourseApply.id, "/courseApply/list", defaultColunms);
    table.setPaginationType("server");
    CourseApply.table = table.init();
});
