/**
 * 学校班级管理表管理初始化
 */
var ShcoolGrade = {
    id: "ShcoolGradeTable",	//表格id
    seItem: null,		//选中的条目
    setItems: '',
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ShcoolGrade.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: 'ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '学校名称', field: 'schoolName', visible: true, align: 'center', valign: 'middle'},
            {title: '年级班级名称', field: 'gcName', visible: true, align: 'center', valign: 'middle',formatter:function (value, row, index) {
                return row.gcName + row.className;
            }},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '修改时间', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ShcoolGrade.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ShcoolGrade.seItem = selected[0];
        ShcoolGrade.seItems = ""; //清空数据
        for(var i = 0 ; i < selected.length; i++){
            ShcoolGrade.seItems += selected[i].id + ",";
        }
        return true;
    }
};

/**
 * 点击添加学校班级管理表
 */
ShcoolGrade.openAddShcoolGrade = function () {
    var index = layer.open({
        type: 2,
        title: '添加学校班级管理表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/shcoolGrade/shcoolGrade_add'
    });
    this.layerIndex = index;
    
};

/**
 * 打开查看学校班级管理表详情
 */
ShcoolGrade.openShcoolGradeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '学校班级管理表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/shcoolGrade/shcoolGrade_update/' + ShcoolGrade.seItem.id
        });
        this.layerIndex = index;
    }
};


ShcoolGrade.getAllSchool = function () {
	 var ajax = new $ax(Feng.ctxPath + "/shcoolGrade/shcoolGrade/delete", function (data) {
		 
    }, function (data) {
        Feng.error("加载学校信息失败!" + data.responseJSON.message + "!");
    });
    ajax.start();
};



/**
 * 删除学校班级管理表
 */
ShcoolGrade.delete = function () {
    if (this.check()) {
    	var operation = function(){
            var ids = ShcoolGrade.seItems;
            var ajax = new $ax(Feng.ctxPath + "/shcoolGrade/delete", function (data) {
            Feng.success("删除成功!");
            ShcoolGrade.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",ids);
        ajax.start();
    };
    Feng.confirm("是否删除?",operation);
}
};

/**
 * 查询学校班级管理表列表
 */
ShcoolGrade.search = function () {
    var queryData = {};
    queryData['schoolId'] = $("#schoolId").val();
    ShcoolGrade.table.refresh({query: queryData});
};

ShcoolGrade.daochu = function(){
    var schoolId = $("#schoolId").val();
	window.location.href=Feng.ctxPath + "/shcoolGrade/daochu?schoolId="+schoolId;
}

$(function () {
    var defaultColunms = ShcoolGrade.initColumn();
    var table = new BSTable(ShcoolGrade.id, "/shcoolGrade/list", defaultColunms);
    table.setPaginationType("server");
    ShcoolGrade.table = table.init();
});
