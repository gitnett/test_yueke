/**
 * 地区信息管理初始化
 */
var Region = {
    id: "RegionTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Region.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '区域名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '排序序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Region.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Region.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加地区信息
 */
Region.openAddRegion = function () {
    var index = layer.open({
        type: 2,
        title: '添加地区信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/region/region_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看地区信息详情
 */
Region.openRegionDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '地区信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/region/region_update/' + Region.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除地区信息
 */
Region.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/region/delete", function (data) {
            Feng.success("删除成功!");
            Region.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询地区信息列表
 */
Region.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Region.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Region.initColumn();
    var table = new BSTreeTable(Region.id, "/region/list", defaultColunms);
    table.setExpandColumn(1);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pId");
    table.setExpandAll(false);
    Region.table = table.init();
});
