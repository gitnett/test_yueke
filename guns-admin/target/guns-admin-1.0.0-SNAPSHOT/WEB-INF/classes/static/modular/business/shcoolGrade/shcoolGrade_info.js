/**
 * 初始化学校班级管理表详情对话框
 */
var ShcoolGradeInfoDlg = {
    shcoolGradeInfoData : {}
};

/**
 * 清除数据
 */
ShcoolGradeInfoDlg.clearData = function() {
    this.shcoolGradeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ShcoolGradeInfoDlg.set = function(key, val) {
    this.shcoolGradeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ShcoolGradeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ShcoolGradeInfoDlg.close = function() {
    parent.layer.close(window.parent.ShcoolGrade.layerIndex);
}

/**
 * 收集数据
 */
ShcoolGradeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('schoolId')
    .set('gradeId')
    .set('classId')
    .set('gcName')
     .set('schoolName')
     .set('className')
    .set('createtime')
    .set('updatetime');
}

/**
 * 提交添加
 */
ShcoolGradeInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",ShcoolGradeInfoDlg.editor.txt.html());

    var patt = /^\d+$/;
    var start = $("#start").val();
    var end = $("#end").val();
    if(!patt.test(start)){
        return Feng.error("开始班级序号必须为数字！");
    }

    if(!patt.test(end)){
        return Feng.error("结束班级序号必须为数字！");
    }

    if (parseInt(start) > parseInt(end)) {
        return Feng.error("开始班级序号不能大于结束班级序号！");
    }


    $.ajax({
        url:  Feng.ctxPath + "/shcoolGrade/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.ShcoolGrade.table.refresh();
                ShcoolGradeInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/shcoolGrade/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.ShcoolGrade.table.refresh();
    //    ShcoolGradeInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.shcoolGradeInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
ShcoolGradeInfoDlg.editSubmit = function() {

    var serialNo = $("#serialNo").val();

    var reg =  /^[0-9]+$/;

    if (serialNo == null || serialNo == '') {
        return Feng.error("排序序号不能为空");
    }

    if (!reg.test(serialNo)) {
        return Feng.error("排序序号输入数字");
    }

    this.clearData();
    this.collectData();

    var patt = /^\d+班$/;

    var className = $("#className").val();

    if(!patt.test(className)){
        return Feng.error("班级必须为[x班]格式！");
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/shcoolGrade/update", function(data){
        if (data.code == 200) {
            Feng.success("修改成功!");
            window.parent.ShcoolGrade.table.refresh();
            ShcoolGradeInfoDlg.close();
        }else{
            Feng.error("修改失败!错误："+data.message);
        }

    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.shcoolGradeInfoData);
    ajax.start();
   
}

$(function() {

});
