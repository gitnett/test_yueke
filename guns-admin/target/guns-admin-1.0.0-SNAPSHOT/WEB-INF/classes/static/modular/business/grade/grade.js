/**
 * 年级管理初始化
 */
var Grade = {
    id: "GradeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Grade.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '年级名称', field: 'gradeName', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '修改时间', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Grade.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Grade.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加年级
 */
Grade.openAddGrade = function () {
    var index = layer.open({
        type: 2,
        title: '添加年级',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/grade/grade_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看年级详情
 */
Grade.openGradeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '年级详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/grade/grade_update/' + Grade.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除年级
 */
Grade.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = Grade.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/grade/delete", function (data) {
	            Feng.success("删除成功!");
	            Grade.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    };
		Feng.confirm("是否删除年级:" + Grade.seItem.gradeName + "?",operation);
    }
};

/**
 * 查询年级列表
 */
Grade.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Grade.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Grade.initColumn();
    var table = new BSTable(Grade.id, "/grade/list", defaultColunms);
    table.setPaginationType("server");
    Grade.table = table.init();
});
