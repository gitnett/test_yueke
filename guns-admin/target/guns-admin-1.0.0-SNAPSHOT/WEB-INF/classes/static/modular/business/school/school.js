/**
 * 校区管理管理初始化
 */
var School = {
    id: "SchoolTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
School.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '学校名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '学校地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '排序序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
School.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        School.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加校区管理
 */
School.openAddSchool = function () {
    var index = layer.open({
        type: 2,
        title: '添加校区管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/school/school_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看校区管理详情
 */
School.openSchoolDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '校区管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/school/school_update/' + School.seItem.id
        });
        this.layerIndex = index;
    }
};


/**
 * 删除校区管理
 */
School.delete = function () {
	
    if (this.check()) {
    	var operation = function(){
	        var id = School.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/school/delete", function (data) {
	            Feng.success("删除成功!");
	            School.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
	    };
	    Feng.confirm("是否删除学校:" + School.seItem.name + "?",operation);
    }
};


/**
 * 分享校区
 */
School.share = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '校区分享详情',
            area: ['500px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/school/share/' + School.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 查询校区管理列表
 */
School.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    School.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = School.initColumn();
    var table = new BSTable(School.id, "/school/list", defaultColunms);
    table.setPaginationType("server");
    School.table = table.init();
});
