/**
 * 横幅信息管理初始化
 */
var SchoolBanner = {
    id: "SchoolBannerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SchoolBanner.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '学校名称', field: 'school', visible: true, align: 'center', valign: 'middle',formatter:function (value, row, index) {
                if (value) {
                    return value.name;
                }else{
                    return "未知学校";
                }
        }},
        {title: '横幅名称', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '排序序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
        {title: '链接', field: 'url', visible: true, align: 'center', valign: 'middle'},
        {title: '是否显示', field: 'state', visible: true, align: 'center', valign: 'middle',formatter:function (value,row,index) {
            if (value == 0) {
                return "否";
            } else {
                return "是";
            }
        }},
        {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SchoolBanner.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SchoolBanner.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加横幅信息
 */
SchoolBanner.openAddSchoolBanner = function () {
    var index = layer.open({
        type: 2,
        title: '添加横幅信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/schoolBanner/schoolBanner_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看横幅信息详情
 */
SchoolBanner.openSchoolBannerDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '横幅信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/schoolBanner/schoolBanner_update/' + SchoolBanner.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除横幅信息
 */
SchoolBanner.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = SchoolBanner.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/schoolBanner/delete", function (data) {
	            Feng.success("删除成功!");
	            SchoolBanner.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    };
    	Feng.confirm("是否删除横幅:" + SchoolBanner.seItem.title + "?",operation);
    }
};

/**
 * 查询横幅信息列表
 */
SchoolBanner.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['schoolId'] = $("#schoolId").val();
    SchoolBanner.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SchoolBanner.initColumn();
    var table = new BSTable(SchoolBanner.id, "/schoolBanner/list", defaultColunms);
    table.setPaginationType("server");
    SchoolBanner.table = table.init();
});
