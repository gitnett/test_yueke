/**
 * 机构信息管理初始化
 */
var Organization = {
    id: "OrganizationTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Organization.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '机构名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '机构地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Organization.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Organization.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加机构信息
 */
Organization.openAddOrganization = function () {
    var index = layer.open({
        type: 2,
        title: '添加机构信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organization/organization_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看机构信息详情
 */
Organization.openOrganizationDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '机构信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organization/organization_update/' + Organization.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除机构信息
 */
Organization.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = Organization.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/organization/delete", function (data) {
	            Feng.success("删除成功!");
	            Organization.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    }
    	Feng.confirm("是否删除机构:" + Organization.seItem.name + "?",operation);
    	
    };
};

/**
 * 查询机构信息列表
 */
Organization.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Organization.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Organization.initColumn();
    var table = new BSTable(Organization.id, "/organization/list", defaultColunms);
    table.setPaginationType("server");
    Organization.table = table.init();
});
