/**
 * 初始化课程信息详情对话框
 */
var OrganizationCourseInfoDlg = {
    organizationCourseInfoData : {},
    editor:null
};

/**
 * 清除数据
 */
OrganizationCourseInfoDlg.clearData = function() {
    this.organizationCourseInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationCourseInfoDlg.set = function(key, val) {
    this.organizationCourseInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationCourseInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganizationCourseInfoDlg.close = function() {
    parent.layer.close(window.parent.OrganizationCourse.layerIndex);
}

/**
 * 收集数据
 */
OrganizationCourseInfoDlg.collectData = function() {
    this
    .set('id')
    .set('picture')
    .set('title')
    .set('totalCount')
    .set('joinCount')
    .set('suitableTarget')
    .set('courseTime')
    .set('address')
    .set('phone')
    .set('content')
    .set('state')
    .set('price')
    .set('organizationId')
    .set('serialNo')
    .set('createtime');
}

/**
 * 提交添加
 */
OrganizationCourseInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    formData.append("content",OrganizationCourseInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organizationCourse/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.OrganizationCourse.table.refresh();
                OrganizationCourseInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationCourse/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.OrganizationCourse.table.refresh();
    //    OrganizationCourseInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationCourseInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
OrganizationCourseInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationCourse/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.OrganizationCourse.table.refresh();
    //   OrganizationCourseInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationCourseInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);

    formData.append("content",OrganizationCourseInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organizationCourse/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.OrganizationCourse.table.refresh();
                OrganizationCourseInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
