/**
 * 课程信息管理初始化
 */
var OrganizationCourse = {
    id: "OrganizationCourseTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OrganizationCourse.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '机构ID', field: 'organizationId', visible: true, align: 'center', valign: 'middle'},
        {title: '课程名称', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '总参与人数', field: 'totalCount', visible: true, align: 'center', valign: 'middle'},
        {title: '现参与人数', field: 'joinCount', visible: true, align: 'center', valign: 'middle'},
        {title: '课程时间', field: 'courseTime', visible: true, align: 'center', valign: 'middle'},
        {title: '咨询电话', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '是否发布', field: 'state', visible: true, align: 'center', valign: 'middle'},
        {title: '课程价格', field: 'price', visible: true, align: 'center', valign: 'middle'},
        {title: '排序序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OrganizationCourse.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OrganizationCourse.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加课程信息
 */
OrganizationCourse.openAddOrganizationCourse = function () {
    var index = layer.open({
        type: 2,
        title: '添加课程信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organizationCourse/organizationCourse_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看课程信息详情
 */
OrganizationCourse.openOrganizationCourseDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '课程信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organizationCourse/organizationCourse_update/' + OrganizationCourse.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除课程信息
 */
OrganizationCourse.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = OrganizationCourse.seItem.id;
        var ajax = new $ax(Feng.ctxPath + "/organizationCourse/delete", function (data) {
            Feng.success("删除成功!");
            OrganizationCourse.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",id);
        ajax.start();
    };
    Feng.confirm("是否删除课程:" + OrganizationCourse.seItem.title + "?",operation);
    }
};

/**
 * 查询课程信息列表
 */
OrganizationCourse.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['organizationId'] = $("#organizationId").val();
    OrganizationCourse.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = OrganizationCourse.initColumn();
    var table = new BSTable(OrganizationCourse.id, "/organizationCourse/list", defaultColunms);
    table.setPaginationType("server");
    OrganizationCourse.table = table.init();
});
