/**
 * 初始化班级详情对话框
 */
var ClassFeedbackInfoDlg = {
    classFeedbackInfoData : {}
};

/**
 * 清除数据
 */
ClassFeedbackInfoDlg.clearData = function() {
    this.classFeedbackInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ClassFeedbackInfoDlg.set = function(key, val) {
    this.classFeedbackInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ClassFeedbackInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ClassFeedbackInfoDlg.close = function() {
    parent.layer.close(window.parent.ClassFeedback.layerIndex);
}

/**
 * 收集数据
 */
ClassFeedbackInfoDlg.collectData = function() {
    this
    .set('id')
    .set('feedbackContent')
    .set('accountId')
    .set('createtime')
    .set('updatetime');
}

/**
 * 提交添加
 */
ClassFeedbackInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",ClassFeedbackInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/classFeedback/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.ClassFeedback.table.refresh();
                ClassFeedbackInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/classFeedback/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.ClassFeedback.table.refresh();
    //    ClassFeedbackInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.classFeedbackInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
ClassFeedbackInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/classFeedback/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.ClassFeedback.table.refresh();
    //   ClassFeedbackInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.classFeedbackInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/classFeedback/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.ClassFeedback.table.refresh();
                ClassFeedbackInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
