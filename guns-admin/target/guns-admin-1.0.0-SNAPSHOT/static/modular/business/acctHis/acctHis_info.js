/**
 * 初始化订单记录详情对话框
 */
var AcctHisInfoDlg = {
    acctHisInfoData : {},
    editor: null
};

/**
 * 清除数据
 */
AcctHisInfoDlg.clearData = function() {
    this.acctHisInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AcctHisInfoDlg.set = function(key, val) {
    this.acctHisInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AcctHisInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AcctHisInfoDlg.close = function() {
    parent.layer.close(window.parent.AcctHis.layerIndex);
}

/**
 * 收集数据
 */
AcctHisInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accountId')
    .set('courseId')
    .set('courseName')
    .set('accountName')
    .set('amount')
    .set('payType')
    .set('acctChannel')
    .set('schoolName')
    .set('gradeClassName')
    .set('status')
    .set('flowNo')
    .set('busFlowNo')
    .set('msg')
    .set('createtime')
    .set('updatetime');
}



$(function() {

});
