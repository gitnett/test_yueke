/**
 * 初始化课后反馈详情对话框
 */
var OrganizationClassFeedbackInfoDlg = {
    organizationClassFeedbackInfoData : {}
};

/**
 * 清除数据
 */
OrganizationClassFeedbackInfoDlg.clearData = function() {
    this.organizationClassFeedbackInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationClassFeedbackInfoDlg.set = function(key, val) {
    this.organizationClassFeedbackInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationClassFeedbackInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganizationClassFeedbackInfoDlg.close = function() {
    parent.layer.close(window.parent.OrganizationClassFeedback.layerIndex);
}

/**
 * 收集数据
 */
OrganizationClassFeedbackInfoDlg.collectData = function() {
    this
    .set('id')
    .set('organizationId')
    .set('accountId')
    .set('content')
    .set('createtime');
}

/**
 * 提交添加
 */
OrganizationClassFeedbackInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",OrganizationClassFeedbackInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organizationClassFeedback/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.OrganizationClassFeedback.table.refresh();
                OrganizationClassFeedbackInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationClassFeedback/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.OrganizationClassFeedback.table.refresh();
    //    OrganizationClassFeedbackInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationClassFeedbackInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
OrganizationClassFeedbackInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationClassFeedback/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.OrganizationClassFeedback.table.refresh();
    //   OrganizationClassFeedbackInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationClassFeedbackInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/organizationClassFeedback/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.OrganizationClassFeedback.table.refresh();
                OrganizationClassFeedbackInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
