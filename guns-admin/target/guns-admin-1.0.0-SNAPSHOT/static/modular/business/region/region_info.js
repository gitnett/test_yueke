/**
 * 初始化地区信息详情对话框
 */
var RegionInfoDlg = {
    regionInfoData : {}
};

/**
 * 清除数据
 */
RegionInfoDlg.clearData = function() {
    this.regionInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RegionInfoDlg.set = function(key, val) {
    this.regionInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RegionInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RegionInfoDlg.close = function() {
    parent.layer.close(window.parent.Region.layerIndex);
}

/**
 * 收集数据
 */
RegionInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('pId')
    .set('createtime');
}

/**
 * 提交添加
 */
RegionInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",RegionInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/region/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Region.table.refresh();
                RegionInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/region/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Region.table.refresh();
    //    RegionInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.regionInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
RegionInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/region/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Region.table.refresh();
    //   RegionInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.regionInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/region/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.Region.table.refresh();
                RegionInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
