/**
 * 班级管理初始化
 */
var Image = {
    id: "ImageTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Image.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '图片名称', field: 'imageName', visible: true, align: 'center', valign: 'middle'},
            {title: '图片路径', field: 'imagePath', visible: true, align: 'center', valign: 'middle'},
            {title: '图片类型', field: 'imageType', visible: true, align: 'center', valign: 'middle'},
            {title: '图片大小', field: 'imageSize', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Image.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Image.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加班级
 */
Image.openAddImage = function () {
    var index = layer.open({
        type: 2,
        title: '添加班级',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/image/image_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看班级详情
 */
Image.openImageDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/image/image_update/' + Image.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除班级
 */
Image.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/image/delete", function (data) {
            Feng.success("删除成功!");
            Image.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("imageId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询班级列表
 */
Image.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Image.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Image.initColumn();
    var table = new BSTable(Image.id, "/image/list", defaultColunms);
    table.setPaginationType("server");
    Image.table = table.init();
});
