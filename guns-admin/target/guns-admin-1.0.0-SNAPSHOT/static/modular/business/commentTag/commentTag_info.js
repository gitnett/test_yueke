/**
 * 初始化评论标签详情对话框
 */
var CommentTagInfoDlg = {
    commentTagInfoData : {}
};

/**
 * 清除数据
 */
CommentTagInfoDlg.clearData = function() {
    this.commentTagInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CommentTagInfoDlg.set = function(key, val) {
    this.commentTagInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CommentTagInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CommentTagInfoDlg.close = function() {
    parent.layer.close(window.parent.CommentTag.layerIndex);
}

/**
 * 收集数据
 */
CommentTagInfoDlg.collectData = function() {
    this
    .set('id')
    .set('content')
    .set('serialNo')
    .set('createtime');
}

/**
 * 提交添加
 */
CommentTagInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",CommentTagInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/commentTag/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.CommentTag.table.refresh();
                CommentTagInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/commentTag/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.CommentTag.table.refresh();
    //    CommentTagInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.commentTagInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
CommentTagInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/commentTag/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.CommentTag.table.refresh();
    //   CommentTagInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.commentTagInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/commentTag/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.CommentTag.table.refresh();
                CommentTagInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
