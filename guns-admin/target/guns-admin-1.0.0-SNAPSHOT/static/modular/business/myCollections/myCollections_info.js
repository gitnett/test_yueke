/**
 * 初始化班级详情对话框
 */
var MyCollectionsInfoDlg = {
    myCollectionsInfoData : {}
};

/**
 * 清除数据
 */
MyCollectionsInfoDlg.clearData = function() {
    this.myCollectionsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MyCollectionsInfoDlg.set = function(key, val) {
    this.myCollectionsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MyCollectionsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MyCollectionsInfoDlg.close = function() {
    parent.layer.close(window.parent.MyCollections.layerIndex);
}

/**
 * 收集数据
 */
MyCollectionsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('courseId')
    .set('accountId')
    .set('createtime')
    .set('updatetime');
}

/**
 * 提交添加
 */
MyCollectionsInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",MyCollectionsInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/myCollections/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.MyCollections.table.refresh();
                MyCollectionsInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/myCollections/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.MyCollections.table.refresh();
    //    MyCollectionsInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.myCollectionsInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
MyCollectionsInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/myCollections/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.MyCollections.table.refresh();
    //   MyCollectionsInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.myCollectionsInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/myCollections/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.MyCollections.table.refresh();
                MyCollectionsInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
