/**
 * 班级管理初始化
 */
var MyCollections = {
    id: "MyCollectionsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
MyCollections.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '课程主键Id', field: 'courseId', visible: true, align: 'center', valign: 'middle'},
            {title: '主表accout主键', field: 'accountId', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
MyCollections.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        MyCollections.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加班级
 */
MyCollections.openAddMyCollections = function () {
    var index = layer.open({
        type: 2,
        title: '添加班级',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/myCollections/myCollections_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看班级详情
 */
MyCollections.openMyCollectionsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/myCollections/myCollections_update/' + MyCollections.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除班级
 */
MyCollections.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/myCollections/delete", function (data) {
            Feng.success("删除成功!");
            MyCollections.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询班级列表
 */
MyCollections.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    MyCollections.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = MyCollections.initColumn();
    var table = new BSTable(MyCollections.id, "/myCollections/list", defaultColunms);
    table.setPaginationType("server");
    MyCollections.table = table.init();
});
