/**
 * 初始化评论信息详情对话框
 */
var OrganizationCourseCommentInfoDlg = {
    organizationCourseCommentInfoData : {}
};

/**
 * 清除数据
 */
OrganizationCourseCommentInfoDlg.clearData = function() {
    this.organizationCourseCommentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationCourseCommentInfoDlg.set = function(key, val) {
    this.organizationCourseCommentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationCourseCommentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganizationCourseCommentInfoDlg.close = function() {
    parent.layer.close(window.parent.OrganizationCourseComment.layerIndex);
}

/**
 * 收集数据
 */
OrganizationCourseCommentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accountId')
    .set('accountName')
    .set('courseId')
    .set('courseName')
    .set('organizationId')
    .set('organizationName')
    .set('content')
    .set('starCount')
    .set('createtime');
}

/**
 * 提交添加
 */
OrganizationCourseCommentInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",OrganizationCourseCommentInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organizationCourseComment/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.OrganizationCourseComment.table.refresh();
                OrganizationCourseCommentInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationCourseComment/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.OrganizationCourseComment.table.refresh();
    //    OrganizationCourseCommentInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationCourseCommentInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
OrganizationCourseCommentInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationCourseComment/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.OrganizationCourseComment.table.refresh();
    //   OrganizationCourseCommentInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationCourseCommentInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/organizationCourseComment/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.OrganizationCourseComment.table.refresh();
                OrganizationCourseCommentInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
