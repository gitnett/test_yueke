/**
 * 评论信息管理初始化
 */
var SchoolCourseType = {
    id: "SchoolCourseTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SchoolCourseType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '课程类别', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SchoolCourseType.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SchoolCourseType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加评论信息
 */
SchoolCourseType.openAddSchoolCourseType = function () {
    var index = layer.open({
        type: 2,
        title: '添加课程类别',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/schoolCourseType/schoolCourseType_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看评论信息详情
 */
SchoolCourseType.openSchoolCourseTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '课程类别详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/schoolCourseType/schoolCourseType_update/' + SchoolCourseType.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除评论信息
 */
SchoolCourseType.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = SchoolCourseType.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/schoolCourseType/delete", function (data) {
	            Feng.success("删除成功!");
	            SchoolCourseType.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    };
     	Feng.confirm("是否删除类别:" + SchoolCourseType.seItem.name + "?",operation);
    }
};

/**
 * 查询评论信息列表
 */
SchoolCourseType.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SchoolCourseType.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SchoolCourseType.initColumn();
    var table = new BSTable(SchoolCourseType.id, "/schoolCourseType/list", defaultColunms);
    table.setPaginationType("server");
    SchoolCourseType.table = table.init();
});
