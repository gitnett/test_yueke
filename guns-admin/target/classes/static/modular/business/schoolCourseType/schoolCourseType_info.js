/**
 * 初始化评论信息详情对话框
 */
var SchoolCourseTypeInfoDlg = {
    schoolCourseTypeInfoData : {}
};

/**
 * 清除数据
 */
SchoolCourseTypeInfoDlg.clearData = function() {
    this.schoolCourseTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolCourseTypeInfoDlg.set = function(key, val) {
    this.schoolCourseTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolCourseTypeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SchoolCourseTypeInfoDlg.close = function() {
    parent.layer.close(window.parent.SchoolCourseType.layerIndex);
}

/**
 * 收集数据
 */
SchoolCourseTypeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('serialNo')
    .set('createtime');
}

/**
 * 提交添加
 */
SchoolCourseTypeInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",SchoolCourseTypeInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/schoolCourseType/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.SchoolCourseType.table.refresh();
                SchoolCourseTypeInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/schoolCourseType/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.SchoolCourseType.table.refresh();
    //    SchoolCourseTypeInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolCourseTypeInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
SchoolCourseTypeInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/schoolCourseType/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.SchoolCourseType.table.refresh();
    //   SchoolCourseTypeInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolCourseTypeInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/schoolCourseType/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.SchoolCourseType.table.refresh();
                SchoolCourseTypeInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
