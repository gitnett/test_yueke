/**
 * 初始化活动信息详情对话框
 */
var ActivityInfoDlg = {
    activityInfoData : {},
    editor:null
};

/**
 * 清除数据
 */
ActivityInfoDlg.clearData = function() {
    this.activityInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ActivityInfoDlg.set = function(key, val) {
    this.activityInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ActivityInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ActivityInfoDlg.close = function() {
    parent.layer.close(window.parent.Activity.layerIndex);
}

/**
 * 收集数据
 */
ActivityInfoDlg.collectData = function() {
    this
    .set('id')
    .set('picture')
    .set('title')
    .set('totalCount')
    .set('joinCount')
    .set('suitableTarget')
    .set('activityTime')
    .set('address')
    .set('phone')
    .set('content')
    .set('state')
    .set('price')
    .set('createtime');
}

/**
 * 提交添加
 */
ActivityInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    formData.append("content",ActivityInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/activity/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Activity.table.refresh();
                ActivityInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/activity/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Activity.table.refresh();
    //    ActivityInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.activityInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
ActivityInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/activity/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Activity.table.refresh();
    //   ActivityInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.activityInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);

    formData.append("content",ActivityInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/activity/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.Activity.table.refresh();
                ActivityInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
