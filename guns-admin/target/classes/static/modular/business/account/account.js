/**
 * 用户列表管理初始化
 */
var Account = {
    id: "AccountTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Account.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '微信昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'},
            {title: '来源', field: 'sources', visible: true, align: 'center', valign: 'middle',formatter:function (value, row, index) {
                    if (value == 0) {
                        return "微信";
                    }else{
                        return "优教云";
                    }
                }},//0-微信公众号，1-优教云
            {title: '注册时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Account.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Account.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户列表
 */
Account.openAddAccount = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户列表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/account/account_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户列表详情
 */
Account.openAccountDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户列表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/account/account_update/' + Account.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开查看用户孩子详情
 */
Account.openAccountChilds = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '孩子列表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/account/account_childs/' + Account.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户列表
 */
Account.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/account/delete", function (data) {
            Feng.success("删除成功!");
            Account.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accountId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户列表列表
 */
Account.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Account.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Account.initColumn();
    var table = new BSTable(Account.id, "/account/list", defaultColunms);
    table.setPaginationType("server");
    Account.table = table.init();
});
