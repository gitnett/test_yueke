/**
 * 班级管理初始化
 */
var Course = {
    id: "CourseTable",	//表格id
    seItem: null,		//选中的条目
    setItems: '',//多个选中
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Course.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        {title: 'ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '课程名称', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
        {
            title: '课程状态',
            field: 'courseStatus',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: function (value, row, index) {
                if (value == 0) {
                    return "发布";
                } else {
                    return "下架";
                }
            }
        },
        {title: '课程分类', field: 'typeName', visible: true, align: 'center', valign: 'middle'},
        {title: '上课星期', field: 'courseOnclassWeek', visible: true, align: 'center', valign: 'middle'},
        {title: '上课时间', field: 'courseOnclassTime', visible: true, align: 'center', valign: 'middle'},
        {title: '课程费用', field: 'courseFee', visible: true, align: 'center', valign: 'middle'},
        {title: '总报名人数', field: 'courseTolNum', visible: true, align: 'center', valign: 'middle'},
        {title: '培训学期', field: 'couresTrainTerm', visible: true, align: 'center', valign: 'middle'},
        {title: '上课教师', field: 'couresTeacher', visible: true, align: 'center', valign: 'middle'},
        {title: '上课教室', field: 'couresClassroom', visible: true, align: 'center', valign: 'middle'},
        {title: '开课时间', field: 'couresOpenclassTime', visible: true, align: 'center', valign: 'middle'},
        {title: '报名开始时间', field: 'couresSignupStarttime', visible: true, align: 'center', valign: 'middle'},
        {title: '报名结束时间', field: 'couresSignupEndtime', visible: true, align: 'center', valign: 'middle'},
        {title: '学校', field: 'schoolName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Course.check = function () {
    var selected = $('#CourseTable').bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Course.seItem = selected[0];
        Course.setItems = ""; //清空数据
        for (var i = 0; i < selected.length; i++) {
            Course.setItems += selected[i].id + ",";
        }
        return true;
    }
};

/**
 * 点击添加班级
 */
Course.openAddCourse = function () {
    var index = layer.open({
        type: 2,
        title: '添加课程',
        area: ['1000px', '720px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/course/course_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看班级详情
 */
Course.openCourseDetail = function () {
    if (this.check()) {
        if($('#' + this.id).bootstrapTable('getSelections').length > 1){
            return Feng.error("只能选中一条记录修改！");
        }
        var index = layer.open({
            type: 2,
            title: '编辑课程',
            area: ['1000px', '720px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/course/course_update/' + Course.seItem.id
        });
        this.layerIndex = index;
    }
};

Course.copy = function () {
    if (this.check()) {
        if($('#' + this.id).bootstrapTable('getSelections').length > 1){
            return Feng.error("只能选中一条记录复制！");
        }
        var index = layer.open({
            type: 2,
            title: '复制课程',
            area: ['1000px', '720px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/course/course_copy/' + Course.seItem.id
        });
        this.layerIndex = index;
    }
};

Course.daochu = function () {
	window.location.href=Feng.ctxPath + "/course/daochu?schoolId=" + $("#schoolId").val();
}

Course.orderExport = function () {
    //检查是否选中
    if (this.check()) {
        window.location.href=Feng.ctxPath + "/course/orderExport?ids=" + Course.setItems;
    }
}

/**
 * 删除班级
 */
Course.delete = function () {
    if (this.check()) {
    	var operation = function(){
    	var id = Course.seItems;
        var ajax = new $ax(Feng.ctxPath + "/course/delete", function (data) {
            Feng.success("删除成功!");
            Course.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",Course.setItems);
        ajax.start();
    };
    	Feng.confirm("是否删除课程:" + Course.seItem.courseName + "?",operation);
    }
};

/**
 * 查询班级列表
 */
Course.search = function () {
    var queryData = {};
    queryData['schoolId'] = $("#schoolId").val();
    Course.table.refresh({query: queryData});
};

/**
 * 分享课程
 */
Course.share = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '课程分享详情',
            area: ['500px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/course/share/' + Course.seItem.id
        });
        this.layerIndex = index;
    }
};

$(function () {
    var defaultColunms = Course.initColumn();
    var table = new BSTable(Course.id, "/course/list", defaultColunms);
    table.setPaginationType("server");
    Course.table = table.init();
});
