/**
 * 初始化班级详情对话框
 */
var CourseInfoDlg = {
    courseInfoData : {},
    validateFields: {
        courseName: {
            validators: {
                notEmpty: {
                    message: '课程名称不能为空'
                }
            }
        },
        courseOnclassTime: {
            validators: {
                notEmpty: {
                    message: '上课时间不能为空'
                },
                regexp: {
                    regexp: /^[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}$/,
                    message: '上课时间未按照格式填写并且符号必须为英文'
                }
            }
        },
        courseFee: {
            validators: {
                notEmpty: {
                    message: '课程费用不能为空'
                },
                regexp: {
                    regexp: /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/,
                    message: '课程费用请输入数字,最多两位小数'
                }
            }
        },
        courseTolNum: {
            validators: {
                notEmpty: {
                    message: '总报名数不能为空'
                },
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: '总报名数输入数字'
                }
            }
        },
        couresIphone: {
            validators: {
                notEmpty: {
                    message: '联系电话不能为空'
                }
            }
        },
        couresClassHour: {
            validators: {
                notEmpty: {
                    message: '课程课时不能为空'
                }
            }
        },
        couresOpenclassTime: {
            validators: {
                notEmpty: {
                    message: '开课时间不能为空'
                }
            }
        },
        couresSignupStarttime: {
            validators: {
                notEmpty: {
                    message: '报名开始时间不能为空'
                }
            }
        },
        couresSignupEndtime: {
            validators: {
                notEmpty: {
                    message: '报名结束时间不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
CourseInfoDlg.clearData = function() {
    this.courseInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseInfoDlg.set = function(key, val) {
    this.courseInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 验证数据是否为空
 */
CourseInfoDlg.validate = function () {
    $('#submitForm').data("bootstrapValidator").resetForm();
    $('#submitForm').bootstrapValidator('validate');
    return $("#submitForm").data('bootstrapValidator').isValid();
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CourseInfoDlg.close = function() {
    parent.layer.close(window.parent.Course.layerIndex);
}

/**
 * 收集数据
 */
CourseInfoDlg.collectData = function() {
    this
    .set('id')
    .set('courseName')
    .set('startCourseGrade')
    .set('endCourseGrade')
    .set('courseOnclassTime')
    .set('courseOnclassWeek')
    .set('courseFee')
    .set('courseQuota')
    .set('courseTolNum')
    .set('typeId')
    .set('couresTrainTerm')
    .set('couresTeacher')
    .set('couresClassroom')
    .set('couresIphone')
    .set('couresOpenclassTime')
    .set('couresSignupStarttime')
    .set('couresSignupEndtime')
    .set('couresIntroduction')
    .set('couresClassHour')
    .set('couresImgPath')
    .set('schoolName')
    .set('schoolId')
    .set('createtime')
    .set('bannerPath')
    .set('courseStatus')
    .set('updatetime');
}

/**
 * 提交添加
 */
CourseInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var formData = new FormData($("#submitForm")[0]);
    formData.append("couresIntroduction",CourseInfoDlg.editor.txt.html())
    //详情
    //formData.append("content",CourseInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/course/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Course.table.refresh();
                CourseInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/course/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Course.table.refresh();
    //    CourseInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
CourseInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/course/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Course.table.refresh();
    //   CourseInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseInfoData);
    //ajax.start();

    //对比报名人数
    if(parseInt(courseTolNum) > parseInt($("#courseTolNum").val())){
        return Feng.error("当前报名人数不能小于之前报名人数,之前报名人数[" +courseTolNum + "]" )
    }

    var formData = new FormData($("#submitForm")[0]);
    formData.append("couresIntroduction",CourseInfoDlg.editor.txt.html())
    $.ajax({
        url:  Feng.ctxPath + "/course/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.Course.table.refresh();
                CourseInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}


CourseInfoDlg.copySubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/course/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Course.table.refresh();
    //   CourseInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseInfoData);
    //ajax.start();

    //对比报名人数
    if(parseInt(courseTolNum) > parseInt($("#courseTolNum").val())){
        return Feng.error("当前报名人数不能小于之前报名人数,之前报名人数[" +courseTolNum + "]" )
    }

    var formData = new FormData($("#submitForm")[0]);
    formData.append("couresIntroduction",CourseInfoDlg.editor.txt.html())
    $.ajax({
        url:  Feng.ctxPath + "/course/addCopy" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("复制成功!");
                window.parent.Course.table.refresh();
                CourseInfoDlg.close();
            }else{
                Feng.error("复制失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
            Feng.error("复制失败!");
        }
    });
}

$(function() {
    Feng.initValidator("submitForm", CourseInfoDlg.validateFields);

});
