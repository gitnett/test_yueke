/**
 * 班级管理初始化
 */
var ClassFeedback = {
    id: "ClassFeedbackTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ClassFeedback.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '学校名称', field: 'school', visible: true, align: 'center', valign: 'middle',formatter : function (value, row, index) {
            if (value) {
                return value.name;
            }else{
                return "未知学校";
            }
        }},
        {title: '用户姓名', field: 'accountName', visible: true, align: 'center', valign: 'middle'},
        {title: '反馈内容', field: 'feedbackContent', visible: true, align: 'center', valign: 'middle'},
        {title: '提交时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
ClassFeedback.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ClassFeedback.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加班级
 */
ClassFeedback.openAddClassFeedback = function () {
    var index = layer.open({
        type: 2,
        title: '添加班级',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/classFeedback/classFeedback_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看班级详情
 */
ClassFeedback.openClassFeedbackDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/classFeedback/classFeedback_update/' + ClassFeedback.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除班级
 */
ClassFeedback.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/classFeedback/delete", function (data) {
            Feng.success("删除成功!");
            ClassFeedback.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("classFeedbackId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询班级列表
 */
ClassFeedback.search = function () {
    var queryData = {};
    queryData['accountName'] = $("#accountName").val();
    queryData['schoolId'] = $("#schoolId").val();
    ClassFeedback.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ClassFeedback.initColumn();
    var table = new BSTable(ClassFeedback.id, "/classFeedback/list", defaultColunms);
    table.setPaginationType("server");
    ClassFeedback.table = table.init();
});
