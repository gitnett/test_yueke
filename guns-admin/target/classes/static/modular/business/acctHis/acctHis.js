/**
 * 订单记录管理初始化
 */
var AcctHis = {
    id: "AcctHisTable",	//表格id
    seItem: null,		//选中的条目
    setItems: '',
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AcctHis.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '课程名称', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
        {title: '用户姓名', field: 'accountName', visible: true, align: 'center', valign: 'middle'},
        {title: '用户手机号', field: 'accountPhone', visible: true, align: 'center', valign: 'middle'},
        {title: '身份证号', field: 'idCard', visible: true, align: 'center', valign: 'middle'},
        {title: '孩子姓名', field: 'childName', visible: true, align: 'center', valign: 'middle'},
        {title: '支付金额', field: 'amount', visible: true, align: 'center', valign: 'middle'},
        {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle'},
        {title: '来源渠道', field: 'acctChannel', visible: true, align: 'center', valign: 'middle'},
        {title: '学校名称', field: 'schoolName', visible: true, align: 'center', valign: 'middle'},
        {title: '年级班级', field: 'gradeClassName', visible: true, align: 'center', valign: 'middle'},
        {title: '支付状态', field: 'status', visible: true, align: 'center', valign: 'middle'},
        {title: '平台流水号', field: 'flowNo', visible: true, align: 'center', valign: 'middle'},
        {title: '第三方流水号', field: 'busFlowNo', visible: true, align: 'center', valign: 'middle'},
        {title: '紧急联系人', field: 'phoneTwo', visible: true, align: 'center', valign: 'middle'},
        {title: '回家方式', field: 'levelType', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
AcctHis.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        AcctHis.seItem = selected[0];
        AcctHis.setItems = ""; //清空数据
        for (var i = 0; i < selected.length; i++) {
            AcctHis.setItems += selected[i].id + ",";
        }
        return true;
    }
};


/**
 *课程详情
 */
AcctHis.openAcctHisDetail = function () {
    if (this.check()) {
        if($('#' + this.id).bootstrapTable('getSelections').length > 1){
            return Feng.error("只能选中一条记录查看！");
        }
        var index = layer.open({
            type: 2,
            title: '课程详情',
            area: ['1200px', '620px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/acctHis/detail/' + AcctHis.seItem.courseId
        });
        this.layerIndex = index;
    }
};

/**
 *退款操作
 */
AcctHis.openBackFee = function () {
    if (this.check()) {
        var operation = function () {
            var ids = AcctHis.setItems;
            var ajax = new $ax(Feng.ctxPath + "/acctHis/backFee", function (data) {
                Feng.success("退款操作成功!系统会有一定延时,请耐心等待");
                AcctHis.table.refresh();
            }, function (data) {
                Feng.error("退款操作失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids",ids);
            ajax.start();
        }
        Feng.confirm("是否操作退款?",operation);
    }
};


AcctHis.hisexport = function(){
    window.location.href=Feng.ctxPath + "/acctHis/hisExceport?courseName="+$("#courseName").val()+"&gradeClassName="+$("#gradeClassName").val()
    					+"&schoolId="+$("#schoolId").val()+"&accountName="+$("#accountName").val()
    					+"&accountPhone="+$("#accountPhone").val()+"&startTime="+$("#startTime").val()+"&endTime"+$("#endTime").val();

};



/**
 * 查询订单记录列表
 */
AcctHis.search = function () {
 	var queryData = {};
    queryData['courseName'] = $("#courseName").val();
    queryData['gradeClassName'] = $("#gradeClassName").val();
    queryData['schoolId'] = $("#schoolId").val();
    queryData['accountName'] = $("#accountName").val();
    queryData['accountPhone'] = $("#accountPhone").val();
    queryData['startTime'] = $("#startTime").val();
    queryData['endTime'] = $("#endTime").val();
    AcctHis.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AcctHis.initColumn();
    var table = new BSTable(AcctHis.id, "/acctHis/list", defaultColunms);
    table.setPaginationType("server");
    AcctHis.table = table.init();
});
