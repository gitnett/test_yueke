/**
 * 横幅信息管理初始化
 */
var OrganizationBanner = {
    id: "OrganizationBannerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OrganizationBanner.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '横幅名称', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '排序序号', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
            {title: '链接', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '是否显示', field: 'state', visible: true, align: 'center', valign: 'middle',formatter: function (value, row, index) {
                    if (value == 0) {
                        return "未上架";
                    }else{
                        return "已上架";
                    }
            }},
            {title: '机构名称', field: 'organization', visible: true, align: 'center', valign: 'middle',formatter: function (value, row, index) {
                    if (value) {
                        return value.name;
                    } else {
                        return "未知机构";
                    }
            }},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OrganizationBanner.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OrganizationBanner.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加横幅信息
 */
OrganizationBanner.openAddOrganizationBanner = function () {
    var index = layer.open({
        type: 2,
        title: '添加横幅信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organizationBanner/organizationBanner_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看横幅信息详情
 */
OrganizationBanner.openOrganizationBannerDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '横幅信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organizationBanner/organizationBanner_update/' + OrganizationBanner.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除横幅信息
 */
OrganizationBanner.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/organizationBanner/delete", function (data) {
            Feng.success("删除成功!");
            OrganizationBanner.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询横幅信息列表
 */
OrganizationBanner.search = function () {
    var queryData = {};
    queryData['organizationId'] = $("#organizationId").val();
    OrganizationBanner.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = OrganizationBanner.initColumn();
    var table = new BSTable(OrganizationBanner.id, "/organizationBanner/list", defaultColunms);
    table.setPaginationType("server");
    OrganizationBanner.table = table.init();
});
