/**
 * 初始化横幅信息详情对话框
 */
var OrganizationBannerInfoDlg = {
    organizationBannerInfoData : {}
};

/**
 * 清除数据
 */
OrganizationBannerInfoDlg.clearData = function() {
    this.organizationBannerInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationBannerInfoDlg.set = function(key, val) {
    this.organizationBannerInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationBannerInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganizationBannerInfoDlg.close = function() {
    parent.layer.close(window.parent.OrganizationBanner.layerIndex);
}

/**
 * 收集数据
 */
OrganizationBannerInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('picture')
    .set('serialNo')
    .set('url')
    .set('state')
    .set('organizationId')
    .set('createtime');
}

/**
 * 提交添加
 */
OrganizationBannerInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",OrganizationBannerInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organizationBanner/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.OrganizationBanner.table.refresh();
                OrganizationBannerInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationBanner/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.OrganizationBanner.table.refresh();
    //    OrganizationBannerInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationBannerInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
OrganizationBannerInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organizationBanner/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.OrganizationBanner.table.refresh();
    //   OrganizationBannerInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationBannerInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/organizationBanner/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.OrganizationBanner.table.refresh();
                OrganizationBannerInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
