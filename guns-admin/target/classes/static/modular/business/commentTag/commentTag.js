/**
 * 评论标签管理初始化
 */
var CommentTag = {
    id: "CommentTagTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CommentTag.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '标签内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'serialNo', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
CommentTag.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CommentTag.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加评论标签
 */
CommentTag.openAddCommentTag = function () {
    var index = layer.open({
        type: 2,
        title: '添加评论标签',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/commentTag/commentTag_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看评论标签详情
 */
CommentTag.openCommentTagDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '评论标签详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/commentTag/commentTag_update/' + CommentTag.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除评论标签
 */
CommentTag.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/commentTag/delete", function (data) {
            Feng.success("删除成功!");
            CommentTag.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询评论标签列表
 */
CommentTag.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    CommentTag.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CommentTag.initColumn();
    var table = new BSTable(CommentTag.id, "/commentTag/list", defaultColunms);
    table.setPaginationType("server");
    CommentTag.table = table.init();
});
