/**
 * 初始化平台配置详情对话框
 */
var ConfigInfoInfoDlg = {
    configInfoInfoData : {}
};

/**
 * 清除数据
 */
ConfigInfoInfoDlg.clearData = function() {
    this.configInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ConfigInfoInfoDlg.set = function(key, val) {
    this.configInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ConfigInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ConfigInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.ConfigInfo.layerIndex);
}

/**
 * 收集数据
 */
ConfigInfoInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('content')
    .set('tips');
}

/**
 * 提交添加
 */
ConfigInfoInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",ConfigInfoInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/configInfo/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.ConfigInfo.table.refresh();
                ConfigInfoInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/configInfo/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.ConfigInfo.table.refresh();
    //    ConfigInfoInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.configInfoInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
ConfigInfoInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/configInfo/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.ConfigInfo.table.refresh();
    //   ConfigInfoInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.configInfoInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/configInfo/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.ConfigInfo.table.refresh();
                ConfigInfoInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
