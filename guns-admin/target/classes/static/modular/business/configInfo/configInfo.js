/**
 * 平台配置管理初始化
 */
var ConfigInfo = {
    id: "ConfigInfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ConfigInfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '配置名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '配置值', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'tips', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ConfigInfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ConfigInfo.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加平台配置
 */
ConfigInfo.openAddConfigInfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加平台配置',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/configInfo/configInfo_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看平台配置详情
 */
ConfigInfo.openConfigInfoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '平台配置详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/configInfo/configInfo_update/' + ConfigInfo.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除平台配置
 */
ConfigInfo.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/configInfo/delete", function (data) {
            Feng.success("删除成功!");
            ConfigInfo.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询平台配置列表
 */
ConfigInfo.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ConfigInfo.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ConfigInfo.initColumn();
    var table = new BSTable(ConfigInfo.id, "/configInfo/list", defaultColunms);
    table.setPaginationType("server");
    ConfigInfo.table = table.init();
});
