/**
 * 评论信息管理初始化
 */
var CourseComment = {
    id: "CourseCommentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CourseComment.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户名称', field: 'accountName', visible: true, align: 'center', valign: 'middle'},
            {title: '课程名称', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
            {title: '学校名称', field: 'schoolName', visible: true, align: 'center', valign: 'middle'},
            {title: '标签', field: 'tag', visible: true, align: 'center', valign: 'middle'},
            {title: '星级', field: 'starCount', visible: true, align: 'center', valign: 'middle'},
            {title: '评论时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
CourseComment.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CourseComment.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加评论信息
 */
CourseComment.openAddCourseComment = function () {
    var index = layer.open({
        type: 2,
        title: '添加评论信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/courseComment/courseComment_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看评论信息详情
 */
CourseComment.openCourseCommentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '评论信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/courseComment/courseComment_update/' + CourseComment.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除评论信息
 */
CourseComment.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = CourseComment.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/courseComment/delete", function (data) {
	            Feng.success("删除成功!");
	            CourseComment.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    };
    	Feng.confirm("是否删除评论?",operation);
    }
};

/**
 * 查询评论信息列表
 */
CourseComment.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    CourseComment.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CourseComment.initColumn();
    var table = new BSTable(CourseComment.id, "/courseComment/list", defaultColunms);
    table.setPaginationType("server");
    CourseComment.table = table.init();
});
