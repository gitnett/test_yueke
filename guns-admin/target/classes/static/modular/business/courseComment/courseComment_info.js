/**
 * 初始化评论信息详情对话框
 */
var CourseCommentInfoDlg = {
    courseCommentInfoData : {}
};

/**
 * 清除数据
 */
CourseCommentInfoDlg.clearData = function() {
    this.courseCommentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseCommentInfoDlg.set = function(key, val) {
    this.courseCommentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseCommentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CourseCommentInfoDlg.close = function() {
    parent.layer.close(window.parent.CourseComment.layerIndex);
}

/**
 * 收集数据
 */
CourseCommentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accountId')
    .set('accountName')
    .set('courseId')
    .set('courseName')
    .set('hisId')
    .set('tag')
    .set('content')
    .set('starCount')
    .set('createtime');
}

/**
 * 提交添加
 */
CourseCommentInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",CourseCommentInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/courseComment/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.CourseComment.table.refresh();
                CourseCommentInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/courseComment/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.CourseComment.table.refresh();
    //    CourseCommentInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseCommentInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
CourseCommentInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/courseComment/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.CourseComment.table.refresh();
    //   CourseCommentInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.courseCommentInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/courseComment/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.CourseComment.table.refresh();
                CourseCommentInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
