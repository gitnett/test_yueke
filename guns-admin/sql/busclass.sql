/*==============================================================*/
/* Table: bus_grade                                           */
/*==============================================================*/
drop table if exists bus_grade;
create table bus_grade
(
  id                   int not null AUTO_INCREMENT,
  grade_name             VARCHAR(200) comment '年级名称',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
   primary key (id)
);
alter table bus_grade comment '年级列表';
CREATE UNIQUE INDEX bus_grade_index_01 ON bus_grade(id);


/*==============================================================*/
/* Table: bus_class                                           */
/*==============================================================*/
drop table if exists bus_class;
create table bus_class
(
  id                   int not null AUTO_INCREMENT,
  class_name             VARCHAR(200) comment '班级名称',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
   primary key (id)
);
alter table bus_class comment '班级列表';
CREATE UNIQUE INDEX bus_class_index_01 ON bus_class(id);


/*==============================================================*/
/* Table: bus_course                                           */
/*==============================================================*/
drop table if exists bus_course;
create table bus_course
(
  id                   int not null AUTO_INCREMENT,
  course_name             VARCHAR(200) comment '课程名称',
  school_name              VARCHAR(200) comment '學校名称',
  school_id          int not null  comment '學校名称',
  start_course_grade            VARCHAR(100) comment '招生对象开始年级',
  end_course_grade            VARCHAR(100) comment '招生对象结束年级',
  course_onclass_time            varchar(100) NOT NULL comment '上课时间',
  course_fee            int comment '课程费用',
  course_quota          int comment '剩余名额',
  course_tol_num        int comment'总报名人数',
  coures_train_term     varchar(100) comment'培训学期',
  coures_teacher        varchar(100) comment'上课教师',
  coures_classroom      varchar(100) comment'上课教室',
  coures_iphone           varchar(100) comment'联系电话',
  coures_openclass_time    VARCHAR(100) comment'开课时间',
  coures_signup_starttime    VARCHAR(100) comment'报名开始时间',
  coures_signup_endtime    VARCHAR(100) comment'报名结束时间',
  coures_introduction   varchar(1000) comment'课程简介',
  coures_class_hour   varchar(1000) comment'课程课时',
  coures_status   int  not null comment'课程状态: 0:发布  1：下架',
  coures_img_path   varchar(1000) comment'课程图片',
  coures_banner_path   varchar(1000) comment'课程横幅',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  primary key (id)
);
alter table bus_course comment '课程列表';
CREATE UNIQUE INDEX bus_course_index_01 ON bus_course(id);

/*==============================================================*/
/* Table: bus_image                                           */
/*==============================================================*/
drop table if exists bus_image;
create table bus_image
(
  id                   int not null AUTO_INCREMENT,
  image_name             VARCHAR(200) comment '图片名称',
  image_path             VARCHAR(200) comment '图片路径',
  image_type             VARCHAR(200) comment '图片类型',
  image_size             int comment '图片大小',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
   primary key (id)
);
alter table bus_image comment '图片';
CREATE UNIQUE INDEX bus_image_index_01 ON bus_image(id);


/*==============================================================*/
/* Table: bus_my_childs                                           */
/*==============================================================*/
drop table if exists bus_my_childs;
create table bus_my_childs
(
  id                   int not null AUTO_INCREMENT,
  child_name             VARCHAR(200) comment '孩子名称',
  child_school             VARCHAR(200) comment '孩子学校',
  child_class             VARCHAR(200) comment '孩子班级',
  account_id            INT NOT NULL  comment '主表accout主键',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  primary key (id)
);
alter table bus_my_childs comment '我的孩子';
CREATE UNIQUE INDEX bus_my_childs_index_01 ON bus_my_childs(id);
CREATE UNIQUE INDEX bus_my_childs_index_02 ON bus_my_childs(account_id);


/*==============================================================*/
/* Table: bus_my_collections                                           */
/*==============================================================*/

drop table if exists bus_my_collections;
create table bus_my_collections
(
  id                   int not null AUTO_INCREMENT,
  course_id            int NOT NULL  comment '课程主键Id',
  account_id          INT NOT NULL  comment '主表accout主键',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  primary key (id)
);
alter table bus_my_collections comment '我的收藏';
CREATE UNIQUE INDEX bus_my_collections_index_01 ON bus_my_collections(id);
CREATE UNIQUE INDEX bus_my_collections_index_02 ON bus_my_collections(account_id);
CREATE UNIQUE INDEX bus_my_collections_index_03 ON bus_my_collections(course_id);

/*==============================================================*/
/* Table: bus_class_feedback                                           */
/*==============================================================*/

drop table if exists bus_class_feedback;
create table bus_class_feedback
(
  id                   int not null AUTO_INCREMENT,
  feedback_content            int NOT NULL  comment '反馈内容',
  account_id          INT NOT NULL  comment '主表accout主键',
  createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  primary key (id)
);
alter table bus_class_feedback comment '课后反馈';
CREATE UNIQUE INDEX bus_class_feedback_index_01 ON bus_class_feedback(id);
CREATE UNIQUE INDEX bus_class_feedback_index_02 ON bus_class_feedback(account_id);


/*==============================================================*/
/* Table: bus_acct_his                                           */
/*==============================================================*/
drop table if exists bus_acct_his;
create table bus_acct_his
(
   id                   int not null AUTO_INCREMENT,
  account_id          INT NOT NULL  comment '主表accout主键',
   amount               bigint comment '支付金额',
   pay_type             varchar(20) comment '支付方式',
   acct_channel      varchar(20) comment '来源渠道',
   status               varchar(20) comment '支付状态',
   flow_no              varchar(200) comment '平台流水号',
    bus_flow_no              varchar(200) comment '第三方流水号',
   msg                  varchar(200) comment '描述语',
   createtime           timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updatetime          timestamp  NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
   primary key (id)
);
CREATE UNIQUE INDEX bus_acct_his_01 ON bus_acct_his(id);
CREATE  INDEX bus_acct_his_02 ON bus_acct_his(flow_no);
CREATE  INDEX bus_acct_his_03 ON bus_acct_his(account_id);
alter table bus_acct_his comment '支付记录';
