package com.stylefeng.guns.admin.modular.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Image;
import com.stylefeng.guns.service.business.IImageService;

/**
 * 班级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 00:57:49
 */
@Controller
@RequestMapping("/image")
public class ImageController extends BaseController {

    private String PREFIX = "/business/image/";

    @Autowired
    private IImageService imageService;

    /**
     * 跳转到班级首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "image.html";
    }

    /**
     * 跳转到添加班级
     */
    @RequestMapping("/image_add")
    public String imageAdd() {
        return PREFIX + "image_add.html";
    }

    /**
     * 跳转到修改班级
     */
    @RequestMapping("/image_update/{imageId}")
    public String imageUpdate(@PathVariable Integer imageId, Model model) {
        Image image = imageService.selectById(imageId);
        model.addAttribute("item",image);
        LogObjectHolder.me().set(image);
        return PREFIX + "image_edit.html";
    }

    /**
     * 获取班级列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<Image> page = new PageFactory().defaultPage();
        page = imageService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增班级
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Image image) {
        imageService.insert(image);
        return SUCCESS_TIP;
    }

    /**
     * 删除班级
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    imageService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改班级
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Image image) {
        imageService.updateById(image);
        return SUCCESS_TIP;
    }

    /**
     * 班级详情
     */
    @RequestMapping(value = "/detail/{imageId}")
    @ResponseBody
    public Object detail(@PathVariable("imageId") Integer imageId) {
        return imageService.selectById(imageId);
    }
}
