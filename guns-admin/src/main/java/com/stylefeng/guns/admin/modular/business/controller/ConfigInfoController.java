package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.ConfigInfo;
import com.stylefeng.guns.service.business.IConfigInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 平台配置控制器
 *
 * @author fengshuonan
 * @Date 2018-08-14 20:24:35
 */
@Controller
@RequestMapping("/configInfo")
public class ConfigInfoController extends BaseController {

    private String PREFIX = "/business/configInfo/";

    @Autowired
    private IConfigInfoService configInfoService;

    /**
     * 跳转到平台配置首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "configInfo.html";
    }

    /**
     * 跳转到添加平台配置
     */
    @RequestMapping("/configInfo_add")
    public String configInfoAdd() {
        return PREFIX + "configInfo_add.html";
    }

    /**
     * 跳转到修改平台配置
     */
    @RequestMapping("/configInfo_update/{configInfoId}")
    public String configInfoUpdate(@PathVariable Integer configInfoId, Model model) {
        ConfigInfo configInfo = configInfoService.selectById(configInfoId);
        model.addAttribute("item",configInfo);
        LogObjectHolder.me().set(configInfo);
        return PREFIX + "configInfo_edit.html";
    }

    /**
     * 获取平台配置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<ConfigInfo> page = new PageFactory().defaultPage();
        page = configInfoService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增平台配置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ConfigInfo configInfo) {
        configInfoService.insert(configInfo);
        return SUCCESS_TIP;
    }

    /**
     * 删除平台配置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    configInfoService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改平台配置
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ConfigInfo configInfo) {
        configInfoService.updateById(configInfo);
        return SUCCESS_TIP;
    }

    /**
     * 平台配置详情
     */
    @RequestMapping(value = "/detail/{configInfoId}")
    @ResponseBody
    public Object detail(@PathVariable("configInfoId") Integer configInfoId) {
        return configInfoService.selectById(configInfoId);
    }
}
