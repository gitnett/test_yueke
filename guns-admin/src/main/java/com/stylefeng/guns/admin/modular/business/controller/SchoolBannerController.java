package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.persistence.modular.business.model.SchoolBanner;
import com.stylefeng.guns.service.business.ISchoolBannerService;
import com.stylefeng.guns.service.business.ISchoolService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 横幅信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 01:24:04
 */
@Controller
@RequestMapping("/schoolBanner")
public class SchoolBannerController extends BaseController {

    private String PREFIX = "/business/schoolBanner/";

    @Autowired
    private ISchoolBannerService schoolBannerService;

    @Autowired
    private ISchoolService schoolService;

    /**
     * 跳转到横幅信息首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<School> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime","id"));
        List<School> schools = schoolService.selectList(wrapper);
        model.addAttribute("schools", schools);
        return PREFIX + "schoolBanner.html";
    }

    /**
     * 跳转到添加横幅信息
     */
    @RequestMapping("/schoolBanner_add")
    public String schoolBannerAdd(Model model) {
        Wrapper<School> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime","id"));
        List<School> schools = schoolService.selectList(wrapper);
        model.addAttribute("schools", schools);
        return PREFIX + "schoolBanner_add.html";
    }

    /**
     * 跳转到修改横幅信息
     */
    @RequestMapping("/schoolBanner_update/{schoolBannerId}")
    public String schoolBannerUpdate(@PathVariable Integer schoolBannerId, Model model) {
        SchoolBanner schoolBanner = schoolBannerService.selectById(schoolBannerId);
        model.addAttribute("item",schoolBanner);
        LogObjectHolder.me().set(schoolBanner);

        Wrapper<School> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime","id"));
        List<School> schools = schoolService.selectList(wrapper);
        model.addAttribute("schools", schools);
        return PREFIX + "schoolBanner_edit.html";
    }

    /**
     * 获取横幅信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Integer schoolId) {
        Page<SchoolBanner> page = new PageFactory().defaultPage();

        Wrapper<SchoolBanner> wrapper = new EntityWrapper<>();

        if (schoolId != null) {
            wrapper.eq("s.id", schoolId);
        }

        wrapper.orderDesc(Arrays.asList("s.serial_no","s.createtime","s.id"));
        page = schoolBannerService.selectByPage(page, wrapper);

        return packForBT(page);
    }

    /**
     * 新增横幅信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SchoolBanner schoolBanner) {
        schoolBanner.setCreatetime(new Date());
        schoolBannerService.insert(schoolBanner);
        return SUCCESS_TIP;
    }

    /**
     * 删除横幅信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    schoolBannerService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改横幅信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SchoolBanner schoolBanner) {
        schoolBannerService.updateById(schoolBanner);
        return SUCCESS_TIP;
    }

    /**
     * 横幅信息详情
     */
    @RequestMapping(value = "/detail/{schoolBannerId}")
    @ResponseBody
    public Object detail(@PathVariable("schoolBannerId") Integer schoolBannerId) {
        return schoolBannerService.selectById(schoolBannerId);
    }

    /**
     * 删除图片
     *
     */
    @RequestMapping("/delete_picture")
    @ResponseBody
    public Object deletePicture(Integer id) {
        return SUCCESS_TIP;
    }
}
