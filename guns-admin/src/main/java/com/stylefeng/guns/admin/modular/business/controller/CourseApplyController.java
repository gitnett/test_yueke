package com.stylefeng.guns.admin.modular.business.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.support.HttpKit;
import com.stylefeng.guns.core.util.HttpUtil;
import com.stylefeng.guns.persistence.modular.business.model.*;
import com.stylefeng.guns.service.business.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 开课申请控制器
 *
 * @author fengshuonan
 * @Date 2018-08-15 21:20:39
 */
@Controller
@RequestMapping("/courseApply")
public class CourseApplyController extends BaseController {

    private String PREFIX = "/business/courseApply/";

    @Autowired
    private ICourseApplyService courseApplyService;

    @Autowired
    private ISchoolService schoolService;

    @Autowired
    private ICourseService courseService;

    @Autowired
    private ISchoolCourseTypeService schoolCourseTypeService;

    @Autowired
    private IGradeService gradeService;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IConfigInfoService configInfoService;

    /**
     * 跳转到开课申请首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<School> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no", "createtime", "id"));
        List<School> schools = schoolService.selectList(wrapper);
        model.addAttribute("schools", schools);
        return PREFIX + "courseApply.html";
    }

    /**
     * 跳转到修改开课申请
     */
    @RequestMapping("/courseApply_update/{courseId}")
    public String courseApplyUpdate(@PathVariable Integer courseId, Model model) {
        if (courseId != null) {
            Course course = courseService.selectById(courseId);
            model.addAttribute("item",course);
            if (course.getTypeId() != null) {
                SchoolCourseType schoolCourseType = schoolCourseTypeService.selectById(course.getTypeId());
                if (schoolCourseType != null) {
                    course.setTypeName(schoolCourseType.getName());
                }
            }
            List<Grade> grades = gradeService.selectList(null);
            model.addAttribute("grades", grades);
        }
        return PREFIX + "courseApply_edit.html";
    }

    /**
     * 跳转到修改开课申请
     */
    @RequestMapping("/courseApply_play/{courseId}")
    public String courseApplyPlay(@PathVariable Integer courseId, Model model) {
        if (courseId != null) {
            Wrapper<CourseApply> wrapper = new EntityWrapper<>();
            wrapper.eq("course_id", courseId);
            wrapper.orderBy("createtime", false);
            List<CourseApply> applies = courseApplyService.selectList(wrapper);
            applies.forEach(a ->{
                if (a.getAccountId() != null) {
                    Account account = accountService.selectById(a.getAccountId());
                    a.setAccount(account);
                }
            });
            model.addAttribute("applies", applies);
        }
        return PREFIX + "courseApply_play.html";
    }

    /**
     * 获取开课申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,Integer schoolId) {
        Page<Map<String,Object>> page = new PageFactory().defaultPage();
        Wrapper<CourseApply> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("a.course_name",condition);
        }
        if (schoolId != null) {
            wrapper.eq("c.school_id",schoolId);
        }
        page = courseApplyService.selectByPage2(page, wrapper);
        return packForBT(page);
    }

    /**
     * 修改开课申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Course course,Integer courseId) {
        course.setCreatetime(new Date());//设置创建时间
        course.setCourseQuota(course.getCourseTolNum());//设置人数

        //拷贝
        Course dbCourse = courseService.selectById(courseId);
        if (dbCourse != null) {
            course.setSchoolName(dbCourse.getSchoolName());
        }

        courseService.insert(course);

        //开课成功之后,发送模版消息通知用户,处理开课申请
        Wrapper<CourseApply> wrapper = new EntityWrapper<>();

        wrapper.eq("course_id", courseId);
        wrapper.eq("status", 0);

        //获取项目当前域名
        Wrapper<ConfigInfo> configWrapper = new EntityWrapper<>();
        configWrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(configWrapper);

        if (configInfo != null && StringUtils.isNotBlank(configInfo.getContent())) {
            Map<String, Object> params = new HashMap<>();
            params.put("type", 1);
            Map<String, Object> data = new HashMap<>();
            data.put("oldId",courseId );
            data.put("newId",course.getId());
            params.put("data", data);
            Map map = HttpUtil.executePost(configInfo.getContent() + "/m/sendCourseTpl", JSON.toJSONString(params), Map.class);
        }

        List<CourseApply> applies = courseApplyService.selectList(wrapper);
        for (CourseApply apply : applies) {
            apply.setStatus(1);
        }
        courseApplyService.updateBatchById(applies);
        return SUCCESS_TIP;
    }
}
