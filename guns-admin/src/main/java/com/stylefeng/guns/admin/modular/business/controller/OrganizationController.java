package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.stylefeng.guns.service.business.IOrganizationService;
import com.stylefeng.guns.service.business.IRegionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 机构信息控制器
 *
 * @author fengshuonan
 * @Date 2018-07-30 12:10:34
 */
@Controller
@RequestMapping("/organization")
public class OrganizationController extends BaseController {

    private String PREFIX = "/business/organization/";

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IRegionService regionService;

    /**
     * 跳转到机构信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "organization.html";
    }

    /**
     * 跳转到添加机构信息
     */
    @RequestMapping("/organization_add")
    public String organizationAdd(Model model) {
        //查询一级业务区域信息
        Wrapper<Region> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
        wrapper.eq("p_id", 0);//只查询一级区域
        List<Region> regions = regionService.selectList(wrapper);
        model.addAttribute("p_regions", regions);

        //取第一个业务区域信息进行查询预渲染
        if (regions != null && regions.size() > 0) {
            Region region = regions.get(0);
            wrapper = new EntityWrapper<>();
            wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
            wrapper.eq("p_id", region.getId());
            regions = regionService.selectList(wrapper);
            model.addAttribute("c_regions", regions);
        }
        return PREFIX + "organization_add.html";
    }

    /**
     * 跳转到修改机构信息
     */
    @RequestMapping("/organization_update/{organizationId}")
    public String organizationUpdate(@PathVariable Integer organizationId, Model model) {
        Organization organization = organizationService.selectById(organizationId);
        model.addAttribute("item",organization);
        //LogObjectHolder.me().set(organization);
        if (organization.getRegionId() != null) {
            Region region = regionService.selectById(organization.getRegionId());
            organization.setRegion(region);
            //查询一级区域
            Wrapper<Region> wrapper = new EntityWrapper<>();
            wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
            wrapper.eq("p_id", 0);//只查询一级区域
            List<Region> regions = regionService.selectList(wrapper);
            model.addAttribute("p_regions", regions);
            //取选中业务区域信息进行查询预渲染
            if (region != null) {
                wrapper = new EntityWrapper<>();
                wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
                wrapper.eq("p_id", region.getpId());
                regions = regionService.selectList(wrapper);
                model.addAttribute("c_regions", regions);
            }
        }
        return PREFIX + "organization_edit.html";
    }

    /**
     * 获取机构信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<Organization> page = new PageFactory().defaultPage();
        page = organizationService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增机构信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Organization organization) {
        organization.setCreatetime(new Date());
        organizationService.insert(organization);
        return SUCCESS_TIP;
    }

    /**
     * 删除机构信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    organizationService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改机构信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Organization organization) {
        organizationService.updateById(organization);
        return SUCCESS_TIP;
    }

    /**
     * 机构信息详情
     */
    @RequestMapping(value = "/detail/{organizationId}")
    @ResponseBody
    public Object detail(@PathVariable("organizationId") Integer organizationId) {
        return organizationService.selectById(organizationId);
    }
}
