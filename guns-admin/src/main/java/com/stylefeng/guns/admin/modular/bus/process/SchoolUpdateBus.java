package com.stylefeng.guns.admin.modular.bus.process;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.google.common.eventbus.Subscribe;
import com.stylefeng.guns.admin.core.bus.BusService;
import com.stylefeng.guns.admin.modular.bus.event.SchoolUpdate;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.persistence.modular.business.model.Course;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;
import com.stylefeng.guns.service.business.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SchoolUpdateBus implements BusService<SchoolUpdate> {

    @Autowired
    private ICourseService courseService;

    @Autowired
    private IBusShcoolGradeService busShcoolGradeService;

    @Override
    @Subscribe
    public void execute(SchoolUpdate schoolUpdate) {
        //更新课程的学校
        Wrapper<Course> wrapper = new EntityWrapper<>();
        wrapper.eq("school_id", schoolUpdate.getSchool().getId());
        List<Course> courses = courseService.selectList(wrapper);
        courses.forEach(g ->{
            g.setSchoolName(schoolUpdate.getSchool().getName());
        });
        courseService.updateBatchById(courses);
        //更新班级
        Wrapper<BusShcoolGrade> gradeWrapper = new EntityWrapper<>();
        gradeWrapper.eq("school_id", schoolUpdate.getSchool().getId());
        List<BusShcoolGrade> grades = busShcoolGradeService.selectList(gradeWrapper);
        grades.forEach(g ->{
            g.setSchoolName(schoolUpdate.getSchool().getName());
        });
        busShcoolGradeService.updateBatchById(grades);
    }
}
