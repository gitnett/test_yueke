package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.service.business.ISchoolService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.ClassFeedback;
import com.stylefeng.guns.service.business.IClassFeedbackService;

import java.util.Arrays;
import java.util.List;

/**
 * 班级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 00:55:05
 */
@Controller
@RequestMapping("/classFeedback")
public class ClassFeedbackController extends BaseController {

    private String PREFIX = "/business/classFeedback/";

    @Autowired
    private IClassFeedbackService classFeedbackService;

    @Autowired
    private ISchoolService schoolService;

    /**
     * 跳转到班级首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<School> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime","id"));
        List<School> schools = schoolService.selectList(wrapper);
        model.addAttribute("schools", schools);
        return PREFIX + "classFeedback.html";
    }

    /**
     * 跳转到添加班级
     */
    @RequestMapping("/classFeedback_add")
    public String classFeedbackAdd() {
        return PREFIX + "classFeedback_add.html";
    }

    /**
     * 跳转到修改班级
     */
    @RequestMapping("/classFeedback_update/{classFeedbackId}")
    public String classFeedbackUpdate(@PathVariable Integer classFeedbackId, Model model) {
        ClassFeedback classFeedback = classFeedbackService.selectById(classFeedbackId);
        model.addAttribute("item",classFeedback);
        LogObjectHolder.me().set(classFeedback);
        return PREFIX + "classFeedback_edit.html";
    }

    /**
     * 获取班级列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Integer schoolId,String accountName) {
        Page<ClassFeedback> page = new PageFactory().defaultPage();
        Wrapper<ClassFeedback> wrapper = new EntityWrapper<>();

        if (schoolId != null) {
            wrapper.eq("f.school_id", schoolId);
        }

        if (StringUtils.isNotBlank(accountName)) {
            wrapper.like("f.account_name", accountName);
        }

        wrapper.orderDesc(Arrays.asList("createtime"));

        page = classFeedbackService.selectByPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增班级
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ClassFeedback classFeedback) {
        classFeedbackService.insert(classFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 删除班级
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String classFeedbackId) {
        if(StringUtils.isNotBlank(classFeedbackId)){
            String[] split = classFeedbackId.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    classFeedbackService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改班级
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ClassFeedback classFeedback) {
        classFeedbackService.updateById(classFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 班级详情
     */
    @RequestMapping(value = "/detail/{classFeedbackId}")
    @ResponseBody
    public Object detail(@PathVariable("classFeedbackId") Integer classFeedbackId) {
        return classFeedbackService.selectById(classFeedbackId);
    }
}
