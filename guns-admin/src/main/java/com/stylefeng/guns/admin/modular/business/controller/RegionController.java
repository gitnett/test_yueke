package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.SuccessTip;
import com.stylefeng.guns.core.base.tips.Tip;
import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.stylefeng.guns.service.business.IRegionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 地区信息控制器
 *
 * @author fengshuonan
 * @Date 2018-07-30 14:49:59
 */
@Controller
@RequestMapping("/region")
public class RegionController extends BaseController {

    private String PREFIX = "/business/region/";

    @Autowired
    private IRegionService regionService;

    /**
     * 跳转到地区信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "region.html";
    }

    /**
     * 跳转到添加地区信息
     */
    @RequestMapping("/region_add")
    public String regionAdd(Model model) {
        Wrapper<Region> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
        wrapper.eq("p_id", 0);//只查询一级区域
        List<Region> regions = regionService.selectList(wrapper);
        model.addAttribute("regions", regions);
        return PREFIX + "region_add.html";
    }

    /**
     * 跳转到修改地区信息
     */
    @RequestMapping("/region_update/{regionId}")
    public String regionUpdate(@PathVariable Integer regionId, Model model) {
        Region region = regionService.selectById(regionId);
        model.addAttribute("item",region);
        Wrapper<Region> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
        wrapper.eq("p_id", 0);//只查询一级区域
        List<Region> regions = regionService.selectList(wrapper);
        model.addAttribute("regions", regions);
        return PREFIX + "region_edit.html";
    }

    /**
     * 获取地区信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper<Region> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
        return regionService.selectList(wrapper);
    }

    /**
     * 新增地区信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Region region) {
        region.setCreatetime(new Date());
        regionService.insert(region);
        return SUCCESS_TIP;
    }

    /**
     * 删除地区信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    regionService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改地区信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Region region) {
        regionService.updateById(region);
        return SUCCESS_TIP;
    }

    /**
     * 地区信息详情
     */
    @RequestMapping(value = "/detail/{regionId}")
    @ResponseBody
    public Object detail(@PathVariable("regionId") Integer regionId) {
        return regionService.selectById(regionId);
    }

    /**
     * 根据pId查找二级区域
     * @param pId 一级区域id
     */
    @RequestMapping(value = "/selectByPid")
    @ResponseBody
    public Object selectByPid(Integer pId) {
        Tip tip = new SuccessTip();
        if (pId != null) {
            Wrapper<Region> wrapper = new EntityWrapper<>();
            wrapper.eq("p_id", pId);
            wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
            tip.setData(regionService.selectList(wrapper));
        }else{
            tip.setData(new ArrayList<>());
        }
        return tip;
    }
}
