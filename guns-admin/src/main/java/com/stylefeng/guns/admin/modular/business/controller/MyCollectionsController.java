package com.stylefeng.guns.admin.modular.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.MyCollections;
import com.stylefeng.guns.service.business.IMyCollectionsService;

/**
 * 班级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 00:57:13
 */
@Controller
@RequestMapping("/myCollections")
public class MyCollectionsController extends BaseController {

    private String PREFIX = "/business/myCollections/";

    @Autowired
    private IMyCollectionsService myCollectionsService;

    /**
     * 跳转到班级首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "myCollections.html";
    }

    /**
     * 跳转到添加班级
     */
    @RequestMapping("/myCollections_add")
    public String myCollectionsAdd() {
        return PREFIX + "myCollections_add.html";
    }

    /**
     * 跳转到修改班级
     */
    @RequestMapping("/myCollections_update/{myCollectionsId}")
    public String myCollectionsUpdate(@PathVariable Integer myCollectionsId, Model model) {
        MyCollections myCollections = myCollectionsService.selectById(myCollectionsId);
        model.addAttribute("item",myCollections);
        LogObjectHolder.me().set(myCollections);
        return PREFIX + "myCollections_edit.html";
    }

    /**
     * 获取班级列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<MyCollections> page = new PageFactory().defaultPage();
        page = myCollectionsService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增班级
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(MyCollections myCollections) {
        myCollectionsService.insert(myCollections);
        return SUCCESS_TIP;
    }

    /**
     * 删除班级
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    myCollectionsService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改班级
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(MyCollections myCollections) {
        myCollectionsService.updateById(myCollections);
        return SUCCESS_TIP;
    }

    /**
     * 班级详情
     */
    @RequestMapping(value = "/detail/{myCollectionsId}")
    @ResponseBody
    public Object detail(@PathVariable("myCollectionsId") Integer myCollectionsId) {
        return myCollectionsService.selectById(myCollectionsId);
    }
}
