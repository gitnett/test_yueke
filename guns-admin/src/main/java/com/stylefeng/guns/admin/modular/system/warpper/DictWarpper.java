package com.stylefeng.guns.admin.modular.system.warpper;

import com.stylefeng.guns.admin.core.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.persistence.modular.system.model.Dict;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.ToolUtil;

import java.util.List;
import java.util.Map;

/**
 * 字典列表的包装
 *
 * @author fengshuonan
 * @date 2017年4月25日 18:10:31
 */
public class DictWarpper extends BaseControllerWarpper {

    public DictWarpper(Object list) {
        super(list);
    }

    @Override
    public void warpTheMap(Map<String, Object> map) {
        StringBuffer detail = new StringBuffer();
        Integer id = (Integer) map.get("id");
        List<Dict> dictModels = ConstantFactory.me().findInDict(id);
        if(dictModels != null){
            for (Dict dictModel : dictModels) {
                detail.append(dictModel.getNum() + ":" + dictModel.getName() + ",");
            }
            map.put("detail", ToolUtil.removeSuffix(detail.toString(),","));
        }
    }

}
