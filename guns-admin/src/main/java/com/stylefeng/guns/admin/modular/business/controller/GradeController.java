package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Grade;
import com.stylefeng.guns.service.business.IGradeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 年级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-03 22:16:56
 */
@Controller
@RequestMapping("/grade")
public class GradeController extends BaseController {

    private String PREFIX = "/business/grade/";

    @Autowired
    private IGradeService gradeService;

    /**
     * 跳转到年级首页
     */
    @RequestMapping("")
    public String index(Model model) {

        return PREFIX + "grade.html";
    }

    /**
     * 跳转到添加年级
     */
    @RequestMapping("/grade_add")
    public String gradeAdd() {
        return PREFIX + "grade_add.html";
    }

    /**
     * 跳转到修改年级
     */
    @RequestMapping("/grade_update/{gradeId}")
    public String gradeUpdate(@PathVariable Integer gradeId, Model model) {
        Grade grade = gradeService.selectById(gradeId);
        model.addAttribute("item",grade);
        LogObjectHolder.me().set(grade);
        return PREFIX + "grade_edit.html";
    }

    /**
     * 获取年级列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String schoolId) {
        Page<Grade> page = new PageFactory().defaultPage();
        Wrapper<Grade> wrapper = new EntityWrapper<>();

        page = gradeService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增年级
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Grade grade) {
        gradeService.insert(grade);
        return SUCCESS_TIP;
    }

    /**
     * 删除年级
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    gradeService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改年级
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Grade grade) {
        gradeService.updateById(grade);
        return SUCCESS_TIP;
    }

    /**
     * 年级详情
     */
    @RequestMapping(value = "/detail/{gradeId}")
    @ResponseBody
    public Object detail(@PathVariable("gradeId") Integer gradeId) {
        return gradeService.selectById(gradeId);
    }
}
