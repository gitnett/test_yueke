package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.CommentTag;
import com.stylefeng.guns.service.business.ICommentTagService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * 评论标签控制器
 *
 * @author fengshuonan
 * @Date 2018-08-13 18:26:00
 */
@Controller
@RequestMapping("/commentTag")
public class CommentTagController extends BaseController {

    private String PREFIX = "/business/commentTag/";

    @Autowired
    private ICommentTagService commentTagService;

    /**
     * 跳转到评论标签首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "commentTag.html";
    }

    /**
     * 跳转到添加评论标签
     */
    @RequestMapping("/commentTag_add")
    public String commentTagAdd() {
        return PREFIX + "commentTag_add.html";
    }

    /**
     * 跳转到修改评论标签
     */
    @RequestMapping("/commentTag_update/{commentTagId}")
    public String commentTagUpdate(@PathVariable Integer commentTagId, Model model) {
        CommentTag commentTag = commentTagService.selectById(commentTagId);
        model.addAttribute("item",commentTag);
        LogObjectHolder.me().set(commentTag);
        return PREFIX + "commentTag_edit.html";
    }

    /**
     * 获取评论标签列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<CommentTag> page = new PageFactory().defaultPage();
        page = commentTagService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增评论标签
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CommentTag commentTag) {
        commentTag.setCreatetime(new Date());
        commentTagService.insert(commentTag);
        return SUCCESS_TIP;
    }

    /**
     * 删除评论标签
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    commentTagService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改评论标签
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CommentTag commentTag) {
        commentTagService.updateById(commentTag);
        return SUCCESS_TIP;
    }

    /**
     * 评论标签详情
     */
    @RequestMapping(value = "/detail/{commentTagId}")
    @ResponseBody
    public Object detail(@PathVariable("commentTagId") Integer commentTagId) {
        return commentTagService.selectById(commentTagId);
    }
}
