package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Activity;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.service.business.IActivityService;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 活动信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 18:55:37
 */
@Controller
@RequestMapping("/activity")
public class ActivityController extends BaseController {

    private String PREFIX = "/business/activity/";

    @Autowired
    private IActivityService activityService;

    @Autowired
    private IOrganizationService organizationService;

    /**
     * 跳转到活动信息首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "activity.html";
    }

    /**
     * 跳转到添加活动信息
     */
    @RequestMapping("/activity_add")
    public String activityAdd(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "activity_add.html";
    }

    /**
     * 跳转到修改活动信息
     */
    @RequestMapping("/activity_update/{activityId}")
    public String activityUpdate(@PathVariable Integer activityId, Model model) {
        Activity activity = activityService.selectById(activityId);
        model.addAttribute("item",activity);
        LogObjectHolder.me().set(activity);

        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);

        return PREFIX + "activity_edit.html";
    }

    /**
     * 获取活动信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,String organizationId) {
        Page<Activity> page = new PageFactory().defaultPage();
        Wrapper<Activity> wrapper = new EntityWrapper<>();

        wrapper.orderDesc(Arrays.asList("createtime", "id"));

        if (StringUtils.isNotBlank(condition)) {
            //根据活动名称查询
            wrapper.like("a.title", condition);
        }

        if (NumberUtils.isCreatable(organizationId)) {
            wrapper.eq("a.organization_id", Integer.valueOf(organizationId));
        }

        page = activityService.selectByPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增活动信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Activity activity) {
        activity.setCreatetime(new Date());
        activityService.insert(activity);
        return SUCCESS_TIP;
    }

    /**
     * 删除活动信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    activityService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改活动信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Activity activity) {
        activityService.updateById(activity);
        return SUCCESS_TIP;
    }

    /**
     * 活动信息详情
     */
    @RequestMapping(value = "/detail/{activityId}")
    @ResponseBody
    public Object detail(@PathVariable("activityId") Integer activityId) {
        return activityService.selectById(activityId);
    }

    /**
     * 删除图片
     *
     */
    @RequestMapping("/delete_picture")
    @ResponseBody
    public Object deletePicture(Integer id) {
        return SUCCESS_TIP;
    }
}
