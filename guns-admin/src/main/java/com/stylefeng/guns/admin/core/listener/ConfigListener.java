/**
 * Copyright (c) 2015-2017, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stylefeng.guns.admin.core.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * ServletContext监听器
 *
 * @author stylefeng
 * @Date 2018/2/22 21:07
 */
public class ConfigListener implements ServletContextListener {

    private static final String DEFAULT_OS = "Windows";

    // 虚拟访问路径
    public static final String VIRTUAL_FILE_PATH = "/file/";

    /**
     * 系统存放路径 window系统默认在 d:\file下 ;linux默认在 /file下
     */
    public static String REAL_FILE_PATH;

    static {
        String osName = System.getProperty("os.name", DEFAULT_OS);
        if (osName != null) {
            if (osName.contains(DEFAULT_OS)) {
                REAL_FILE_PATH = "d:" + File.separator + "file" + File.separator;
            } else {
                REAL_FILE_PATH = File.separator + "file" + File.separator;
            }
        }
    }

    private static Map<String, String> conf = new HashMap<>();

    public static Map<String, String> getConf() {
        return conf;
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        conf.clear();
    }

    @Override
    public void contextInitialized(ServletContextEvent evt) {
        ServletContext sc = evt.getServletContext();

        //项目发布,当前运行环境的绝对路径
        conf.put("realPath", sc.getRealPath("/").replaceFirst("/", ""));

        //servletContextPath,默认""
        conf.put("contextPath", sc.getContextPath());

        // 查看是否有上传目录
        File file = new File(REAL_FILE_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

}
