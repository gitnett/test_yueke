package com.stylefeng.guns.admin.modular.business.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.admin.modular.system.utils.ExcelData;
import com.stylefeng.guns.admin.modular.system.utils.ExportExcelUtils;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.support.DateTime;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.persistence.modular.business.model.Grade;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;
import com.stylefeng.guns.service.business.IGradeService;
import com.stylefeng.guns.service.business.ISchoolService;

/**
 * 学校班级管理表控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 21:39:10
 */
@Controller
@RequestMapping("/shcoolGrade")
public class ShcoolGradeController extends BaseController {

	private String PREFIX = "/business/shcoolGrade/";

	@Autowired
	private IBusShcoolGradeService shcoolGradeService;

	@Autowired
	private ISchoolService shcoolService;

	@Autowired
	private IGradeService gradeService;

	@Autowired
	private ISchoolService schoolService;

	private DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	/**
	 * 跳转到学校班级管理表首页
	 */
	@RequestMapping("")
	public String index(Model model) {
		Wrapper<School> wrapper = new EntityWrapper<>();
		wrapper.orderDesc(Arrays.asList("serial_no", "createtime", "id"));
		List<School> schools = schoolService.selectList(wrapper);
		model.addAttribute("schools", schools);
		return PREFIX + "shcoolGrade.html";
	}

	/**
	 * 跳转到添加学校班级管理表
	 */
	@RequestMapping("/shcoolGrade_add")
	public String shcoolGradeAdd(Model model) {

		Wrapper<School> wrapper = new EntityWrapper<>();
		wrapper.orderDesc(Arrays.asList("createtime"));
		List<School> schools = shcoolService.selectList(wrapper);

		Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
		List<Grade> busgrade = gradeService.selectList(gradeWrapper);

		model.addAttribute("b_school", schools);
		model.addAttribute("b_grade", busgrade);
		return PREFIX + "shcoolGrade_add.html";
	}

	/**
	 * 跳转到修改学校班级管理表
	 */
	@RequestMapping("/shcoolGrade_update/{shcoolGradeId}")
	public String shcoolGradeUpdate(@PathVariable Integer shcoolGradeId, Model model) {
		BusShcoolGrade shcoolGrade = shcoolGradeService.selectById(shcoolGradeId);
		model.addAttribute("item", shcoolGrade);
		LogObjectHolder.me().set(shcoolGrade);

		Wrapper<School> wrapper = new EntityWrapper<>();
		wrapper.orderDesc(Arrays.asList("createtime"));
		List<School> schools = shcoolService.selectList(wrapper);

		Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
		List<Grade> busgrade = gradeService.selectList(gradeWrapper);

		model.addAttribute("b_school", schools);
		model.addAttribute("b_grade", busgrade);
		return PREFIX + "shcoolGrade_edit.html";
	}

	/**
	 * 获取学校班级管理表列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(String schoolId) {
		Page<BusShcoolGrade> page = new PageFactory().defaultPage();
		Wrapper<BusShcoolGrade> wrapper = new EntityWrapper<>();
		if (StringUtils.isNoneEmpty(schoolId)) {
			wrapper.eq("school_id", Integer.valueOf(schoolId));
		}
		page = shcoolGradeService.selectPage(page, wrapper);
		return packForBT(page);
	}

	/**
	 * 新增学校班级管理表
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(BusShcoolGrade shcoolGrade, Integer start, Integer end) {
		List<BusShcoolGrade> grades = new ArrayList<>();
		if (start != null && end != null) {
			School school = shcoolService.selectById(shcoolGrade.getSchoolId());
			Grade grade = gradeService.selectById(shcoolGrade.getGradeId());
			Wrapper<BusShcoolGrade> wrapper = null;
			List<BusShcoolGrade> gradeList = new ArrayList<>();
			BusShcoolGrade tmp;
			for (int i = start; i <= end; i++) {
				// 判断是否存在重复
				wrapper = new EntityWrapper<>();
				wrapper.eq("school_id", shcoolGrade.getSchoolId());
				wrapper.eq("grade_id", shcoolGrade.getGradeId());
				wrapper.eq("class_name", i + "班");

				int count = shcoolGradeService.selectCount(wrapper);

				if (count > 0) {
					return new ErrorTip(500, school.getName() + grade.getGradeName() + i + "班已存在!");
				}
				tmp = new BusShcoolGrade();
				tmp.setSchoolId(shcoolGrade.getSchoolId());
				tmp.setGradeId(shcoolGrade.getGradeId());
				tmp.setClassName(i + "班");
                tmp.setSerialNo(i);
				tmp.setSchoolName(school.getName());
				tmp.setGcName(grade.getGradeName());
				tmp.setCreatetime(new Date());
				gradeList.add(tmp);
			}

			//
			gradeList.forEach(g -> {
				shcoolGradeService.insert(g);
			});

		}
		return SUCCESS_TIP;
	}

	/**
	 * 删除学校班级管理表
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam String ids) {
		if (StringUtils.isNotBlank(ids)) {
			String[] split = ids.split(",");
			for (String id : split) {
				if (NumberUtils.isCreatable(id)) {
					shcoolGradeService.deleteById(Integer.valueOf(id));
				}
			}
		}
		return SUCCESS_TIP;
	}

	/**
	 * 修改学校班级管理表
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(BusShcoolGrade shcoolGrade) {
		School school = shcoolService.selectById(shcoolGrade.getSchoolId());
		Grade grade = gradeService.selectById(shcoolGrade.getGradeId());
		if (school != null) {
			shcoolGrade.setSchoolName(school.getName());
		}
		if (grade != null) {
			shcoolGrade.setGcName(grade.getGradeName());
		}

		// 判断是否存在重复
		Wrapper<BusShcoolGrade> wrapper = new EntityWrapper<>();
		wrapper.eq("school_id", shcoolGrade.getSchoolId());
		wrapper.eq("grade_id", shcoolGrade.getGradeId());
		wrapper.eq("class_name", shcoolGrade.getClassName());
		wrapper.notIn("id", shcoolGrade.getId());

		int count = shcoolGradeService.selectCount(wrapper);

		if (count > 0) {
			return new ErrorTip(500, school.getName() + grade.getGradeName() + shcoolGrade.getClassName() + "已存在!");
		}

		shcoolGrade.setUpdatetime(new DateTime());
		shcoolGradeService.updateById(shcoolGrade);
		return SUCCESS_TIP;
	}

	/**
	 * 学校班级管理表详情
	 */
	@RequestMapping(value = "/detail/{shcoolGradeId}")
	@ResponseBody
	public Object detail(@PathVariable("shcoolGradeId") Integer shcoolGradeId) {
		return shcoolGradeService.selectById(shcoolGradeId);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/daochu")
	public void list(HttpServletResponse response, String schoolId) throws Exception {
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (StringUtils.isNoneEmpty(schoolId)) {
			map.put("school_id", Integer.valueOf(schoolId));
		}
		List<BusShcoolGrade> lists = shcoolGradeService.selectByMap(map);
		ExcelData data = new ExcelData();
		List<String> titles = new ArrayList();
		titles.add("ID");
		titles.add("学校名称");
		titles.add("年级名称");
		titles.add("班级名称");
		titles.add("创建时间");
		data.setTitles(titles);

		@SuppressWarnings("unchecked")
		List<List<Object>> rows = new ArrayList();
		for (BusShcoolGrade grade :lists) {
			List<Object> row = new ArrayList();
			row.add(grade.getId());
			row.add(grade.getSchoolName());
			row.add(grade.getGcName());
			row.add(grade.getClassName());
			row.add(format.format(grade.getCreatetime()));
			rows.add(row);
		}
		data.setRows(rows);
		ExportExcelUtils.exportExcel(response, "班级信息.xlsx", data);
		//return SUCCESS_TIP;
	}
}
