package com.stylefeng.guns.admin.modular.bus.event;

import com.stylefeng.guns.persistence.modular.business.model.School;


/**
 * 删除学校事件
 */
public class SchoolDelete {

    private School school;


    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
