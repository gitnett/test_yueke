package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourse;
import com.stylefeng.guns.service.business.IOrganizationCourseService;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 课程信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 00:10:14
 */
@Controller
@RequestMapping("/organizationCourse")
public class OrganizationCourseController extends BaseController {

    private String PREFIX = "/business/organizationCourse/";

    @Autowired
    private IOrganizationCourseService organizationCourseService;

    @Autowired
    private IOrganizationService organizationService;

    /**
     * 跳转到课程信息首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationCourse.html";
    }

    /**
     * 跳转到添加课程信息
     */
    @RequestMapping("/organizationCourse_add")
    public String organizationCourseAdd(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationCourse_add.html";
    }

    /**
     * 跳转到修改课程信息
     */
    @RequestMapping("/organizationCourse_update/{organizationCourseId}")
    public String organizationCourseUpdate(@PathVariable Integer organizationCourseId, Model model) {
        OrganizationCourse organizationCourse = organizationCourseService.selectById(organizationCourseId);
        model.addAttribute("item",organizationCourse);
        LogObjectHolder.me().set(organizationCourse);

        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationCourse_edit.html";
    }

    /**
     * 获取课程信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,String organizationId) {
        Page<OrganizationCourse> page = new PageFactory().defaultPage();
        Wrapper<OrganizationCourse> wrapper = new EntityWrapper<>();

        wrapper.orderDesc(Arrays.asList("serial_no", "createtime", "id"));

        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("c.title", condition);
        }

        if (NumberUtils.isCreatable(organizationId)) {
            wrapper.eq("organization_id", Integer.valueOf(organizationId));
        }

        page = organizationCourseService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增课程信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OrganizationCourse organizationCourse) {
        organizationCourse.setCreatetime(new Date());
        organizationCourseService.insert(organizationCourse);
        return SUCCESS_TIP;
    }

    /**
     * 删除课程信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    organizationCourseService.deleteById(Long.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改课程信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OrganizationCourse organizationCourse) {
        organizationCourseService.updateById(organizationCourse);
        return SUCCESS_TIP;
    }

    /**
     * 课程信息详情
     */
    @RequestMapping(value = "/detail/{organizationCourseId}")
    @ResponseBody
    public Object detail(@PathVariable("organizationCourseId") Integer organizationCourseId) {
        return organizationCourseService.selectById(organizationCourseId);
    }

    /**
     * 删除图片
     *
     */
    @RequestMapping("/delete_picture")
    @ResponseBody
    public Object deletePicture(Integer id) {
        return SUCCESS_TIP;
    }
}
