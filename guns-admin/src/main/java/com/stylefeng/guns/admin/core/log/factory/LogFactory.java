package com.stylefeng.guns.admin.core.log.factory;

import com.stylefeng.guns.admin.core.common.constant.state.LogSucceed;
import com.stylefeng.guns.admin.core.common.constant.state.LogType;
import com.stylefeng.guns.persistence.modular.system.model.LoginLog;
import com.stylefeng.guns.persistence.modular.system.model.Operation;

import java.util.Date;

/**
 * 日志对象创建工厂
 *
 * @author fengshuonan
 * @date 2016年12月6日 下午9:18:27
 */
public class LogFactory {

    /**
     * 创建操作日志
     */
    public static Operation createOperationLog(LogType logType, Integer userId, String bussinessName, String clazzName, String methodName, String msg, LogSucceed succeed) {
        Operation operation = new Operation();
        operation.setLogtype(logType.getMessage());
        operation.setLogname(bussinessName);
        operation.setUserid(userId);
        operation.setClassname(clazzName);
        operation.setMethod(methodName);
        operation.setCreatetime(new Date());
        operation.setSucceed(succeed.getMessage());
        operation.setMessage(msg);
        return operation;
    }

    /**
     * 创建登录日志
     */
    public static LoginLog createLoginLog(LogType logType, Integer userId, String msg, String ip) {
        LoginLog loginLog = new LoginLog();
        loginLog.setLogname(logType.getMessage());
        loginLog.setUserid(userId);
        loginLog.setCreatetime(new Date());
        loginLog.setSucceed(LogSucceed.SUCCESS.getMessage());
        loginLog.setIp(ip);
        loginLog.setMessage(msg);
        return loginLog;
    }
}
