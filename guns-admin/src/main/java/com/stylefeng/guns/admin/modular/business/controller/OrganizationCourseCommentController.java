package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationCourseComment;
import com.stylefeng.guns.service.business.IOrganizationCourseCommentService;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

/**
 * 评论信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 22:32:44
 */
@Controller
@RequestMapping("/organizationCourseComment")
public class OrganizationCourseCommentController extends BaseController {

    private String PREFIX = "/business/organizationCourseComment/";

    @Autowired
    private IOrganizationCourseCommentService organizationCourseCommentService;

    @Autowired
    private IOrganizationService organizationService;

    /**
     * 跳转到评论信息首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationCourseComment.html";
    }

    /**
     * 跳转到添加评论信息
     */
    @RequestMapping("/organizationCourseComment_add")
    public String organizationCourseCommentAdd() {
        return PREFIX + "organizationCourseComment_add.html";
    }

    /**
     * 跳转到修改评论信息
     */
    @RequestMapping("/organizationCourseComment_update/{organizationCourseCommentId}")
    public String organizationCourseCommentUpdate(@PathVariable Integer organizationCourseCommentId, Model model) {
        OrganizationCourseComment organizationCourseComment = organizationCourseCommentService.selectById(organizationCourseCommentId);
        model.addAttribute("item",organizationCourseComment);
        //LogObjectHolder.me().set(organizationCourseComment);
        return PREFIX + "organizationCourseComment_edit.html";
    }

    /**
     * 获取评论信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,Integer organizationId) {
        Page<OrganizationCourseComment> page = new PageFactory().defaultPage();
        Wrapper<OrganizationCourseComment> wrapper = new EntityWrapper<>();
        if (organizationId != null) {
            wrapper.eq("organization_id", organizationId);
        }
        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("course_name", condition);
        }
        page = organizationCourseCommentService.selectPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增评论信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OrganizationCourseComment organizationCourseComment) {
        organizationCourseCommentService.insert(organizationCourseComment);
        return SUCCESS_TIP;
    }

    /**
     * 删除评论信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    organizationCourseCommentService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改评论信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OrganizationCourseComment organizationCourseComment) {
        organizationCourseCommentService.updateById(organizationCourseComment);
        return SUCCESS_TIP;
    }

    /**
     * 评论信息详情
     */
    @RequestMapping(value = "/detail/{organizationCourseCommentId}")
    @ResponseBody
    public Object detail(@PathVariable("organizationCourseCommentId") Integer organizationCourseCommentId) {
        return organizationCourseCommentService.selectById(organizationCourseCommentId);
    }
}
