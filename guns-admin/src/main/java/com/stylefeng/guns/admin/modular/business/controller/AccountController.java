package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Account;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.persistence.modular.business.model.MyChilds;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.service.business.IAccountService;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;
import com.stylefeng.guns.service.business.IMyChildsService;
import com.stylefeng.guns.service.business.ISchoolService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 用户列表控制器
 *
 * @author fengshuonan
 * @Date 2018-07-30 12:19:00
 */
@Controller
@RequestMapping("/account")
public class AccountController extends BaseController {

    private String PREFIX = "/business/account/";

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IMyChildsService myChildsService;

    @Autowired
    private ISchoolService schoolService;

    @Autowired
    private IBusShcoolGradeService busShcoolGradeService;

    /**
     * 跳转到用户列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "account.html";
    }

    /**
     * 跳转到添加用户列表
     */
    @RequestMapping("/account_add")
    public String accountAdd() {
        return PREFIX + "account_add.html";
    }

    /**
     * 跳转到修改用户列表
     */
    @RequestMapping("/account_update/{accountId}")
    public String accountUpdate(@PathVariable Integer accountId, Model model) {
        Account account = accountService.selectById(accountId);
        model.addAttribute("item",account);
        LogObjectHolder.me().set(account);
        return PREFIX + "account_edit.html";
    }

    /**
     * 跳转到查看用户孩子列表
     */
    @RequestMapping("/account_childs/{accountId}")
    public String accountChilds(@PathVariable Integer accountId, Model model) {
        Wrapper<MyChilds> wrapper = new EntityWrapper<>();
        wrapper.eq("account_id", accountId);
        wrapper.orderBy("createtime", false);
        List<MyChilds> myChilds = myChildsService.selectList(wrapper);
        myChilds.forEach(child ->{
            if (NumberUtils.isCreatable(child.getChildSchool())) {
                School school = schoolService.selectById(Integer.valueOf(child.getChildSchool()));
                if (school != null) {
                    child.setChildSchool(school.getName());
                }
            }
            if (NumberUtils.isCreatable(child.getChildClass())) {
                BusShcoolGrade busShcoolGrade = busShcoolGradeService.selectById(Integer.valueOf(child.getChildClass()));
                if (busShcoolGrade != null) {
                    child.setChildClass(busShcoolGrade.getGcName() + busShcoolGrade.getClassName());
                }
            }
        });
        model.addAttribute("myChilds", myChilds);
        return PREFIX + "account_childs.html";
    }

    /**
     * 获取用户列表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<Account> page = new PageFactory().defaultPage();
        page = accountService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增用户列表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Account account) {
        accountService.insert(account);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户列表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    accountService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改用户列表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Account account) {
        accountService.updateById(account);
        return SUCCESS_TIP;
    }

    /**
     * 用户列表详情
     */
    @RequestMapping(value = "/detail/{accountId}")
    @ResponseBody
    public Object detail(@PathVariable("accountId") Integer accountId) {
        return accountService.selectById(accountId);
    }
}
