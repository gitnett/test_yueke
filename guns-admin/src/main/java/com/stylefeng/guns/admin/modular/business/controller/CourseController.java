package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.base.Splitter;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.admin.modular.system.utils.ExcelData;
import com.stylefeng.guns.admin.modular.system.utils.ExportExcelUtils;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.HttpUtil;
import com.stylefeng.guns.persistence.modular.business.model.*;
import com.stylefeng.guns.service.business.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 班级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 00:56:10
 */
@Controller
@RequestMapping("/course")
public class CourseController extends BaseController {

	private String PREFIX = "/business/course/";

    @Autowired
    private IBusAcctHisService acctHisService;

	@Autowired
	private ICourseService courseService;

	@Autowired
	private ISchoolService shcoolService;

	@Autowired
	private IGradeService gradeService;

	@Autowired
	private ISchoolCourseTypeService schoolCourseTypeService;

	@Autowired
    private IConfigInfoService configInfoService;
	

	private DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	/**
	 * 跳转到首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "course.html";
	}

	/**
	 * 跳转到添加
	 */
	@RequestMapping("/course_add")
	public String courseAdd(Model model) {

		Wrapper<School> schoolWarpper = new EntityWrapper<>();
		List<School> schools = shcoolService.selectList(schoolWarpper);
		model.addAttribute("b_school", schools);

		Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
		List<Grade> grades = gradeService.selectList(gradeWrapper);
		model.addAttribute("b_school", schools);
		model.addAttribute("b_grade", grades);

		Wrapper<SchoolCourseType> typeWrapper = new EntityWrapper<>();
		typeWrapper.orderDesc(Arrays.asList("serial_no", "createtime"));
		List<SchoolCourseType> schoolCourseTypes = schoolCourseTypeService.selectList(typeWrapper);
		model.addAttribute("schoolCourseTypes", schoolCourseTypes);
		return PREFIX + "course_add.html";
	}

	@RequestMapping("/course_update/{courseId}")
	public String courseUpdate(@PathVariable Integer courseId, Model model) {
		Course course = courseService.selectById(courseId);
		model.addAttribute("item", course);
		LogObjectHolder.me().set(course);

		Wrapper<School> schoolWrapper = new EntityWrapper<>();
		List<School> schools = shcoolService.selectList(schoolWrapper);
		Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
		List<Grade> grades = gradeService.selectList(gradeWrapper);
		model.addAttribute("b_school", schools);
		model.addAttribute("b_grade", grades);

		Wrapper<SchoolCourseType> typeWrapper = new EntityWrapper<>();
		typeWrapper.orderDesc(Arrays.asList("serial_no", "createtime"));
		List<SchoolCourseType> schoolCourseTypes = schoolCourseTypeService.selectList(typeWrapper);
		model.addAttribute("schoolCourseTypes", schoolCourseTypes);
		return PREFIX + "course_edit.html";
	}

    @RequestMapping("/course_copy/{courseId}")
    public String courseCopy(@PathVariable Integer courseId, Model model) {
        Course course = courseService.selectById(courseId);
        model.addAttribute("item", course);
        LogObjectHolder.me().set(course);

        Wrapper<School> schoolWrapper = new EntityWrapper<>();
        List<School> schools = shcoolService.selectList(schoolWrapper);
        Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
        List<Grade> grades = gradeService.selectList(gradeWrapper);
        model.addAttribute("b_school", schools);
        model.addAttribute("b_grade", grades);

        Wrapper<SchoolCourseType> typeWrapper = new EntityWrapper<>();
        typeWrapper.orderDesc(Arrays.asList("serial_no", "createtime"));
        List<SchoolCourseType> schoolCourseTypes = schoolCourseTypeService.selectList(typeWrapper);
        model.addAttribute("schoolCourseTypes", schoolCourseTypes);
        return PREFIX + "course_copy.html";
    }

    /**
     * 拷贝班级
     */
    @RequestMapping(value = "/addCopy")
    @ResponseBody
    public Object addCopy(Course course) {
        course.setCourseQuota(course.getCourseTolNum());
        course.setSchoolName(shcoolService.selectById(course.getSchoolId()).getName());
        courseService.insert(course);
        return SUCCESS_TIP;
    }

	/**
	 * 获取班级列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(String schoolId) {
		Page<Course> page = new PageFactory().defaultPage();
		Wrapper<Course> wrapper = new EntityWrapper<>();
		wrapper.orderDesc(Arrays.asList("createtime", "id"));
		if (StringUtils.isNotEmpty(schoolId)) {
			wrapper.eq("school_Id", Integer.valueOf(schoolId));
		}
		page = courseService.selectPage(page, wrapper);
		page.getRecords().forEach(record ->{
            if (record.getTypeId() != null) {
                SchoolCourseType schoolCourseType = schoolCourseTypeService.selectById(record.getTypeId());
                if (schoolCourseType != null) {
                    record.setTypeName(schoolCourseType.getName());
                }
            }
        });
		return packForBT(page);
	}

	/**
	 * 新增班级
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(Course course) {
		course.setCourseQuota(course.getCourseTolNum());
        course.setSchoolName(shcoolService.selectById(course.getSchoolId()).getName());
		courseService.insert(course);
        //通知用户
        Wrapper<ConfigInfo> configWrapper = new EntityWrapper<>();
        configWrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(configWrapper);
        if (configInfo != null && StringUtils.isNotBlank(configInfo.getContent())) {
            Map<String, String> params = new HashMap<>();
            params.put("id", course.getId() + "");
            Map map = HttpUtil.executeGet(configInfo.getContent() + "/m/updateCourse",params, Map.class);
        }
		return SUCCESS_TIP;
	}

	/**
	 * 删除班级
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam String ids) {
		if (StringUtils.isNotBlank(ids)) {
            List<String> strings = Splitter.on(",").omitEmptyStrings().splitToList(ids);
            for (String id : strings) {
                if (NumberUtils.isCreatable(id)) {
                    courseService.deleteById(Integer.valueOf(id));
                }
            }
		}
		return SUCCESS_TIP;
	}

	/**
	 * 修改课程
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(Course course) {
	    //修改学校名称
        course.setSchoolName(shcoolService.selectById(course.getSchoolId()).getName());
        //检查可报名人数
        if (course.getId() != null) {
            Course course1 = courseService.selectById(course.getId());
            if (course1 != null) {
                if (!course1.getCourseTolNum().equals(course.getCourseTolNum())) {//如果不一致
                    if (course.getCourseTolNum().compareTo(course1.getCourseTolNum()) > 0) {//修改的要比之前的大
                        Integer gap = course.getCourseTolNum() - course1.getCourseTolNum();
                        gap = course1.getCourseQuota() + gap;
                        course.setCourseQuota(gap);
                    }
                }
            }
        }
		courseService.updateById(course);
        //通知用户
        Wrapper<ConfigInfo> configWrapper = new EntityWrapper<>();
        configWrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(configWrapper);
        if (configInfo != null && StringUtils.isNotBlank(configInfo.getContent())) {
            Map<String, String> params = new HashMap<>();
            params.put("id", course.getId() + "");
            Map map = HttpUtil.executeGet(configInfo.getContent() + "/m/updateCourse" ,params, Map.class);
        }
		return SUCCESS_TIP;
	}

    /**
     * 跳转到分享二维码
     * http://s%/mi/details?id=?
     */
    @RequestMapping("/share/{courseId}")
    public String schoolShare(@PathVariable Integer courseId, Model model) {
        Wrapper<ConfigInfo> wrapper = new EntityWrapper<>();
        wrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(wrapper);
        String qrcodeUrl = null;
        if (configInfo != null) {
            qrcodeUrl = configInfo.getContent().concat("/mi/details?id=").concat(String.valueOf(courseId));
            model.addAttribute("code", 200);
        }else{
            model.addAttribute("code", 500);
        }
        model.addAttribute("qrcodeUrl", qrcodeUrl);
        return PREFIX + "course_share.html";
    }

	/**
	 * 课程详情
	 */
	@RequestMapping(value = "/detail/{courseId}")
	@ResponseBody
	public Object detail(@PathVariable("courseId") Integer courseId) {
		return courseService.selectById(courseId);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/daochu")
	public void list(HttpServletResponse response,String schoolId) throws Exception {
		
		Map<String,Object> map = new HashMap<String,Object>();
		if (NumberUtils.isCreatable(schoolId)) {
			map.put("school_id", Integer.valueOf(schoolId));
		}
		List<Course> lists = courseService.selectByMap(map);
		ExcelData data = new ExcelData();
		List<String> titles = new ArrayList();
		titles.add("ID");
		titles.add("课程名称");
		titles.add("招生对象");
		titles.add("上课时间");
		titles.add("上课星期");
		titles.add("课程费用");
		titles.add("剩余名额");
		titles.add("总报名人数");
		titles.add("培训学期");
		titles.add("上课教师");
		titles.add("上课教室");
		titles.add("联系电话");
		titles.add("状态");
		titles.add("创建时间");
		data.setTitles(titles);
		@SuppressWarnings("unchecked")
		
		List<List<Object>> rows = new ArrayList();
		for (Course course :lists) {
			List<Object> row = new ArrayList();
			row.add(course.getId());
			row.add(course.getCourseName());
			//招生对象
            if (StringUtils.isNotBlank(course.getStartCourseGrade())) {
                List<String> strings = Splitter.on(",").omitEmptyStrings().splitToList(course.getStartCourseGrade());
                StringBuilder sb = new StringBuilder();
                Grade grade;
                for (String id : strings) {
                    if (NumberUtils.isCreatable(id)) {
                        grade = gradeService.selectById(Integer.valueOf(id));
                        if (grade != null) {
                            sb.append(grade.getGradeName() + ",");
                        }
                    }
                }
                row.add(sb);
            }
			row.add(course.getCourseOnclassTime());
			row.add(course.getCourseOnclassWeek());
			row.add(course.getCourseFee());
			row.add(course.getCourseQuota());
			row.add(course.getCourseTolNum());
			row.add(course.getCouresTrainTerm());
			row.add(course.getCouresTeacher());
			row.add(course.getCouresClassroom());
			row.add(course.getCouresIphone());
			row.add(course.getCourseStatus()==0?"发布":"下架");
			row.add(format.format(course.getCreatetime()));
			rows.add(row);
		}
		data.setRows(rows);
		ExportExcelUtils.exportExcel(response, "课程信息.xlsx", data);
	}

    /**
     * 导出订单
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/orderExport")
    public void orderExport(HttpServletResponse response,String ids) throws Exception {
        Wrapper<BusAcctHis> wrapper = new EntityWrapper<>();
        wrapper.in("course_id", ids);
        wrapper.eq("status", "已支付");
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<BusAcctHis> busAcctHis = acctHisService.selectList(wrapper);

        ExcelData data = new ExcelData();
        List<String> titles = new ArrayList();
        titles.add("学校名称");
        titles.add("课程名称");
        titles.add("年级班级");
        titles.add("孩子姓名");
        titles.add("身份证号");
        titles.add("家长姓名");
        titles.add("家长电话");
        titles.add("创建时间");
        data.setTitles(titles);


        List<List<Object>> rows = new ArrayList();
        for (BusAcctHis acctHis :busAcctHis) {
            List<Object> row = new ArrayList();
            row.add(acctHis.getSchoolName());//学校名称
            row.add(acctHis.getCourseName());//课程名称
            row.add(acctHis.getGradeClassName());//班级
            row.add(acctHis.getChildName());//学生
            row.add(acctHis.getIdCard());//身份证
            row.add(acctHis.getAccountName());//家长
            row.add(acctHis.getAccountPhone());//家长电话
            row.add(format.format(acctHis.getCreatetime()));
            rows.add(row);
        }

        data.setRows(rows);
        ExportExcelUtils.exportExcel(response, "订单信息.xlsx", data);
    }
}
