package com.stylefeng.guns.admin.modular.bus.process;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.google.common.eventbus.Subscribe;
import com.stylefeng.guns.admin.core.bus.BusService;
import com.stylefeng.guns.admin.modular.bus.event.SchoolDelete;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;
import com.stylefeng.guns.service.business.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SchoolDeleteBus implements BusService<SchoolDelete> {

    @Autowired
    private ICourseService courseService;

    @Autowired
    private IBusShcoolGradeService busShcoolGradeService;


    @Override
    @Subscribe
    public void execute(SchoolDelete schoolDelete) {
        //删除班级
        Wrapper<BusShcoolGrade> gradeWrapper = new EntityWrapper<>();
        gradeWrapper.eq("school_id", schoolDelete.getSchool().getId());
        busShcoolGradeService.delete(gradeWrapper);
    }
}
