package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.bus.TopicBus;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.admin.modular.bus.event.SchoolDelete;
import com.stylefeng.guns.admin.modular.bus.event.SchoolUpdate;
import com.stylefeng.guns.admin.modular.bus.process.SchoolUpdateBus;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.BusShcoolGrade;
import com.stylefeng.guns.persistence.modular.business.model.ConfigInfo;
import com.stylefeng.guns.persistence.modular.business.model.Region;
import com.stylefeng.guns.persistence.modular.business.model.School;
import com.stylefeng.guns.service.business.IBusShcoolGradeService;
import com.stylefeng.guns.service.business.IConfigInfoService;
import com.stylefeng.guns.service.business.IRegionService;
import com.stylefeng.guns.service.business.ISchoolService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 校区管理控制器
 *
 * @author fengshuonan
 * @Date 2018-07-30 12:11:24
 */
@Controller
@RequestMapping("/school")
public class SchoolController extends BaseController {

    private String PREFIX = "/business/school/";

    @Autowired
    private ISchoolService schoolService;

    @Autowired
    private IRegionService regionService;

    @Autowired
    private IConfigInfoService configInfoService;

    @Autowired
    private TopicBus topicBus;

    @Autowired
    private IBusShcoolGradeService shcoolGradeService;

    /**
     * 跳转到校区管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "school.html";
    }

    /**
     * 跳转到添加校区管理
     */
    @RequestMapping("/school_add")
    public String schoolAdd(Model model) {

        //查询一级业务区域信息
        Wrapper<Region> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
        wrapper.eq("p_id", 0);//只查询一级区域
        List<Region> regions = regionService.selectList(wrapper);
        model.addAttribute("p_regions", regions);

        //取第一个业务区域信息进行查询预渲染
        if (regions != null && regions.size() > 0) {
            Region region = regions.get(0);
            wrapper = new EntityWrapper<>();
            wrapper.orderDesc(Arrays.asList("serial_no","createtime"));
            wrapper.eq("p_id", region.getId());
            regions = regionService.selectList(wrapper);
            model.addAttribute("c_regions", regions);
        }

        return PREFIX + "school_add.html";
    }

    /**
     * 跳转到修改校区管理
     */
    @RequestMapping("/school_update/{schoolId}")
    public String schoolUpdate(@PathVariable Integer schoolId, Model model) {
        School school = schoolService.selectById(schoolId);
        model.addAttribute("item",school);
        LogObjectHolder.me().set(school);
        return PREFIX + "school_edit.html";
    }

    /**
     * 跳转到分享二维码
     * http://s%/mi?schoolId=s%
     */
    @RequestMapping("/share/{schoolId}")
    public String schoolShare(@PathVariable Integer schoolId, Model model) {
        Wrapper<ConfigInfo> wrapper = new EntityWrapper<>();
        wrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(wrapper);
        String qrcodeUrl = null;
        if (configInfo != null) {
            qrcodeUrl = configInfo.getContent().concat("/mi?schoolId=").concat(String.valueOf(schoolId));
            model.addAttribute("code", 200);
        }else{
            model.addAttribute("code", 500);
        }
        model.addAttribute("qrcodeUrl", qrcodeUrl);
        return PREFIX + "school_share.html";
    }

    /**
     * 获取校区管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<School> page = new PageFactory().defaultPage();
        Wrapper<School> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("name", condition);
        }
        wrapper.orderDesc(Arrays.asList("createtime"));
        page = schoolService.selectPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增校区管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(School school) {
        school.setCreatetime(new Date());
        schoolService.insert(school);
        return SUCCESS_TIP;
    }

    /**
     * 删除校区管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    School school = schoolService.selectById(Integer.valueOf(id));
                    if (school != null) {
                        SchoolDelete schoolDelete = new SchoolDelete();
                        schoolDelete.setSchool(school);
                        topicBus.post(schoolDelete);
                    }
                    schoolService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改校区管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(School school) {
        schoolService.updateById(school);

        SchoolUpdate schoolUpdate = new SchoolUpdate();
        schoolUpdate.setSchool(schoolService.selectById(school.getId()));

        topicBus.post(schoolUpdate);

        return SUCCESS_TIP;
    }

    /**
     * 校区管理详情
     */
    @RequestMapping(value = "/detail/{schoolId}")
    @ResponseBody
    public Object detail(@PathVariable("schoolId") Integer schoolId) {
        return schoolService.selectById(schoolId);
    }
    
    
    /**
     * 获取校区管理列表
     */
    @RequestMapping(value = "/getAll")
    @ResponseBody
    public Object getAll() {
        Page<School> page = new PageFactory().defaultPage();
        page = schoolService.selectPage(page, null);
        return page.getRecords();
    }
    
    
    /**
     * 获取校区管理列表
     */
    @RequestMapping(value = "/queryAll")
    @ResponseBody
    public Object queryAll() {
        Wrapper<School> gradeWrapper = new EntityWrapper<>();
        List<School> schools =  schoolService.selectList(gradeWrapper);
        return schools;
    }
    

}
