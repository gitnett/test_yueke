package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Course;
import com.stylefeng.guns.persistence.modular.business.model.CourseComment;
import com.stylefeng.guns.service.business.ICourseCommentService;
import com.stylefeng.guns.service.business.ICourseService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 评论信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-13 17:26:42
 */
@Controller
@RequestMapping("/courseComment")
public class CourseCommentController extends BaseController {

    private String PREFIX = "/business/courseComment/";

    @Autowired
    private ICourseCommentService courseCommentService;

    @Autowired
    private ICourseService courseService;

    /**
     * 跳转到评论信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "courseComment.html";
    }

    /**
     * 跳转到添加评论信息
     */
    @RequestMapping("/courseComment_add")
    public String courseCommentAdd() {
        return PREFIX + "courseComment_add.html";
    }

    /**
     * 跳转到修改评论信息
     */
    @RequestMapping("/courseComment_update/{courseCommentId}")
    public String courseCommentUpdate(@PathVariable Integer courseCommentId, Model model) {
        CourseComment courseComment = courseCommentService.selectById(courseCommentId);
        model.addAttribute("item",courseComment);
        LogObjectHolder.me().set(courseComment);
        return PREFIX + "courseComment_edit.html";
    }

    /**
     * 获取评论信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<CourseComment> page = new PageFactory().defaultPage();
        page = courseCommentService.selectPage(page, null);
        page.getRecords().forEach(c ->{
            Integer courseId = c.getCourseId();
            Course course = courseService.selectById(courseId);
            if (course != null) {
                c.setSchoolName(course.getSchoolName());
            }
        });
        return packForBT(page);
    }

    /**
     * 新增评论信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CourseComment courseComment) {
        courseCommentService.insert(courseComment);
        return SUCCESS_TIP;
    }

    /**
     * 删除评论信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    courseCommentService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改评论信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CourseComment courseComment) {
        courseCommentService.updateById(courseComment);
        return SUCCESS_TIP;
    }

    /**
     * 评论信息详情
     */
    @RequestMapping(value = "/detail/{courseCommentId}")
    @ResponseBody
    public Object detail(@PathVariable("courseCommentId") Integer courseCommentId) {
        return courseCommentService.selectById(courseCommentId);
    }
}
