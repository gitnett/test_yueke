package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.support.DateTime;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationBanner;
import com.stylefeng.guns.service.business.IOrganizationBannerService;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 横幅信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 19:17:58
 */
@Controller
@RequestMapping("/organizationBanner")
public class OrganizationBannerController extends BaseController {

    private String PREFIX = "/business/organizationBanner/";

    @Autowired
    private IOrganizationBannerService organizationBannerService;

    @Autowired
    private IOrganizationService organizationService;

    /**
     * 跳转到横幅信息首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationBanner.html";
    }

    /**
     * 跳转到添加横幅信息
     */
    @RequestMapping("/organizationBanner_add")
    public String organizationBannerAdd(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationBanner_add.html";
    }

    /**
     * 跳转到修改横幅信息
     */
    @RequestMapping("/organizationBanner_update/{organizationBannerId}")
    public String organizationBannerUpdate(@PathVariable Integer organizationBannerId, Model model) {
        OrganizationBanner organizationBanner = organizationBannerService.selectById(organizationBannerId);
        model.addAttribute("item",organizationBanner);
        LogObjectHolder.me().set(organizationBanner);

        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationBanner_edit.html";
    }

    /**
     * 获取横幅信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Integer organizationId) {
        Page<OrganizationBanner> page = new PageFactory().defaultPage();
        Wrapper<OrganizationBanner> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("b.serial_no","b.createtime","b.id"));
        if (organizationId != null) {
            wrapper.eq("b.organization_id", organizationId);
        }
        page = organizationBannerService.selectByPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增横幅信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OrganizationBanner organizationBanner) {
        organizationBanner.setCreatetime(new Date());
        organizationBannerService.insert(organizationBanner);
        return SUCCESS_TIP;
    }

    /**
     * 删除横幅信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    organizationBannerService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改横幅信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OrganizationBanner organizationBanner) {
        organizationBannerService.updateById(organizationBanner);
        return SUCCESS_TIP;
    }

    /**
     * 横幅信息详情
     */
    @RequestMapping(value = "/detail/{organizationBannerId}")
    @ResponseBody
    public Object detail(@PathVariable("organizationBannerId") Integer organizationBannerId) {
        return organizationBannerService.selectById(organizationBannerId);
    }

    /**
     * 删除图片
     *
     */
    @RequestMapping("/delete_picture")
    @ResponseBody
    public Object deletePicture(Integer id) {
        return SUCCESS_TIP;
    }
}
