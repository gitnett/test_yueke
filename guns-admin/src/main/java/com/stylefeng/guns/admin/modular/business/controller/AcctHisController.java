package com.stylefeng.guns.admin.modular.business.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.base.Splitter;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.admin.modular.system.utils.ExcelData;
import com.stylefeng.guns.admin.modular.system.utils.ExportExcelUtils;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.HttpUtil;
import com.stylefeng.guns.persistence.modular.business.model.*;
import com.stylefeng.guns.service.business.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 订单记录控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 22:12:35
 */
@Controller
@RequestMapping("/acctHis")
public class AcctHisController extends BaseController {

    private String PREFIX = "/business/acctHis/";

    @Autowired
    private IBusAcctHisService acctHisService;
    
    @Autowired
    private ICourseService courseService;
    
    @Autowired
    private ISchoolCourseTypeService schoolCourseTypeService;

    @Autowired
    private ISchoolService schoolService;

    @Autowired
    private IGradeService gradeService;

    @Autowired
    private IConfigInfoService configInfoService;

    @Autowired
    private IMyChildsService myChildsService;
    
    private DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    /**
     * 跳转到订单记录首页
     */
    @RequestMapping("")
    public String index(Model model) {

        Wrapper<School> schoolWarpper = new EntityWrapper<>();
        List<School> schools = schoolService.selectList(schoolWarpper);
        model.addAttribute("schools", schools);

        return PREFIX + "acctHis.html";
    }

    /**
     * 跳转到添加订单记录
     */
    @RequestMapping("/acctHis_add")
    public String acctHisAdd() {
        return PREFIX + "acctHis_add.html";
    }

    /**
     * 跳转到修改订单记录
     */
    @RequestMapping("/acctHis_update/{acctHisId}")
    public String acctHisUpdate(@PathVariable Integer acctHisId, Model model) {
        BusAcctHis acctHis = acctHisService.selectById(acctHisId);
        model.addAttribute("item",acctHis);
        LogObjectHolder.me().set(acctHis);
        return PREFIX + "acctHis_edit.html";
    }

    /**
     * 获取订单记录列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String courseName,String gradeClassName,String accountName,Integer schoolId,String accountPhone,String startTime,String endTime) {
        Page<BusAcctHis> page = new PageFactory().defaultPage();
        Wrapper<BusAcctHis> wrapper = new EntityWrapper<>();

        wrapper.orderDesc(Arrays.asList("acct.createtime", "acct.id"));

        if (StringUtils.isNotEmpty(courseName)) {
            wrapper.like("acct.course_name", courseName);
        }

        if (StringUtils.isNotEmpty(gradeClassName)) {
            wrapper.like("acct.grade_class_name", gradeClassName);
        }
        if (StringUtils.isNotEmpty(accountName)) {
            wrapper.like("acct.account_name", accountName);
        }

        if (schoolId != null) {
            School school = schoolService.selectById(schoolId);
            if (school != null) {
                wrapper.like("acct.school_name", school.getName());
            }

        }

        if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
            startTime = startTime.concat(" 00:00");
            endTime = endTime.concat(" 23:59");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                Date start = sdf.parse(startTime);
                Date end = sdf.parse(endTime);
                wrapper.between("acct.paytime", start, end);
            } catch (Exception e) {
            }

        }

        if (StringUtils.isNotBlank(accountPhone)) {
            wrapper.like("acct.account_phone", accountPhone);
        }

        page = acctHisService.selectByPage(page,wrapper);
        return packForBT(page);
    }

    /**
     * 获取订单记录列表
     */
    @RequestMapping(value = "/statistics")
    public Object statistics(Model model,String courseName,String gradeClassName,String accountName,Integer schoolId,String accountPhone,String startTime,String endTime) {
        Wrapper<BusAcctHis> wrapper = new EntityWrapper<>();

        if (StringUtils.isNotEmpty(courseName)) {
            wrapper.like("course_name", courseName);
        }

        if (StringUtils.isNotEmpty(gradeClassName)) {
            wrapper.like("grade_class_name", gradeClassName);
        }

        if (StringUtils.isNotEmpty(accountName)) {
            wrapper.like("account_name", accountName);
        }

        if (schoolId != null) {
            School school = schoolService.selectById(schoolId);
            if (school != null) {
                wrapper.eq("school_name", school.getName());
            }

        }

        if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
            startTime = startTime.concat(" 00:00");
            endTime = endTime.concat(" 23:59");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                Date start = sdf.parse(startTime);
                Date end = sdf.parse(endTime);
                wrapper.between("paytime", start, end);
            } catch (Exception e) {
            }
        }

        if (StringUtils.isNotBlank(accountPhone)) {
            wrapper.like("account_phone", accountPhone);
        }

        Wrapper<BusAcctHis> or = wrapper.andNew("status = {0}","已支付");
        or.or("status = {0}", "已完成");


        List<BusAcctHis> busAcctHis = acctHisService.selectList(wrapper);

        //统计金额

        BigDecimal total = new BigDecimal("0.00").setScale(2,BigDecimal.ROUND_HALF_UP);


        for (BusAcctHis b : busAcctHis) {
            total = total.add(new BigDecimal(b.getAmount()));
        }

        model.addAttribute("total", total.doubleValue());


        return PREFIX + "statistics.html";
    }

    /**
     * 新增订单记录
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(BusAcctHis acctHis) {
        acctHisService.insert(acctHis);
        return SUCCESS_TIP;
    }

    /**
     * 删除订单记录
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    acctHisService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改订单记录
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(BusAcctHis acctHis) {
        acctHisService.updateById(acctHis);
        return SUCCESS_TIP;
    }

    /**
     * 课程详情
     */
    @RequestMapping(value = "/detail/{courseId}")
    public String detail(@PathVariable("courseId") Integer courseId,Model model) {
    	Course list =  courseService.selectById(courseId);
        model.addAttribute("item",list);
        Wrapper<SchoolCourseType> typeWrapper = new EntityWrapper<>();
		typeWrapper.orderDesc(Arrays.asList("serial_no", "createtime"));
		List<SchoolCourseType> schoolCourseTypes = schoolCourseTypeService.selectList(typeWrapper);
		model.addAttribute("schoolCourseTypes", schoolCourseTypes);

        Wrapper<Grade> gradeWrapper = new EntityWrapper<>();
        List<Grade> grades = gradeService.selectList(gradeWrapper);
        model.addAttribute("b_grade", grades);
        return PREFIX + "acctHis_detail.html";
    }

    /**
     * 退费操作
     */
    @RequestMapping("/backFee")
    @ResponseBody
    public Object backFee(String ids) {
        Wrapper<ConfigInfo> wrapper = new EntityWrapper<>();
        wrapper.eq("alias", "DOMAIN_NAME");
        ConfigInfo configInfo = configInfoService.selectOne(wrapper);
        List<String> stringList = Splitter.on(",").omitEmptyStrings().splitToList(ids);
        for (String id : stringList) {
            if (NumberUtils.isCreatable(id)) {
                BusAcctHis acctHis = acctHisService.selectById(Integer.valueOf(id));
                if (acctHis != null && configInfo != null && StringUtils.isNotBlank(configInfo.getContent())) {
                    String concat = configInfo.getContent().concat("/m/cancelCourse");
                    Map<String, String> params = new HashMap<>();
                    params.put("id", acctHis.getId() + "");
                    JSONObject jsonObject = HttpUtil.executeGet(concat ,params,JSONObject.class);
                    System.out.println(jsonObject);
                }
            }
        }

        return SUCCESS_TIP;
    }
    
    
    /**
     * 获取订单记录列表
     * @throws Exception 
     */
    @RequestMapping(value = "/hisExceport")
    public void hisExceport(HttpServletResponse response,String courseName,String gradeClassName,String accountName,Integer schoolId,String accountPhone,String startTime,String endTime) throws Exception {
        Wrapper<BusAcctHis> wrapper = new EntityWrapper<>();

        wrapper.orderDesc(Arrays.asList("createtime", "id"));

        if (StringUtils.isNotEmpty(courseName)) {
            wrapper.like("course_name", courseName);
        }

        if (StringUtils.isNotEmpty(gradeClassName)) {
            wrapper.like("grade_class_name", gradeClassName);
        }
        if (StringUtils.isNotEmpty(accountName)) {
            wrapper.like("account_name", accountName);
        }

        if (schoolId != null) {
            School school = schoolService.selectById(schoolId);
            if (school != null) {
                wrapper.like("school_name", school.getName());
            }

        }

        if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
            startTime = startTime.concat(" 00:00");
            endTime = endTime.concat(" 23:59");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            try {
                Date start = sdf.parse(startTime);
                Date end = sdf.parse(endTime);
                wrapper.between("paytime", start, end);
            } catch (Exception e) {
            }

        }

        if (StringUtils.isNotBlank(accountPhone)) {
            wrapper.like("account_phone", accountPhone);
        }

        List<BusAcctHis> list = acctHisService.selectList(wrapper);

        //处理孩子
        list.forEach(acctHis ->{
            if (acctHis.getChildId() != null) {
                MyChilds myChilds = myChildsService.selectById(acctHis.getChildId());
                acctHis.setChilds(myChilds);
            }
        });
        
        ExcelData data = new ExcelData();
        List<String> titles = new ArrayList();
        titles.add("学校名称");
        titles.add("课程名称");
        titles.add("上课星期");
        titles.add("上课时间");
        titles.add("年级班级");
        titles.add("孩子姓名");
        titles.add("身份证号");
        titles.add("家长姓名");
        titles.add("家长电话");
        titles.add("紧急联系人");
        titles.add("离开方式");
        titles.add("平台流水号(订单号)");
        titles.add("第三方流水号");
        titles.add("支付金额");
        titles.add("来源渠道");
        titles.add("支付状态");
        titles.add("描述语");
        titles.add("创建时间");
        data.setTitles(titles);


        List<List<Object>> rows = new ArrayList();
        for (BusAcctHis acctHis :list) {
            List<Object> row = new ArrayList();
            row.add(acctHis.getSchoolName());//学校名称
            row.add(acctHis.getCourseName());//课程名称
            Course course = courseService.selectById(acctHis.getCourseId());
            if (course != null) {
                row.add(course.getCourseOnclassWeek());//上课星期
                row.add(course.getCourseOnclassTime());//上课时间
            } else {
                row.add("");//上课星期
                row.add("");//上课时间
            }
            row.add(acctHis.getGradeClassName());//班级
            row.add(acctHis.getChildName());//学生
            row.add(acctHis.getIdCard());//身份证
            row.add(acctHis.getAccountName());//家长
            row.add(acctHis.getAccountPhone());//家长电话
            if (acctHis.getChilds() != null) {
                row.add(acctHis.getChilds().getPhoneTwo());//紧急联系人
                row.add(acctHis.getChilds().getLevelType());//离开方式
            }else{
                row.add("");//紧急联系人
                row.add("");//离开方式
            }
            row.add(acctHis.getFlowNo());
            row.add(acctHis.getBusFlowNo());
            row.add(acctHis.getAmount());
            row.add(acctHis.getAcctChannel());
            row.add(acctHis.getStatus());
            row.add(acctHis.getMsg());
            row.add(format.format(acctHis.getCreatetime()));
            rows.add(row);
        }
        data.setRows(rows);
        ExportExcelUtils.exportExcel(response, "订单信息.xlsx", data);
    }
}
