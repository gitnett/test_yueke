package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.SchoolCourseType;
import com.stylefeng.guns.service.business.ISchoolCourseTypeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;

/**
 * 评论信息控制器
 *
 * @author fengshuonan
 * @Date 2018-08-13 17:04:21
 */
@Controller
@RequestMapping("/schoolCourseType")
public class SchoolCourseTypeController extends BaseController {

    private String PREFIX = "/business/schoolCourseType/";

    @Autowired
    private ISchoolCourseTypeService schoolCourseTypeService;

    /**
     * 跳转到评论信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "schoolCourseType.html";
    }

    /**
     * 跳转到添加评论信息
     */
    @RequestMapping("/schoolCourseType_add")
    public String schoolCourseTypeAdd() {
        return PREFIX + "schoolCourseType_add.html";
    }

    /**
     * 跳转到修改评论信息
     */
    @RequestMapping("/schoolCourseType_update/{schoolCourseTypeId}")
    public String schoolCourseTypeUpdate(@PathVariable Integer schoolCourseTypeId, Model model) {
        SchoolCourseType schoolCourseType = schoolCourseTypeService.selectById(schoolCourseTypeId);
        model.addAttribute("item",schoolCourseType);
        LogObjectHolder.me().set(schoolCourseType);
        return PREFIX + "schoolCourseType_edit.html";
    }

    /**
     * 获取评论信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<SchoolCourseType> page = new PageFactory().defaultPage();
        Wrapper<SchoolCourseType> wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("name", condition);
        }
        wrapper.orderDesc(Arrays.asList("serial_no", "createtime"));
        page = schoolCourseTypeService.selectPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增评论信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SchoolCourseType schoolCourseType) {
        schoolCourseType.setCreatetime(new Date());
        schoolCourseTypeService.insert(schoolCourseType);
        return SUCCESS_TIP;
    }

    /**
     * 删除评论信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    schoolCourseTypeService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改评论信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SchoolCourseType schoolCourseType) {
        schoolCourseTypeService.updateById(schoolCourseType);
        return SUCCESS_TIP;
    }

    /**
     * 评论信息详情
     */
    @RequestMapping(value = "/detail/{schoolCourseTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("schoolCourseTypeId") Integer schoolCourseTypeId) {
        return schoolCourseTypeService.selectById(schoolCourseTypeId);
    }
}
