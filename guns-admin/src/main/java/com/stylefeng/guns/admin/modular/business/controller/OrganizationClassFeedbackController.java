package com.stylefeng.guns.admin.modular.business.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.Organization;
import com.stylefeng.guns.persistence.modular.business.model.OrganizationClassFeedback;
import com.stylefeng.guns.service.business.IOrganizationClassFeedbackService;
import com.stylefeng.guns.service.business.IOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;

import java.util.Arrays;
import java.util.List;

/**
 * 课后反馈控制器
 *
 * @author fengshuonan
 * @Date 2018-08-08 01:35:55
 */
@Controller
@RequestMapping("/organizationClassFeedback")
public class OrganizationClassFeedbackController extends BaseController {

    private String PREFIX = "/business/organizationClassFeedback/";

    @Autowired
    private IOrganizationClassFeedbackService organizationClassFeedbackService;

    @Autowired
    private IOrganizationService organizationService;

    /**
     * 跳转到课后反馈首页
     */
    @RequestMapping("")
    public String index(Model model) {
        Wrapper<Organization> wrapper = new EntityWrapper<>();
        wrapper.orderDesc(Arrays.asList("createtime"));
        List<Organization> organizations = organizationService.selectList(wrapper);
        model.addAttribute("organizations", organizations);
        return PREFIX + "organizationClassFeedback.html";
    }

    /**
     * 跳转到添加课后反馈
     */
    @RequestMapping("/organizationClassFeedback_add")
    public String organizationClassFeedbackAdd() {
        return PREFIX + "organizationClassFeedback_add.html";
    }

    /**
     * 跳转到修改课后反馈
     */
    @RequestMapping("/organizationClassFeedback_update/{organizationClassFeedbackId}")
    public String organizationClassFeedbackUpdate(@PathVariable Integer organizationClassFeedbackId, Model model) {
        OrganizationClassFeedback organizationClassFeedback = organizationClassFeedbackService.selectById(organizationClassFeedbackId);
        if (organizationClassFeedback != null && organizationClassFeedback.getOrganizationId() != null) {
            Organization organization = organizationService.selectById(organizationClassFeedback.getOrganizationId());
            organizationClassFeedback.setOrganization(organization);
        }
        model.addAttribute("item",organizationClassFeedback);
        LogObjectHolder.me().set(organizationClassFeedback);
        return PREFIX + "organizationClassFeedback_edit.html";
    }

    /**
     * 获取课后反馈列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,Integer organizationId) {
        Page<OrganizationClassFeedback> page = new PageFactory().defaultPage();
        Wrapper<OrganizationClassFeedback> wrapper = new EntityWrapper<>();

        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("f.account_name", condition);
        }

        if (organizationId != null) {
            wrapper.eq("f.organization_id", organizationId);
        }

        page = organizationClassFeedbackService.selectByPage(page, wrapper);
        return packForBT(page);
    }

    /**
     * 新增课后反馈
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OrganizationClassFeedback organizationClassFeedback) {
        organizationClassFeedbackService.insert(organizationClassFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 删除课后反馈
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    organizationClassFeedbackService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改课后反馈
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OrganizationClassFeedback organizationClassFeedback) {
        organizationClassFeedbackService.updateById(organizationClassFeedback);
        return SUCCESS_TIP;
    }

    /**
     * 课后反馈详情
     */
    @RequestMapping(value = "/detail/{organizationClassFeedbackId}")
    @ResponseBody
    public Object detail(@PathVariable("organizationClassFeedbackId") Integer organizationClassFeedbackId) {
        OrganizationClassFeedback organizationClassFeedback = organizationClassFeedbackService.selectById(organizationClassFeedbackId);
        if (organizationClassFeedback != null && organizationClassFeedback.getOrganizationId() != null) {
            Organization organization = organizationService.selectById(organizationClassFeedback.getOrganizationId());
            organizationClassFeedback.setOrganization(organization);
        }
        return organizationClassFeedback;
    }
}
