package com.stylefeng.guns.admin.modular.bus.event;


import com.stylefeng.guns.persistence.modular.business.model.School;

/**
 * 学校名称修改->对应修改课程绑定的学校名称
 *
 */
public class SchoolUpdate {

    private School school;


    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
