package com.stylefeng.guns.admin.modular.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.admin.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.admin.core.log.LogObjectHolder;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.persistence.modular.business.model.MyChilds;
import com.stylefeng.guns.service.business.IMyChildsService;

/**
 * 班级控制器
 *
 * @author fengshuonan
 * @Date 2018-08-04 00:56:45
 */
@Controller
@RequestMapping("/myChilds")
public class MyChildsController extends BaseController {

    private String PREFIX = "/business/myChilds/";

    @Autowired
    private IMyChildsService myChildsService;

    /**
     * 跳转到班级首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "myChilds.html";
    }

    /**
     * 跳转到添加班级
     */
    @RequestMapping("/myChilds_add")
    public String myChildsAdd() {
        return PREFIX + "myChilds_add.html";
    }

    /**
     * 跳转到修改班级
     */
    @RequestMapping("/myChilds_update/{myChildsId}")
    public String myChildsUpdate(@PathVariable Integer myChildsId, Model model) {
        MyChilds myChilds = myChildsService.selectById(myChildsId);
        model.addAttribute("item",myChilds);
        LogObjectHolder.me().set(myChilds);
        return PREFIX + "myChilds_edit.html";
    }

    /**
     * 获取班级列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<MyChilds> page = new PageFactory().defaultPage();
        page = myChildsService.selectPage(page, null);
        return packForBT(page);
    }

    /**
     * 新增班级
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(MyChilds myChilds) {
        myChildsService.insert(myChilds);
        return SUCCESS_TIP;
    }

    /**
     * 删除班级
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam String ids) {
        if(StringUtils.isNotBlank(ids)){
            String[] split = ids.split(",");
            for(String id : split){
                if(NumberUtils.isCreatable(id)){
                    myChildsService.deleteById(Integer.valueOf(id));
                }
            }
        }
        return SUCCESS_TIP;
    }

    /**
     * 修改班级
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(MyChilds myChilds) {
        myChildsService.updateById(myChilds);
        return SUCCESS_TIP;
    }

    /**
     * 班级详情
     */
    @RequestMapping(value = "/detail/{myChildsId}")
    @ResponseBody
    public Object detail(@PathVariable("myChildsId") Integer myChildsId) {
        return myChildsService.selectById(myChildsId);
    }
}
