/**
 * 班级管理初始化
 */
var MyChilds = {
    id: "MyChildsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
MyChilds.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '孩子名称', field: 'childName', visible: true, align: 'center', valign: 'middle'},
            {title: '孩子学校', field: 'childSchool', visible: true, align: 'center', valign: 'middle'},
            {title: '孩子班级', field: 'childClass', visible: true, align: 'center', valign: 'middle'},
            {title: '主表accout主键', field: 'accountId', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'createtime', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'updatetime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
MyChilds.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        MyChilds.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加班级
 */
MyChilds.openAddMyChilds = function () {
    var index = layer.open({
        type: 2,
        title: '添加班级',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/myChilds/myChilds_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看班级详情
 */
MyChilds.openMyChildsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/myChilds/myChilds_update/' + MyChilds.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除班级
 */
MyChilds.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/myChilds/delete", function (data) {
            Feng.success("删除成功!");
            MyChilds.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("myChildsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询班级列表
 */
MyChilds.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    MyChilds.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = MyChilds.initColumn();
    var table = new BSTable(MyChilds.id, "/myChilds/list", defaultColunms);
    table.setPaginationType("server");
    MyChilds.table = table.init();
});
