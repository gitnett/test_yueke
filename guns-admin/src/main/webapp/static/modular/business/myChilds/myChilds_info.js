/**
 * 初始化班级详情对话框
 */
var MyChildsInfoDlg = {
    myChildsInfoData : {}
};

/**
 * 清除数据
 */
MyChildsInfoDlg.clearData = function() {
    this.myChildsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MyChildsInfoDlg.set = function(key, val) {
    this.myChildsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MyChildsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MyChildsInfoDlg.close = function() {
    parent.layer.close(window.parent.MyChilds.layerIndex);
}

/**
 * 收集数据
 */
MyChildsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('childName')
    .set('childSchool')
    .set('childClass')
    .set('accountId')
    .set('createtime')
    .set('updatetime');
}

/**
 * 提交添加
 */
MyChildsInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",MyChildsInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/myChilds/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.MyChilds.table.refresh();
                MyChildsInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/myChilds/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.MyChilds.table.refresh();
    //    MyChildsInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.myChildsInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
MyChildsInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/myChilds/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.MyChilds.table.refresh();
    //   MyChildsInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.myChildsInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/myChilds/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.MyChilds.table.refresh();
                MyChildsInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
