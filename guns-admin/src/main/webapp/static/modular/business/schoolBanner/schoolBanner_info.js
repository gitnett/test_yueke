/**
 * 初始化横幅信息详情对话框
 */
var SchoolBannerInfoDlg = {
    schoolBannerInfoData : {}
};

/**
 * 清除数据
 */
SchoolBannerInfoDlg.clearData = function() {
    this.schoolBannerInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolBannerInfoDlg.set = function(key, val) {
    this.schoolBannerInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolBannerInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SchoolBannerInfoDlg.close = function() {
    parent.layer.close(window.parent.SchoolBanner.layerIndex);
}

/**
 * 收集数据
 */
SchoolBannerInfoDlg.collectData = function() {
    this
    .set('id')
    .set('title')
    .set('picture')
    .set('serialNo')
    .set('url')
    .set('state')
    .set('schoolId')
    .set('createtime');
}

/**
 * 提交添加
 */
SchoolBannerInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",SchoolBannerInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/schoolBanner/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.SchoolBanner.table.refresh();
                SchoolBannerInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/schoolBanner/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.SchoolBanner.table.refresh();
    //    SchoolBannerInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolBannerInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
SchoolBannerInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/schoolBanner/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.SchoolBanner.table.refresh();
    //   SchoolBannerInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolBannerInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/schoolBanner/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.SchoolBanner.table.refresh();
                SchoolBannerInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
