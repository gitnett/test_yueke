/**
 * 初始化校区管理详情对话框
 */
var SchoolInfoDlg = {
    schoolInfoData : {}
};

/**
 * 清除数据
 */
SchoolInfoDlg.clearData = function() {
    this.schoolInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolInfoDlg.set = function(key, val) {
    this.schoolInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SchoolInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SchoolInfoDlg.close = function() {
    parent.layer.close(window.parent.School.layerIndex);
}

/**
 * 收集数据
 */
SchoolInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('address')
    .set('createtime');
}

/**
 * 提交添加
 */
SchoolInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();
    var name = $("#name").val();
    var serialNo = $("#serialNo").val();

    if (name == null || name == '') {
        return Feng.error("学校名称不能为空");
    }

    if (serialNo == null || serialNo == '') {
        return Feng.error("排序序号不能为空");
    }

    var reg = /^\d+$/;
    if (!reg.test(serialNo)) {
        return Feng.error("排序序号只能为数字");
    }

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",SchoolInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/school/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.School.table.refresh();
                SchoolInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/school/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.School.table.refresh();
    //    SchoolInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
SchoolInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/school/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.School.table.refresh();
    //   SchoolInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.schoolInfoData);
    //ajax.start();

    var name = $("#name").val();
    var serialNo = $("#serialNo").val();

    if (name == null || name == '') {
        return Feng.error("学校名称不能为空");
    }

    if (serialNo == null || serialNo == '') {
        return Feng.error("排序序号不能为空");
    }

    var reg = /^\d+$/;
    if (!reg.test(serialNo)) {
        return Feng.error("排序序号只能为数字");
    }

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/school/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.School.table.refresh();
                SchoolInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
