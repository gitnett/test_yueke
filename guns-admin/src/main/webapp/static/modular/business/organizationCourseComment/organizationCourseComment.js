/**
 * 评论信息管理初始化
 */
var OrganizationCourseComment = {
    id: "OrganizationCourseCommentTable",	//表格id
    seItem: null,		//选中的条目
    setItems:'',
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OrganizationCourseComment.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
        {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '用户名称', field: 'accountName', visible: true, align: 'center', valign: 'middle'},
        {title: '课程名称', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
        {title: '机构名称', field: 'organizationName', visible: true, align: 'center', valign: 'middle'},
        {title: '星级', field: 'starCount', visible: true, align: 'center', valign: 'middle'},
        {title: '评论时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OrganizationCourseComment.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OrganizationCourseComment.seItem = selected[0];
        OrganizationCourseComment.seItems = ""; //清空数据
        for(var i = 0 ; i < selected.length; i++){
            OrganizationCourseComment.seItems += selected[i].id + ",";
        }
        return true;
    }
};

/**
 * 点击添加评论信息
 */
OrganizationCourseComment.openAddOrganizationCourseComment = function () {
    var index = layer.open({
        type: 2,
        title: '添加评论信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organizationCourseComment/organizationCourseComment_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看评论信息详情
 */
OrganizationCourseComment.openOrganizationCourseCommentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '评论信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organizationCourseComment/organizationCourseComment_update/' + OrganizationCourseComment.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除评论信息
 */
OrganizationCourseComment.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/organizationCourseComment/delete", function (data) {
            Feng.success("删除成功!");
            OrganizationCourseComment.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ids",this.seItems);
        ajax.start();
    }
};

/**
 * 查询评论信息列表
 */
OrganizationCourseComment.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['organizationId'] = $("#organizationId").val();
    OrganizationCourseComment.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = OrganizationCourseComment.initColumn();
    var table = new BSTable(OrganizationCourseComment.id, "/organizationCourseComment/list", defaultColunms);
    table.setPaginationType("server");
    OrganizationCourseComment.table = table.init();
});
