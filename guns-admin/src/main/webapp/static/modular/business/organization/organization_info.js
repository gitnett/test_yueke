/**
 * 初始化机构信息详情对话框
 */
var OrganizationInfoDlg = {
    organizationInfoData : {}
};

/**
 * 清除数据
 */
OrganizationInfoDlg.clearData = function() {
    this.organizationInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationInfoDlg.set = function(key, val) {
    this.organizationInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
OrganizationInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
OrganizationInfoDlg.close = function() {
    parent.layer.close(window.parent.Organization.layerIndex);
}

/**
 * 收集数据
 */
OrganizationInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('address')
    .set('createtime');
}

/**
 * 提交添加
 */
OrganizationInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",OrganizationInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/organization/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Organization.table.refresh();
                OrganizationInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organization/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Organization.table.refresh();
    //    OrganizationInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
OrganizationInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/organization/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Organization.table.refresh();
    //   OrganizationInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.organizationInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/organization/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.Organization.table.refresh();
                OrganizationInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
