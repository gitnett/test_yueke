

/**
 * 初始化部门详情对话框
 */
var GradeInfoDlg = {
    deptInfoData : {},
    zTreeInstance : null,
    validateFields: {
    	gradeName: {
            validators: {
                notEmpty: {
                    message: '年级名称不能为空'
                }
            }
        },
        serialNo : {
            validators: {
                notEmpty: {
                    message: '排序序号不能为空'
                },
                regexp:{
                    regexp: /^[0-9]+$/,
                    message: '排序序号输入数字'
                }
            }
        }
    }
};




/**
 * 清除数据
 */
GradeInfoDlg.clearData = function() {
    this.gradeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GradeInfoDlg.set = function(key, val) {
    this.gradeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GradeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GradeInfoDlg.close = function() {
    parent.layer.close(window.parent.Grade.layerIndex);
}

/**
 * 收集数据
 */
GradeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('gradeName')
    .set('createtime')
    .set('updatetime');
}
/**
 * 验证数据是否为空
 */
GradeInfoDlg.validate = function () {
    $('#submitForm').data("bootstrapValidator").resetForm();
    $('#submitForm').bootstrapValidator('validate');
    return $("#submitForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
GradeInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",GradeInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/grade/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Grade.table.refresh();
                GradeInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/grade/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Grade.table.refresh();
    //    GradeInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.gradeInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
GradeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }


    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/grade/update", function(data){
       Feng.success("修改成功!");
       window.parent.Grade.table.refresh();
      GradeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.gradeInfoData);
    ajax.start();
}

$(function() {
	 Feng.initValidator("submitForm", GradeInfoDlg.validateFields);
});
