/**
 * 初始化班级详情对话框
 */
var ImageInfoDlg = {
    imageInfoData : {}
};

/**
 * 清除数据
 */
ImageInfoDlg.clearData = function() {
    this.imageInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ImageInfoDlg.set = function(key, val) {
    this.imageInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ImageInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ImageInfoDlg.close = function() {
    parent.layer.close(window.parent.Image.layerIndex);
}

/**
 * 收集数据
 */
ImageInfoDlg.collectData = function() {
    this
    .set('id')
    .set('imageName')
    .set('imagePath')
    .set('imageType')
    .set('imageSize')
    .set('createtime')
    .set('updatetime');
}

/**
 * 提交添加
 */
ImageInfoDlg.addSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    var formData = new FormData($("#submitForm")[0]);

    //详情
    //formData.append("content",ImageInfoDlg.editor.txt.html());

    $.ajax({
        url:  Feng.ctxPath + "/image/add" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("添加成功!");
                window.parent.Image.table.refresh();
                ImageInfoDlg.close();
            }else{
                Feng.error("添加失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
         Feng.error("添加失败!");
        }
   });

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/image/add", function(data){
    //    Feng.success("添加成功!");
    //   window.parent.Image.table.refresh();
    //    ImageInfoDlg.close();
    //},function(data){
    //    Feng.error("添加失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.imageInfoData);
    //ajax.start();
}

/**
 * 提交修改
 */
ImageInfoDlg.editSubmit = function() {

    //this.clearData();
    //this.collectData();

    //提交信息
    //var ajax = new $ax(Feng.ctxPath + "/image/update", function(data){
    //    Feng.success("修改成功!");
    //    window.parent.Image.table.refresh();
    //   ImageInfoDlg.close();
    //},function(data){
    //    Feng.error("修改失败!" + data.responseJSON.message + "!");
    //});
    //ajax.set(this.imageInfoData);
    //ajax.start();

    var formData = new FormData($("#submitForm")[0]);
    $.ajax({
        url:  Feng.ctxPath + "/image/update" ,
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
            if(returndata.code == 200){
                Feng.success("修改成功!");
                window.parent.Image.table.refresh();
                ImageInfoDlg.close();
            }else{
                Feng.error("修改失败!错误："+returndata.message);
            }
        },
        error: function (returndata) {
           Feng.error("修改失败!");
        }
     });
}

$(function() {

});
