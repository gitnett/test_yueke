/**
 * 课后反馈管理初始化
 */
var OrganizationClassFeedback = {
    id: "OrganizationClassFeedbackTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
OrganizationClassFeedback.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '机构名称', field: 'organization', visible: true, align: 'center', valign: 'middle',formatter : function (value, row, index) {
                    if (value) {
                        return value.name;
                    }else{
                        return "未知机构";
                    }
            }},
            {title: '用户名称', field: 'accountName', visible: true, align: 'center', valign: 'middle'},
            {title: '反馈内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
OrganizationClassFeedback.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        OrganizationClassFeedback.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加课后反馈
 */
OrganizationClassFeedback.openAddOrganizationClassFeedback = function () {
    var index = layer.open({
        type: 2,
        title: '添加课后反馈',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/organizationClassFeedback/organizationClassFeedback_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看课后反馈详情
 */
OrganizationClassFeedback.openOrganizationClassFeedbackDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '课后反馈详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/organizationClassFeedback/organizationClassFeedback_update/' + OrganizationClassFeedback.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除课后反馈
 */
OrganizationClassFeedback.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/organizationClassFeedback/delete", function (data) {
            Feng.success("删除成功!");
            OrganizationClassFeedback.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("organizationClassFeedbackId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询课后反馈列表
 */
OrganizationClassFeedback.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['organizationId'] = $("#organizationId").val();
    OrganizationClassFeedback.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = OrganizationClassFeedback.initColumn();
    var table = new BSTable(OrganizationClassFeedback.id, "/organizationClassFeedback/list", defaultColunms);
    table.setPaginationType("server");
    OrganizationClassFeedback.table = table.init();
});
