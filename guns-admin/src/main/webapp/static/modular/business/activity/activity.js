/**
 * 活动信息管理初始化
 */
var Activity = {
    id: "ActivityTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Activity.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '系统编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '活动名称', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '机构名称', field: 'organization', visible: true, align: 'center', valign: 'middle',formatter : function (v,r,i) {
           if(v){
               return v.name;
           }else{
               return "未知机构";
           }
        }},
        {title: '总参与人数', field: 'totalCount', visible: true, align: 'center', valign: 'middle'},
        {title: '现参与人数', field: 'joinCount', visible: true, align: 'center', valign: 'middle'},
        {title: '适合学生', field: 'suitableTarget', visible: true, align: 'center', valign: 'middle'},
        {title: '活动时间', field: 'activityTime', visible: true, align: 'center', valign: 'middle'},
        {title: '咨询电话', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '是否发布', field: 'state', visible: true, align: 'center', valign: 'middle',formatter : function (v,r,i) {
            if (v == 0) {
                return "未发布"
            }else if (v == 1) {
                return "已发布"
            }
        }},
        {title: '参与价格', field: 'price', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createtime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Activity.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Activity.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加活动信息
 */
Activity.openAddActivity = function () {
    var index = layer.open({
        type: 2,
        title: '添加活动信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/activity/activity_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看活动信息详情
 */
Activity.openActivityDetail = function () {
    if (this.check()) {
        if($('#' + this.id).bootstrapTable('getSelections').length > 1){
            return Feng.error("只能选中一条记录查看！");
        }
        var index = layer.open({
            type: 2,
            title: '活动信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/activity/activity_update/' + Activity.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除活动信息
 */
Activity.delete = function () {
    if (this.check()) {
    	var operation = function(){
	        var id = Activity.seItem.id;
	        var ajax = new $ax(Feng.ctxPath + "/activity/delete", function (data) {
	            Feng.success("删除成功!");
	            Activity.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("ids",id);
	        ajax.start();
    };
    Feng.confirm("是否删除活动:" + Grade.seItem.title + "?",operation);
    }
};

/**
 * 查询活动信息列表
 */
Activity.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['organizationId'] = $("#organizationId").val();
    Activity.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Activity.initColumn();
    var table = new BSTable(Activity.id, "/activity/list", defaultColunms);
    table.setPaginationType("server");
    Activity.table = table.init();
});
