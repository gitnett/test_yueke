package com.stylefeng.guns.generator.action.config;

import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 默认的代码生成的配置
 *
 * @author fengshuonan
 * @date 2017-10-28-下午8:27
 */
public class GunsGeneratorConfig extends AbstractGeneratorConfig {

    protected void globalConfig() {
        globalConfig.setOutputDir("D:\\TEMPLATE");//写自己项目的绝对路径,注意具体到java目录
        globalConfig.setFileOverride(true);
        globalConfig.setEnableCache(false);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setOpen(false);
        globalConfig.setAuthor("LinYingQiang");
    }

    protected void dataSourceConfig() {
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
        dataSourceConfig.setUsername("arrange-class");
        dataSourceConfig.setPassword("arrange-class");
        dataSourceConfig.setUrl("jdbc:mysql://119.23.104.59:3306/arrange_class??autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&verifyServerCertificate=false&autoReconnect=true&failOverReadOnly=false");
    }

    protected void strategyConfig() {
        strategyConfig.setTablePrefix(new String[]{"bus_"});// 此处可以修改为您的表前缀
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
    }

    protected void packageConfig() {
        packageConfig.setParent(null);
        packageConfig.setEntity("com.stylefeng.guns.persistence.modular.business.model");
        packageConfig.setMapper("com.stylefeng.guns.persistence.modular.business.mapper");
        packageConfig.setXml("com.stylefeng.guns.persistence.modular.business.mapper.mapping");
        packageConfig.setController("com.stylefeng.guns.persistence.modular.business.controller");
        
    }

    protected void contextConfig() {
        contextConfig.setProPackage("com.stylefeng.guns.admin");
        contextConfig.setCoreBasePackage("com.stylefeng.guns.admin.core");
        contextConfig.setBizChName("开课申请");
        contextConfig.setBizEnName("courseApply");
        contextConfig.setModuleName("business");
        contextConfig.setProjectPath("D:\\TEMPLATE");//写自己项目的绝对路径
        contextConfig.setEntityName("CourseApply");
        contextConfig.setTableName("bus_course_apply");
        sqlConfig.setParentMenuName("");//这里写已有菜单的名称,当做父节点

        /**
         * mybatis-plus 生成器开关
         */
        contextConfig.setEntitySwitch(true);
        contextConfig.setDaoSwitch(true);
        contextConfig.setServiceSwitch(true);

        /**
         * guns 生成器开关
         */
        contextConfig.setControllerSwitch(true);
        contextConfig.setIndexPageSwitch(true);
        contextConfig.setAddPageSwitch(true);
        contextConfig.setEditPageSwitch(true);
        contextConfig.setJsSwitch(true);
        contextConfig.setInfoJsSwitch(true);
        contextConfig.setSqlSwitch(true);
    }

    @Override
    protected void config() {
        globalConfig();
        dataSourceConfig();
        strategyConfig();
        packageConfig();
        contextConfig();
    }
}
